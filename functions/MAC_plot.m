function [] = MAC_plot(mac,varargin);
% [] = MAC_plot(mac,fig_num);
%
% Function which generates a MAC plot of the input matrix 'mac'
% in the figure window 'fig_num' specified.
%
% One may specify the colormap as a third argument:
%   MAC_plot(mac,fig_num,'gray'); produces a MAC plot suitable for
%   grayscale publication.
%
% Matt Allen Janary 2004
% msalle@sandia.gov

% Fix by Dan Rohe to correct gray colorbar, 2012
if nargin > 1
    fig_num = varargin{1};
else
    fig_num = figure;
end

if length(size(mac)) < 3 & length(size(mac)) > 1;
    [a,b] = size(mac);
    %if a ~= b; error('Input Matrix must be square'); end
else
    error('Input must be a 2-Dimensional Vector');
end

xs = [1:a];
ys = [1:b];

% Generate Colors
	if nargin > 2
        cmap_name = varargin{2};
	else
        cmap_name = 'jet';
	end
    % Assumes scale from 0 to 1;
    cmap = eval([cmap_name,'(1001)']);
    if strcmp(cmap_name,'gray')
        cmap = flipud(cmap);
    end
    
x = [0:0.01:1]';
w2 = 0.50;

if min(min(mac)) < 0
    warning('MAC values less than 0, abs() taken');
    mac = abs(mac);
end
if max(max(mac)) > 1
    warning('MAC values greater than 1, Matrix Scaled');
    mac = mac/(max(max(mac)));
end

if nargin < 3
    figure(fig_num);  clf(fig_num);
end
%axes('Color',[1,1,1]);
for ii = 1:1:a;
    for jj = 1:1:b;
        xcoord = xs(ii) + [-w2 w2 w2 -w2];
        ycoord = ys(jj) + [-w2 -w2 w2 w2];
        col_vec = cmap(round(mac(ii,jj)*1000)+1,:);%interp1(x,cmap,mac(ii,jj));
        han = patch(xcoord,ycoord,col_vec);
    end
end
set(gca, 'XLim', [0.5 size(mac,1)+0.5],'YLim', [0.5 size(mac,2)+0.5],...
        'YTick', [1:size(mac,2)],'XTick', [1:size(mac,1)]);
colorbar;
title('\bfPlot of MAC Matrix');
xlabel('\bfFirst Index'); ylabel('\bfSecond Index');
eval(['colormap(',cmap_name,')']);

if strcmp(cmap_name,'gray');
    eval(['colormap(flipud(',cmap_name,'))']);
else
    eval(['colormap(',cmap_name,')']);
end

%fill([0 1 1 0],[0 0 1 1],'b')