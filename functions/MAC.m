function [varargout] = MAC(Phi_fit,varargin);
% [MACmat] = MAC(Phi);
% [MACmat] = MAC(Phi,Psi)
% Compute the auto MAC matrix for mode matrix Phi, 
% or the MAC matrix between modes Phi and Psi.
%
% Plots the MAC matrix to the current figure if no outputs are specified
%
% Matt Allen, 2004
% msalle@sandia.gov

if nargin == 1;
    for ii = 1:1:size(Phi_fit,2);
        for jj = 1:1:size(Phi_fit,2);
            MACmat(ii,jj) = abs(Phi_fit(:,ii)'*Phi_fit(:,jj))^2 / ...
                ((Phi_fit(:,ii)'*Phi_fit(:,ii))*(Phi_fit(:,jj)'*Phi_fit(:,jj)));
        end
    end
else
    Psi_fit = varargin{1};
    for ii = 1:1:size(Phi_fit,2);
        for jj = 1:1:size(Psi_fit,2);
            MACmat(ii,jj) = abs(Phi_fit(:,ii)'*Psi_fit(:,jj))^2 / ...
                ((Phi_fit(:,ii)'*Phi_fit(:,ii))*(Psi_fit(:,jj)'*Psi_fit(:,jj)));
        end
    end
end

if nargout < 1;
    MAC_plot(MACmat,gcf)
else
    varargout{1} = MACmat;
end