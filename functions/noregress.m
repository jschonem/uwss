function [Knl,tlist, dispErr, forceErr]=noregress(freq,shapes,force,disp, quad, triple, varargin)
%
%  function [Knl,tlist]=noregress(freq,shapes,force,disp,controls,flist);
%
%  function [Knl,tlist]=noregress(freq,phi,force,disp,controls,flist,phiTM);
%
%   fit a McEwan type of model without regression
%
%   INPUT
%
%        freq or K          stiffness: either a list of Modal Frequencies (Hz) (for modes of interest);
%                           or a linear stiffness matrix K
%                       if freq --(no. modes x 1)  if K (no. modes X no. modes)
%                            if (1 X 1) stiffness is assumed to be in form of freq
%        shapes              Matrix of Mode Shapes (phi) (no. nodes X no. modes)
%        phiTM               Matrix of phi.'*M where M is the mass matrix  (no. modes X no. nodes)
%        force               Static Force Cases (no. nodes X no. cases)
%        disp                Static Displacements (no. nodes x no. cases);
%        controls.quqd       if quad==1; use quads in the fit (default=0);
%        controls.triple      if triple==1; use 123 type cubic terms (default=0);
%        flist                fixed list of terms, same format as tlist; (default=[])
%
%   Note that there may be some inaccuracy associated with calling the
%   function using shapes instead of phiTM, but that is allowed for cases
%   where the FEA mass matrix is inaccessible.
%
%   OUTPUT
%        Knl          nonlinear coefficients (1 X nterms);
%        tlist        nonlinear term list  (nterms X 3)
%                     i.e. [1 1 0; 1 1 1; 1 1 2; ...]
%                     means Knl are coefficients in front of u1^2, u1^3, u2*u1^2, ...
%
%   version 3.0   JJH  original 8/21/02; add 123 cubic cross terms 8/12/05
%                      add option for K instead of freq 1/16/08
%   Updated by MSA 10/9/2014, added phiTM, added fit check.
%

% Defaults
if isempty(quad); quad = 1; end
if isempty(triple); triple = 1; end

if nargin > 6; flist=varargin{1}; else flist=[]; end

phiTM= pinv(shapes); % this is approximate

svd_flag=0;
auto_flag=1;
debug_flag='n';
nnodes=size(shapes,1);
nmodes=size(shapes,2);
ncases=size(force,2);
m=ones(1,nmodes);

if min(size(freq)) == 1
    K=diag((2*pi*freq(1:nmodes)).^2);
else
    K=freq(1:nmodes,1:nmodes);
end

%  make the term list
tlist=flist; qlist=[];
if isempty(tlist)
    if triple == 0
        clist=cubic_combinations([1:nmodes],0);
    else
        clist=cubic_combinations([1:nmodes],nmodes+1);
    end
    if quad==1
        qlist=quad_combinations([1:nmodes]);
    end
    tlist=[qlist' clist']';
end
nterms=size(tlist,1);

%  first calculate the modal contributions to the force and disp
% size(phiTM)
% size(disp)
p=phiTM*disp;             % Dim: (nmodes x ncases)
mforce=shapes.'*force;         % Dim: (nmodes x ncases)

% Form data matrix with all the combinations present

data_matrix=zeros(ncases,nterms);

for ii=1:ncases
    for jj=1:nterms
        if tlist(jj,3) == 0
            data_matrix(ii,jj)=p(tlist(jj,1),ii)*p(tlist(jj,2),ii);
        else
            data_matrix(ii,jj)=p(tlist(jj,1),ii)*p(tlist(jj,2),ii)*p(tlist(jj,3),ii);
        end
    end
end
% data_matrix = 10E10.*data_matrix;
% data_matrix
Fnl = mforce - K * p;
Fnl = Fnl';                               % Dim: (ncases X nmodes)

if svd_flag==1
    [U,S,V]=svd(data_matrix,0);
    sv=diag(S);
    if auto_flag==1
        tol=max(size(data_matrix)') * max(sv) * eps;
        rank = sum(sv > tol);
    else
        semilogy(1:(length(sv)),sv);grid;xlabel('sv number');ylabel('SV')
        rank=input('\n Input the rank:');
        rank=abs(round(rank));
        if rank > length(sv); rank=length(sv); end;
    end
    Sinv=diag(1 ./sv(1:rank));                         % find soln
    U=U(:,1:rank);V=V(:,1:rank);
    Knl=V*Sinv*U'*Fnl;
    Knl=Knl';
else
    size(data_matrix)
    size(Fnl)
    %   Knl = pinv(data_matrix)*Fnl;
    %   Knl =(10E10).*Knl;
    Knl= data_matrix \ Fnl;
%     [x,resnorm,residual,~,~,~] = lsqlin(data_matrix,Fnl,[],[]);
%     disp(['The residual for the LSQ fit is : ',num2str(resnorm)]);
    Knl=Knl';
end

%% Calculate Error Metric keyboard
disp_errornorm=norm(disp-shapes*p)/norm(disp); % error in approximating displacement as shapes*p (or phi*q)
% disp_errormax= max(abs(disp-shapes*p))/max(abs(disp))
fprintf('Ratio of norm(error) in Approximating Displacement as phi*q = %g\n',disp_errornorm);
% First using the actual curve fit:
modal_errornorm=norm(data_matrix*(Knl.')-Fnl)/norm(Fnl); % Percent error in force in modal coords
fprintf('Ratio of norm(error) in unconstrained fit for modal force = %g\n',modal_errornorm);

% Output values
dispErr = disp_errornorm;
forceErr = modal_errornorm;