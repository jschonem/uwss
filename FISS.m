% CCSS(filepath, filename) 
% 
% Craig-Bampton with characteristic constraint mode substructuring 
% routine for CBICE models. 
%
% 
 

classdef FISS < CBSS

    properties(GetAccess = 'public', SetAccess = 'public')
        
        Mcc;
        Kcc;
        phiCB;      % Original Craig-Bampton modal matrix
        psiCC;      % Matrix of characteristic constraint modes
        fnCC;       % "Frequencies" of the CC modes
        phiFI;      % Resulting modal matrix
        Tfi;        % FI dual transformation matrix
        modalCB;    % Modal DOF indices for CB matrix
        boundaryCB; % Boundary DOF indices for CB matrix
        nCharDOF;   % Number of characteristic constraint modes retained
        ccBasis;
        
        % Logical values
        cbModel = false; % Last SS was a CB model
        fiModel = false; % Last SS was a FI model
        
       
    end
    
    
    methods
        % Constructor
        function obj = FISS(filepath, filename)
            
            % Call CBSS constructor
            obj@CBSS(filepath, filename);
            
        end
        
        function obj = initNLSystem(obj)
            %
            % Initialize an NL system by setting all part nonlinearities to
            % "0."
            for i = 1:obj.npart
                obj.parts(i).part = obj.parts(i).part.zeroNLROM(obj.nCharDOF);
            end
        end
        
        function obj = copyNLPart(obj, basePart, copyPart)
            %    obj = copyNLPart(obj, basePart, copyPart)
            %
            % Copy the NL model of "basePart" to that of "copyPart."
            obj.parts(copyPart).part.NLROM = obj.parts(basePart).part.NLROM;
        end   
        
        % Build CC model
        function obj = buildFISystem(obj, nModes)
            %    obj = buildFISystem(obj, nModes)
            %
            % Build a system using flexible interface modes. Inputs
            % are the number of modes to compute and the number of CC modes
            % to retain.
            
            % Build B, L, matrix etc.
            obj = obj.buildBL();
            
            % For each component, go through and re-compute modes using the
            % flexible interface boundaries
            
            % Run through each part and make its CB model
            Tmodes = [];
            Tright = [];
            currDof = obj.parts(1).nModalDOF + 1; endDof = 0;
            Msys = []; Ksys = []; bexpSys = [];
            for i = 1:obj.npart
                
                part = obj.parts(i).part;
                lm = part.linModel;
                
                % Get the boundary stiffness matrix of the component
                [Kb, kb] = obj.interfaceReduction(i);
                
                % Get the connection and BC sets
                connSet = lm.connDofAbs;
                bcSet = lm.bcDofAbs;
                internalSet = lm.internalDofAbs;
                
                aset = [internalSet; connSet];
                
                % Get and augment component mass/stiffness matrices
                M = lm.M; K = lm.K;
                % To hell with it, just use Free interface modes!
%                 K(connSet, connSet) = K(connSet, connSet) + Kb;
%                 for j = 1:length(connSet)
%                     ind = connSet(j);
%                     K(ind, ind) = K(ind, ind) + kb(j);
%                 end
                
                Ma = M(aset, aset); Ka = K(aset, aset);
                
                % Get modes of augmented system
                nm = obj.parts(i).nModalDOF;
                [phiFI, fnFI] = eigs(Ka, Ma, nm, 'SM');
                fnFI = sqrt(diag(fnFI))/2/pi;
                [fnFI, sortInd] = sort(fnFI);
                phiFI = [phiFI(:, sortInd)];
                   
                % Note that nodes are ordered to go with the "aset"
                   
                % Assign to part
                obj.parts(i).part.linModel.phiFI = phiFI;
                obj.parts(i).part.linModel.psiIB = [];
                obj.parts(i).part.linModel.omegaN = fnFI*2*pi;
                
                % Add to block diagonal modal transformation
                Tmodes = blkdiag(Tmodes, phiFI);
                
                % Add to stacked pseudo-inverse
                endDof = currDof + obj.parts(i).nBoundaryDOF - 1;
                localInds = currDof:endDof;
                bi = obj.B(:, localInds); % Need to expand to size of K
                bexp = zeros(size(bi, 1), size(lm.K, 1));
                for j = 1:obj.parts(i).nBoundaryDOF
                    bexp(j, lm.connDofAbs) = bi(j, :);
                end
                bexp = bexp(:, aset);
                bexpSys = [bexpSys, bexp];
                
                Tright = [Tright; -lm.K(aset, aset)\(bexp')];
%                 Tright = [Tright; -Ka\(bexp')];
                if (i < obj.npart)
                    currDof = endDof + obj.parts(i + 1).nModalDOF + 1;
                end
                
                % Build full-order unassembled matrices
                Msys = blkdiag(Msys, lm.M(aset, aset));
                Ksys = blkdiag(Ksys, lm.K(aset, aset));
%                 Ksys = blkdiag(Ksys, Ka);
            end
            
            
            % Build the Dual CB transformation matrix (all full right now)
            nC = size(obj.B, 1);
            obj.Tfi = [Tmodes, Tright;
                      zeros(nC, size(Tmodes, 2)), eye(nC)];
            
            
            % Complete the reduced matrices
            Msys = blkdiag(Msys, zeros(nC));
            Ksys = [Ksys, bexpSys'; bexpSys, zeros(nC)];
            % Make transformation to get reduced matrices
            Mfi = obj.Tfi'*Msys*obj.Tfi;
            Kfi = obj.Tfi'*Ksys*obj.Tfi;
            
            % Modal analysis
            [obj.phiFI, fn] = eigs(Kfi, Mfi, nModes, 'SM');
            obj.fn = sqrt(diag(fn))/2/pi;
            
            % Sort modes to ascending order
            [obj.fn, modeOrder] = sort(obj.fn);
            obj.phiFI = obj.phiFI(:, modeOrder);
            
            
            % Convert the CC modal matrix back to Craig-Bampton "phi" for
            % plotting and MAC comparison
            obj.phi = obj.L*obj.phiFI; % Expand to unassembled coordinates
            
            % Assign modal amplitudes to each part
            obj = obj.assignAmplitudes();
            
            obj.cbModel = false;
            obj.fiModel = true;
        end
        
        % Slightly altered "comparePlot" routine
        function fig = comparePlot(obj, maxErr, nModes, cmap)
            if nargin < 2; maxErr = 2.5; end
            if nargin < 3; nModes = size(obj.phiFull, 2); end
            if nargin < 4; cmap = colormap('jet'); end
            
            fig = comparePlot@CBSS(obj, maxErr, nModes, cmap);
            
            % Modify title
            if (obj.ccModel)
                children = get(fig, 'children');
                ax = children(3);
                titlestr = get(ax, 'title'); titlestr = titlestr.String;
                titlestr = sprintf('%s Reduced to %i CC Modes', titlestr, ...
                    obj.nCharDOF);
                title(ax, titlestr);
            end
        end
        
        % Disallow calling "buildCBSystem" from this class, so there's no
        function obj = buildCBSystem(obj, nModes)
%             error('CB Substructuring Disabled for CCSS Class');
            obj.cbModel = true;
            obj.ccModel = false;
            obj = buildCBSystem@CBSS(obj, nModes);
        end
        
        function plotConstraintmode(obj, mode, factor, varargin)
            %    plotConstraintmode(obj, mode, factor, varargin)
            % Plot the deformation of a given mode for all parts
            if (nargin < 3); factor = 1; end
            
            cMode = obj.Tcc(:, obj.nModalDOF + mode);
            [~, cc2cb] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
            cMode = cMode(cc2cb);
            cMode = obj.L*cMode;
            currDOF = 1;
            for i = 1:obj.npart
                % Call the CBICE "deform" function with the appropriate set
                % of modal amplitudes
                endDOF = currDOF + obj.parts(i).nBoundaryDOF + obj.parts(i).nModalDOF;
                amps = cMode(currDOF:endDOF - 1);
                obj.parts(i).part.plot(amps*factor);
                currDOF = endDOF;
            end
            title(sprintf('\\bfConstraint Mode %i', mode));
        end
        
        
        function save(obj, filename)
            obj.filepath = 'CCSSs';
            if (nargin == 2)
                obj.filename = filename;
            end
            ccss = obj; %#ok
            save(fullfile(obj.filepath, obj.filename), 'ccss');
        end
        
        function disp(obj)
            % Display the object in a readable format
            fprintf('\nFISS Object\n');
            if obj.npart == 0;
                fprintf('No included parts\n');
            else
                fprintf('\t%s\n', obj.filename);
                fprintf('\t%i parts\n', obj.npart);
                fprintf('\t%i Modal DOF; %i Boundary DOF\n', ...
                    obj.nModalDOF, obj.nBoundaryDOF);
            end
            
            byteSize = whos('obj'); byteSize = byteSize.bytes;
            fprintf('\tSize of %.3f mb\n\n', byteSize/1024^2);
        end
            
    end
end