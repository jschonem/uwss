clear; clc; close all
figurePath = 'C:\Users\Joe\Dropbox\Thesis\chapter5\images';

addpath('C:\Users\Joe\Dropbox\MultiBayPanel\MATLAB\classes');

styles = {'--o', '-.o', ':s', '-^', '--<', ':*', 'x', '-'};
blue = [0.2, 0.2, 0.9]; red = [0.9, 0.2, 0.2]; grey = 0.5*[1, 1, 1];
green = [0.2, 0.4, 0.2];
black = [0, 0, 0];
colors = {black, 0.75*red, 0.75*blue, red, blue, grey, 'g'};

return

%% Truth NLROMs Development
for ii = 1
close all
saveFlag = true;
figurename = 'truth2Plates';
% Load the AMF NNM
f = figure('units', 'inches', 'position', [2, 1, 10, 3.5]);

nnmDir = '../NNMs';
nnmList = {'assembly_1_1_NNM-1';
           'assembly_1_1-2_NNM-1';
           'assembly_1_1-2-7-9_NNM-1'};
           

displayNames = {'Mode 1', 'Modes 1, 2', 'Modes 1, 2, 7, 9'};
% subplot(3, 1, 1:2);
n = length(displayNames);
for i = 1:n
    filename = nnmList{i};
    displayName = strrep(filename, '_', ' ');
    dat = load(fullfile(nnmDir, filename));
    semilogx(dat.Energy, dat.Pulsation/2/pi, styles{i}, 'linewidth', 1.5, ...
        'displayName', displayNames{i}, 'color', colors{i});
    set(gca, 'nextplot', 'add');
end

% Put the 1 beam thickness deflection in 
assy = load('../ICEROMs/assembly_1_qu1tr1_1');
% 1 thickness deflection in mode 1:
lm = assy.rom.linModel;
q1 = 0.031/max(abs(lm.phi(:, 1)));
N = length(assy.rom.mind); q = [q1; zeros(2*N - 1, 1)];
Klin = diag(lm.fn(assy.rom.mind)*2*pi).^2;

PE = PEfcn(q, Klin, assy.rom.NLROM.N1, assy.rom.NLROM.N2, assy.rom.NLROM.Nlist)


% xlim([1E-4, 1])

h = plot(PE*[1, 1], ylim, 'k:', 'linewidth', 2, 'displayName', '1 Thick. Disp.');
% nolegend(h);
grid on
ylim([120, 180]);
xlabel('\bfEnergy [in\cdot lb]');
% Add linear mode
fLin = dat.Pulsation(1)/2/pi;
% plot(xlim, fLin*[1, 1], 'k--', 'linewidth', 2, 'displayname', '1^{st} Linear Mode');

legend('location', 'northwest');
ylabel('\bfFrequency [Hz]');
if saveFlag; quicksave(figurename, figurePath, gcf, 'png', 250'); end
return
% Add periodicity data
subplot(3, 1, 3);
for i = 1:n
    filename = nnmList{i};
    displayName = strrep(filename, '_', ' ');
    dat = load(fullfile(nnmDir, 'periodicity', filename));
    semilogx(dat.nnm.Energy, dat.nnm.per*100, styles{i}, 'linewidth', 1.5, ...
        'color', colors{i});
    set(gca, 'nextplot', 'add');
end
plot(PE*[1, 1], ylim, 'k:', 'linewidth', 2, 'displayName', '1 Thick. Disp.');
xlabel('\bfEnergy [N\cdot mm]');
ylabel('\bfPeriodicity Error [%]');
grid on

if saveFlag; quicksave(figurename, figurePath, gcf, 'png', 250'); end
end

%% NNM Comparison of Panel-Only NNMs
for ii = 1
close all
saveFlag = true;
figurename = 'twoPlateNNMs';
% Load the AMF NNM
f = figure('units', 'inches', 'position', [2, 1, 10, 3.5]);

nnmDir = '../NNMs';
nnmList = {'assembly_1_1-2-7-9_NNM-1';
           'assembly_1_fi1-5_fix-NNM-1';
           'assembly_1_fi1-5_cc1-5-NNM-1';
           'assembly_1_qr1-10-NNM-1'
           'assembly_1_ficc_1-5_1-5_isv2-NNMc-1';
           'assembly_1_all_qris-NNM-1'};
           

displayNames = {'Assembly', 'FI Only [Fixed BCs]', ...
                'FI/CC [Linear BCs]', 'QR [Linear BCs]', ...
                'FI/CC [\itin situ\rm BCs]', 'QR [\itin situ\rm BCs]'};
            
colors = {black, grey, 0.75*red, 0.75*blue, red, blue};
            
            
n = length(displayNames);
for i = 1:n
    filename = nnmList{i};
    displayName = strrep(filename, '_', ' ');
    dat = load(fullfile(nnmDir, filename));
    semilogx(dat.Energy, dat.Pulsation/2/pi, styles{i}, 'linewidth', 1.5, ...
        'displayName', displayNames{i}, 'color', colors{i});
    set(gca, 'nextplot', 'add');
end

% Put the 1 beam thickness deflection in 
assy = load('../ICEROMs/assembly_1_qu1tr1_1-2-7-9');
% 1 thickness deflection in mode 1:
lm = assy.rom.linModel;
q1 = 0.031/max(abs(lm.phi(:, 1)));
N = length(assy.rom.mind); q = [q1; zeros(2*N - 1, 1)];
Klin = diag(lm.fn(assy.rom.mind)*2*pi).^2;

PE = PEfcn(q, Klin, assy.rom.NLROM.N1, assy.rom.NLROM.N2, assy.rom.NLROM.Nlist)


% xlim([1E-4, 1])
h = plot(PE*[1, 1], ylim, 'k:', 'linewidth', 2, 'displayName', '1 Thick. Disp.');
% nolegend(h);
grid on
ylim([123, 128]);

legend('location', 'northwest');
xlabel('\bfEnergy [in\cdot lb]');
ylabel('\bfFrequency [Hz]');


if saveFlag; quicksave(figurename, figurePath, gcf, 'png', 250'); end
end


