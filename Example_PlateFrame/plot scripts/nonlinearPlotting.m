% Nonlinear plot scripts for the 2-plate system
clear; clc; close all
filepath = 'C:\Users\Joe\Dropbox\Thesis\Panel Stiffener Model\images';
tablepath = 'C:\Users\Joe\Dropbox\Thesis\Panel Stiffener Model\tables';
addpath('C:\Users\Joe\Dropbox\MultiBayPanel\MATLAB\classes');
return

%% Examination of the FI/CC basis
close all
if ~exist('fitDat')
    load('../fitDatFICC');
end

fitDat = fd.fit9x9;
saveFlag = false;
f = figure('units', 'inches', 'position', [2, 2, 16, 5.2]);
Kl = fitDat.Kl;

% Self-MAC
ax1 = subplot(1, 3, 1);
b = fitDat.basis;
m = MAC(b, b);
MAC_plot(m, [], 'jet', ax1);
title('\bfSelf-MAC of FI/CC Basis');
ylabel('\bfFI/CC Basis'); xlabel('');
xlim([0.5, 10.5]); ylim([0.5, 10.5]);

% 9 x 9 plate - K Ortho
ax1 = subplot(1, 3, 2);
b = fitDat.basis;
lambda = b'*Kl*b;
for i = 1:size(lambda, 2)
    lambda(:, i) = lambda(:, i)/lambda(i, i);
end
MAC_plot(lambda, [], 'jet', ax1);
xlabel(''); ylabel('');
title('\bfStiffness Orthogonality of FI/CC Basis');
xlim([0.5, 10.5]); ylim([0.5, 10.5]);

% 9 x 9 plate - displacement cross-MAC
ax1 = subplot(1, 3, 3);
b = fitDat.basis; d = fitDat.displacement(:, 1:size(fitDat.basis, 2));
m = MAC(b, d);
MAC_plot(m, [], 'jet', ax1);
title('\bfCross-MAC of FI/CC Basis and Displacements');
xlabel(''); ylabel('');
xlim([0.5, 10.5]); ylim([0.5, 10.5]);


figurePath = 'C:\Users\Joe\Dropbox\Thesis\Panel Stiffener Substructuring\images';
filename = 'panelStiffener_ficcMac';
if saveFlag; quicksave(filename, figurePath, gcf, 'png', 250'); end



%% Examination of the QR basis
close all
if ~exist('fitDat')
    fitDat = load('../diagQRData');
end
K9x9 = load('../K9x9'); K9 = K9x9.Kl; K9x6 = load('../K9x6');K6 = K9x6.Kl;
    
saveFlag = true;
f = figure('units', 'inches', 'position', [2, 2, 16, 5.2]);

% 9 x 9 plate - Self-MAC
ax1 = subplot(2, 3, 1);
[b, ~] = qr(fitDat.fit9x9.basis, 0);
m = MAC(b, b);
MAC_plot(m, [], 'jet', ax1);
title('\bfSelf-MAC of QR Basis');
ylabel('\bfQR Basis'); xlabel('');

% 9 x 6 plate - Self-MAC
ax2 = subplot(2, 3, 4);
[b, ~] = qr(fitDat.fit9x6.basis, 0);
m = MAC(b, b);
MAC_plot(m, [], 'jet', ax2);
title('');
xlabel('\bfQR Basis'); ylabel('\bfQR Basis');


% 9 x 9 plate - K Ortho
ax1 = subplot(2, 3, 2);
[b, ~] = qr(fitDat.fit9x9.basis, 0);
lambda = b'*K9*b;
for i = 1:size(lambda, 2)
    lambda(:, i) = lambda(:, i)/max(abs(lambda(:, i)));
end
MAC_plot(lambda, [], 'jet', ax1);
xlabel(''); ylabel('');
title('\bfStiffness Orthogonality of QR Basis');


% 9 x 6 plate - K Orth
ax2 = subplot(2, 3, 5);
[b, ~] = qr(fitDat.fit9x6.basis, 0);
lambda = b'*K6*b;
for i = 1:size(lambda, 2)
    lambda(:, i) = lambda(:, i)/max(abs(lambda(:, i)));
end
MAC_plot(lambda, [], 'jet', ax2);
ylabel(''); title('');
xlabel('\bfQR Basis');
% 
% 9 x 9 plate - displacement cross-MAC
ax1 = subplot(2, 3, 3);
b = fitDat.fit9x9.loadBasis; d = fitDat.fit9x9.displacement(:, 1:16);
m = MAC(b, d);
MAC_plot(m, [], 'jet', ax1);
title('\bfCross-MAC of QR Basis and Displacements');
xlabel(''); ylabel('');

% 9 x 6 plate - displacement cross-MAC
ax2 = subplot(2, 3, 6);
b = fitDat.fit9x6.loadBasis; d = fitDat.fit9x6.displacement(:, 1:11);
m = MAC(b, d);
MAC_plot(m, [], 'jet', ax2); 
title('');% 9 x 9 plate
xlabel('\bfDisplacement'); ylabel('');




figurePath = 'C:\Users\Joe\Dropbox\Thesis\Basic Plate\images';
filename = 'plate_qrMac';
if saveFlag; quicksave(filename, figurePath, gcf, 'png', 250'); end

%% Cross-MAC of the FI/CC Basis with SVD and QR bases
if ~exist('fitDat')
    fitDat = load('../fullQRData');
end

    
saveFlag = true;
f = figure('units', 'inches', 'position', [2, 2, 10, 2.75]);

a = fitDat.fit9x9.basis;
% 9 x 9 plate - SVD
ax1 = subplot(1, 2, 1);
[b, ~, ~] = svd(fitDat.fit9x9.basis, 0);
m = MAC(b, a);
MAC_plot(m, [], 'jet', ax1);
title('\bfCross MAC - FI/CC and SVD');
ylabel('\bfFI/CC Basis'); xlabel('\bfSVD Basis');

% 9 x 9 plate - QR
ax2 = subplot(1, 2, 2);
[b, ~] = qr(fitDat.fit9x9.basis, 0);
m = MAC(b, a);
MAC_plot(m, [], 'jet', ax1);
title('\bfCross MAC - FI/CC and QR');
ylabel('\bfFI/CC Basis'); xlabel('\bfQR Basis');



figurePath = 'C:\Users\Joe\Dropbox\Thesis\Basic Plate\images';
filename = 'plate_svdqrCrossMac';
if saveFlag; quicksave(filename, figurePath, gcf, 'png', 250'); end



%% Plots of interesting SVD/QR modes
% This doesn't work right now.
close all
load('../CCSSs/plates');


if ~(exist('svdDat') && exist('qrDat'))
    svdDat = load('../fullSVDData');
    qrDat = load('../fullQRData');
end



vectors = [svdDat.fit9x9.theta(:, 12), svdDat.fit9x9.theta(:, 16), ...
           qrDat.fit9x9.theta(:, 13), qrDat.fit9x9.theta(:, 14)];
labels = {'SVD 12'; 'SVD 16'; 'SVD 13'; 'SVD 14'};

overwrite = false;

tablename = 'plateTransformedModes';
fid = fopen(fullfile(tablepath, [tablename, '.tex']), 'w');
f = figure('units', 'inches', 'position', [4, 5, 3, 2]);
try
    % Table preamble
    fprintf(fid, '\\begin{tabular}{c | c | c | c}\n');
%     fprintf(fid, '\\multicolumn{4}{c}{Fixed Interface Modes; 9x9 Plate} \\\\ \n');
    fprintf(fid, '\\hline \\hline\n');
     
    % Plot each vector
    fprintf(fid, '& & & \\\\ \n');
    for j = 1:4
        
        % If this works then there is a mode to plot
        figname = sprintf('%s_cc_%i', tablename, j);

        % Make and plot the figure
        clf;
        ccss.parts(1).part.plot(vectors(:, j));
        title('');
        view([-45, 35]);
        axis off
        if (~(exist(fullfile(tablepath, [figname, '.jpg']), 'file')) || overwrite)
            quicksave(figname, tablepath, f, 'jpg', 300);
        end
        % Now add text to table
        fprintf(fid, '\\begin{minipage}{.2\\textwidth}\n');
        fprintf(fid, '\\includegraphics[width=\\linewidth]{tables/%s}\n', figname);
        fprintf(fid, '\\end{minipage} ');
        if (j < 4)
            fprintf(fid, '&\n');
        else 
            fprintf(fid, '\\\\ \n');
        end

    end


    for j = 1:4
        fprintf(fid, labels{j});
        if (j < 4)
            fprintf(fid, '&');
        else
            fprintf(fid, '\\\\');
        end
    end
    fprintf(fid, '\\hline');         


    fprintf(fid, '\\hline');
    fprintf(fid, '\\end{tabular}\n');

catch err
    fclose(fid);
    rethrow(err);
end
fclose(fid);


