% Linear plot scripts for the plate-stiffener system
clear; clc; close all
filepath = 'C:\Users\Joe\Dropbox\Thesis\chapter5\images';
tablepath = 'C:\Users\Joe\Dropbox\Thesis\chapter5\tables';
addpath('C:\Users\Joe\Dropbox\MultiBayPanel\MATLAB\classes');
return


%% Plot of just the assembly
% Load a basic CCSS model
load('../CCSSs/assembly_1');

f = figure('units', 'inches', 'position', [3, 2, 7, 4]);
ccss.plot();
view([-46, 33]);

axis off

quicksave('assembly2Plates', filepath, f, 'jpg', 250);


%% Constraint Modes System
close all
load('../CCSSs/assembly_1');

fiGroups = {[1, 2, 3, 4, 5]; [6, 7, 8, 9, 10]};

overwrite = true;
tablename = 'constraintModes2Plates';
fid = fopen(fullfile(tablepath, [tablename, '.tex']), 'w');
f = figure('units', 'inches', 'position', [4, 5, 3, 2]);
tablecount = 5;
try
    % Table preamble
    fprintf(fid, '\\begin{tabular}{');
    for j = 1:tablecount
        fprintf(fid, 'c'); if (j < tablecount); fprintf(fid, '|'); end
    end
    fprintf(fid, '}\n');   
%     fprintf(fid, '\\multicolumn{%i}{c}{Soft Component (1.5 mm Thickness)} \\\\ \n', tablecount);
    fprintf(fid, '\\hline \\hline\n');
     
    % Plot each set of FI modes
    for i = 1:length(fiGroups)
        modeVec = fiGroups{i};
        for j = 1:(tablecount - 1)
            fprintf(fid, '& ');
        end
        fprintf(fid, '\\\\ \n');
        for j = 1:tablecount
            try k = modeVec(j);
                % If this works then there is a mode to plot
                figname = sprintf('%s_cc_%i', tablename, k);
                
                % Make and plot the figure
                clf;
                ccss.plotConstraintmode(k, -5);
                title('');
                cameratoolbar('SetCoordSys', 'y')
                view([-46, 33]);
                axis off
                
                if (~(exist(fullfile(tablepath, [figname, '.jpg']), 'file')) || overwrite)
                    quicksave(figname, tablepath, f, 'jpg', 300);
                end
                % Now add text to table
                width = 0.85/tablecount;
                fprintf(fid, '\\begin{minipage}{%.2f\\textwidth}\n', width);
                fprintf(fid, '\\includegraphics[width=\\linewidth]{chapter5/tables/%s}\n', figname);
                fprintf(fid, '\\end{minipage} ');
                if (j < tablecount)
                    fprintf(fid, '&\n');
                else 
                    fprintf(fid, '\\\\ \n');
                end
                    
            catch err
                if (j < tablecount)
                    fprintf(fid, '-- & ');
                else
                    fprintf(fid, '-- \\\\ \n');
                end
            end
            
        end
        
        
        for j = 1:tablecount
            try k = modeVec(j); % Add mode number and natural frequency
                fprintf(fid, 'Mode %i: %.1f Hz ', k, ccss.fnCC(k));
                if (j < tablecount)
                    fprintf(fid, '&');
                else
                    fprintf(fid, '\\\\');
                end
            catch err
                if (j < tablecount)
                    fprintf(fid, ' & ');
                else
                    fprintf(fid, '\\\\ \n');
                end
            end
        end
        fprintf(fid, '\\hline');         
    end
      

    fprintf(fid, '\\hline');
    
    fprintf(fid, '\\end{tabular}\n');

catch err
    fclose(fid);
    rethrow(err);
end
fclose(fid);



%% Vibration Modes of Stiff/Soft System
close all
load('../CCSSs/assembly_1_true');

fiGroups = {[1, 2, 3, 4, 5]; [6, 7, 8, 9, 10]};

overwrite = true;
tablename = 'vibModes2Plates';
fid = fopen(fullfile(tablepath, [tablename, '.tex']), 'w');
f = figure('units', 'inches', 'position', [4, 5, 3, 2]);
tablecount = 5;
try
    % Table preamble
    fprintf(fid, '\\begin{tabular}{');
    for j = 1:tablecount
        fprintf(fid, 'c'); if (j < tablecount); fprintf(fid, '|'); end
    end
    fprintf(fid, '}\n');   
%     fprintf(fid, '\\multicolumn{%i}{c}{Soft Component (1.5 mm Thickness)} \\\\ \n', tablecount);
    fprintf(fid, '\\hline \\hline\n');
     
    % Plot each set of FI modes
    for i = 1:length(fiGroups)
        modeVec = fiGroups{i};
        for j = 1:(tablecount - 1)
            fprintf(fid, '& ');
        end
        fprintf(fid, '\\\\ \n');
        for j = 1:tablecount
            try k = modeVec(j);
                % If this works then there is a mode to plot
                figname = sprintf('%s_vm_%i', tablename, k);
                
                % Make and plot the figure
                clf;
                ccss.plotmode(k, -1);
                title('');
                cameratoolbar('SetCoordSys', 'y')
                view([-46, 33]);
                axis off
                
                if (~(exist(fullfile(tablepath, [figname, '.jpg']), 'file')) || overwrite)
                    quicksave(figname, tablepath, f, 'jpg', 300);
                end
                % Now add text to table
                width = 0.85/tablecount;
                fprintf(fid, '\\begin{minipage}{%.2f\\textwidth}\n', width);
                fprintf(fid, '\\includegraphics[width=\\linewidth]{chapter5/tables/%s}\n', figname);
                fprintf(fid, '\\end{minipage} ');
                if (j < tablecount)
                    fprintf(fid, '&\n');
                else 
                    fprintf(fid, '\\\\ \n');
                end
                    
            catch err
                if (j < tablecount)
                    fprintf(fid, '-- & ');
                else
                    fprintf(fid, '-- \\\\ \n');
                end
            end
            
        end
        
        
        for j = 1:tablecount
            try k = modeVec(j); % Add mode number and natural frequency
                fprintf(fid, 'Mode %i: %.1f Hz ', k, ccss.fn(k));
                if (j < tablecount)
                    fprintf(fid, '&');
                else
                    fprintf(fid, '\\\\');
                end
            catch err
                if (j < tablecount)
                    fprintf(fid, ' & ');
                else
                    fprintf(fid, '\\\\ \n');
                end
            end
        end
        fprintf(fid, '\\hline');         
    end
   

    fprintf(fid, '\\hline');
    
    fprintf(fid, '\\end{tabular}\n');

catch err
    fclose(fid);
    rethrow(err);
end
fclose(fid);




%% MAC of the substructure modes and FEA modes
close all

load('../CCSSs/assembly_1');
f = ccss.comparePlot(1, 12, colormap('jet'));
axs = get(f, 'children');
ax = axs(3);
title(ax, '');
quicksave('mac2plates', filepath, f, 'jpg', 250);


