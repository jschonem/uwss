% ROM Wrapper for MBPANEL Test model
% Hit "F5" to change directory
cd('C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\MATLAB');
clear; clc; close all
clear classes;
addpath('C:\Users\Joe\Dropbox\MultiBayPanel\MATLAB\classes');
return;


%% Generate some NLROMs 
cd('C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\MATLAB');
mind = [1, 2, 3, 4, 5];
deflect = 1*ones(size(mind));
romFile = 'assembly_1_unified_qu1tr1_';
dat = load(fullfile('ICEROMs', romFile));
rom = dat.rom;

rom = rom.genLoadCases(mind, deflect);
rom = rom.runLoadCases();
rom.save();




%% INITIAL ABINT/ICEROM GENERATION %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%% Generate ABINT for Assembly 1
workingDir = 'C:\Temp';
filepathAbint = 'ABINTs';
filepathCbice = 'CBICEs';
pythonpath = 'C:\Users\Joe\Dropbox\MultiBayPanel\python';

% Make ABINT object and save for later
ab = ABINT(workingDir, filepathAbint, pythonpath);
% Read an input file
inputPath = 'C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\Abaqus\models';
inputFile = 'assembly_nomembrane9x6';
ab = ab.readInp(inputPath, inputFile);
ab = ab.getDof();
[M, K] = ab.matrix([]);
save(ab);

filepath = 'ICEROMs';
thick = 0.031;
rom = ICEROMab(ab, thick, filepath);
boundaryNodes = ab.nodeSets.('FRAME-1_BCSET'); nBC = length(boundaryNodes);
boundary = [boundaryNodes, ones(nBC, 3), zeros(nBC, 3)];

rom = rom.linearModel(boundary);
rom.save();


