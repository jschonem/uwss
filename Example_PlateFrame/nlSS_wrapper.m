% Wrapper script for substructure testing and development
% Hit "F5" to change directory
clear; clc; close all
clear classes;
return;
%% Assembly 1; FI Modes, Springs

dat = load('CCSSs/assembly_1');
ccs = dat.ccss;

ccs = ccs.initNLSystem();

basePart = 3;
flist = repmat([1:5]', 1, 3);
[ccs, fd.fit9x6] = ccs.addNLPart(2, 1:5, 1:10, false, 1, 0, 0, flist);
flist = repmat([1:10]', 1, 3);
[ccs, fd.fit9x9] = ccs.addNLPart(3, 1:5, 1:10, false, 1, 0, 0, flist);

save('fitDatFICC', 'fd');
% ccs.save('assembly_1_fi1-5_cc1-5_fix');

%% Assembly 1; QR Vectors, Springs
cd('C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\MATLAB');
dat = load('CCSSs/assembly_1');
ccs = dat.ccss;

ccs = ccs.initNLSystem();
ccs = ccs.addNLPartQR(2, [1:10], true, 1, 1);
ccs = ccs.addNLPartQR(3, [1:10], true, 1, 1);

ccs.save('assembly_1_qr1-10');


%% Make substructure using "in situ" process - FI/CC modes
cd('C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\MATLAB');
dat = load('CCSSs/assembly_1');
ccs = dat.ccss;
dat = load('ABINTs/assembly_1');
ab = dat.abint;

plate9x6 = 2; plate9x9 = 3; % Panel part numbers
nodeset9x6 = ab.nodeSets.('PLATE9X6-1_ALLNODES');
nodeset9x9 = ab.nodeSets.('PLATE9X9-1_ALLNODES');
bcset = ab.nodeSets.('FRAME-1_BCSET');
nBC = length(bcset);
boundary = [bcset(:), ones(nBC, 3), zeros(nBC, 3)];
refNode9x6 = 364; % Abaqus reference node corresponding to panel component ref node
refNode9x9 = 611; % Abaqus reference node corresponding to panel component ref node

ccs = ccs.initNLSystem();
% NLROM for 9x6 plate
[ccs, fd.fit9x6] = ccs.addNLPartIS(ab, nodeset9x6, boundary, refNode9x6, plate9x6, 1:5, 1:10, 1, 1);
% NLROM for 9x9 plate
[ccs, fd.fit9x9] = ccs.addNLPartIS(ab, nodeset9x9, boundary, refNode9x9, plate9x9, 1:5, 1:10, 1, 1);


ccs.save('assembly_1_ficc_1-5_1-5_isv2');



%% Make substructure using "in situ" process - FI/CC Modes, no membrane
cd('C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\MATLAB');
dat = load('CCSSs/assembly_1');
ccs = dat.ccss;
dat = load('ABINTs/assembly_nomembrane9x6');
nomembrane9x6 = dat.abint;
dat = load('ABINTs/assembly_nomembrane9x9');
nomembrane9x9 = dat.abint;

plate9x6 = 2; plate9x9 = 3; % Panel part numbers
nodeset9x6 = nomembrane9x9.nodeSets.('PLATE9X6-1_ALLNODES');
nodeset9x9 = nomembrane9x6.nodeSets.('PLATE9X9-1_ALLNODES');
bcset = nomembrane9x9.nodeSets.('FRAME-1_BCSET');
nBC = length(bcset);
boundary = [bcset(:), ones(nBC, 3), zeros(nBC, 3)];
refNode9x6 = 364; % Abaqus reference node corresponding to panel component ref node
refNode9x9 = 611; % Abaqus reference node corresponding to panel component ref node

ccs = ccs.initNLSystem();
% NLROM for 9x6 plate
[ccs, fd.fit9x6] = ccs.addNLPartIS(nomembrane9x9, nodeset9x6, boundary, refNode9x6, plate9x6, 1:5, 1:5, 1);
% NLROM for 9x9 plate
[ccs, fd.fit9x9] = ccs.addNLPartIS(nomembrane9x6, nodeset9x9, boundary, refNode9x9, plate9x9, 1:5, 1:5, 1);


ccs.save('assembly_1_1-5_1-5_isnomembrane');



%% Make substructure using "in situ" process - FI Modes; 9x9 Panel
cd('C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\MATLAB');
dat = load('CCSSs/assembly_1');
ccs = dat.ccss;
dat = load('ABINTs/assembly_1');
ab = dat.abint;

plate9x9 = 2; % Panel part numbers
nodeset9x9 = ab.nodeSets.('PLATE9X9-1_ALLNODES');
bcset = ab.nodeSets.('FRAME-1_BCSET');
refNode9x9 = 364; % Abaqus reference node corresponding to panel component ref node

ccs = ccs.initNLSystem();
% NLROM for 9x6 plate
% NLROM for 9x9 plate
[ccs, fitDat] = ccs.addNLPartIS(ab, nodeset9x9, bcset, refNode9x9, plate9x9, 1:3, 1:2, 1);


ccs.save('assembly_9x9_ficc_1-3_1-2_is');
% save('fitData_qrDiag', 'fitDat');


%% Make substructure using "in situ" process - QR Modes; 9x9 Panel
cd('C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\MATLAB');
dat = load('CCSSs/assembly_9x9');
ccs = dat.ccss;
dat = load('ABINTs/assembly_9x9');
ab = dat.abint;

plate9x9 = 2; % Panel part numbers
nodeset9x9 = ab.nodeSets.('PLATE9X9-1_ALLNODES');
bcset = ab.nodeSets.('FRAME-1_BCSET');
nBC = length(bcset);
boundary = [bcset(:), ones(nBC, 3), zeros(nBC, 3)];
refNode9x9 = 364; % Abaqus reference node corresponding to panel component ref node

ccs = ccs.initNLSystem();
% NLROM for 9x6 plate
% NLROM for 9x9 plate
[ccs, fitDat] = ccs.addNLPartQRIS(ab, nodeset9x9, boundary, refNode9x9, plate9x9, [1:10], 1);


ccs.save('assembly_9x9_qr_1-10_is');
% save('fitData_qrDiag', 'fitDat');



%% Make substructure using "in situ" process - QR vectors
cd('C:\Users\Joe\Documents\Projects\Plate Frame Substructuring\MATLAB');
dat = load('CCSSs/assembly_1');
ccs = dat.ccss;
dat = load('ABINTs/assembly_1');
ab = dat.abint;

plate9x6 = 2; plate9x9 = 3; % Panel part numbers
nodeset9x6 = ab.nodeSets.('PLATE9X6-1_ALLNODES');
nodeset9x9 = ab.nodeSets.('PLATE9X9-1_ALLNODES');
bcset = ab.nodeSets.('FRAME-1_BCSET');
nBC = length(bcset);
boundary = [bcset(:), ones(nBC, 3), zeros(nBC, 3)];
refNode9x6 = 364; % Abaqus reference node corresponding to panel component ref node
refNode9x9 = 611; % Abaqus reference node corresponding to panel component ref node

ccs = ccs.initNLSystem();
% NLROM for 9x6 plate
flist = repmat([1:15]', 1, 3);
[ccs, fd.fit9x6] = ccs.addNLPartQRIS(ab, nodeset9x6, boundary, refNode9x6, plate9x6, [1:15], 1, 0, 0, flist);
% NLROM for 9x9 plate
[ccs, fd.fit9x9] = ccs.addNLPartQRIS(ab, nodeset9x9, boundary, refNode9x9, plate9x9, [1:15], 1, 0, 0, flist);


ccs.save('assembly_1_qr1-10');


