% Wrapper script for substructure testing and development
% Hit "F5" to change directory

clear; clc; close all
clear classes;
addpath('..');
return;

%% CCSS Generation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Generate CCSS object for frame

models = load('CBICEs/frame');
frame = models.cbice;

models = load('CBICEs/plate9x6');
plate96 = models.cbice;

models = load('CBICEs/plate9x9');
plate99 = models.cbice;

filepath = 'CCSSs'; 
filename = 'assembly_1_true';
ccs = CCSS(filepath, filename);


% Add frame
refNode = 10;
boundaryNodes = frame.abint.nodeSets.('FRAME-1_BCSET');
boundary = [boundaryNodes, ones(length(boundaryNodes), 3), zeros(length(boundaryNodes), 3)];
connNodes = frame.abint.nodeSets.('FRAME-1_RETAINEDDOFS');
conn = [connNodes, ones(length(connNodes), 6)];
location = [0, 0, 0];
frange = 7;
ccs = ccs.addPart(frame, refNode, location, boundary, conn, frange);

% Add 9x6 plate
refNode = 1;
boundary = [];
connNodes = plate96.abint.nodeSets.('PLATE9X6_RETAINEDDOFS');
conn = [connNodes, ones(length(connNodes), 6)];
location = [1, 0.5, 0];
frange = 7;
ccs = ccs.addPart(plate96, refNode, location, boundary, conn, frange);

% Add 9x9 plate
refNode = 1;
boundary = [];
connNodes = plate99.abint.nodeSets.('PLATE9X9_RETAINEDDOFS');
conn = [connNodes, ones(length(connNodes), 6)];
location = [1, 7.5, 0];
frange = 7;
ccs = ccs.addPart(plate99, refNode, location, boundary, conn, frange);


% Build the connection array
ccs = ccs.makeConnections(1E-2); 
ccs.plot();


% Build the CB model of each part
ccs = ccs.buildCBModels('abaqus');

% Load full-order modal results
odbpath = '..\Abaqus\ODB';
odbfile = 'assembly_1';
ccs = ccs.loadOdb(odbpath, odbfile, 'C:\Users\Joe\Dropbox\MultiBayPanel\python');

% Generate CC modal model and compare
ccs = ccs.buildCCSystem(20, 15);
ccs = ccs.makeMac();
ccs.comparePlot(2.5, 20);
ccs.save();



%% ABINT/CBICE Generation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Generate ABINT/CBICE for Panel
workingDir = '../scratch';
filepathAbint = 'ABINTs';
filepathCbice = 'CBICEs';
pythonpath = 'C:\Users\Joe\Dropbox\MultiBayPanel\python';


% Make ABINT object and save for later
ab = ABINT(workingDir, filepathAbint, pythonpath);
% Read an input file
inputPath = '..\Abaqus\models';
inputFile = 'frame';
ab = ab.readInp(inputPath, inputFile);
ab = ab.getDof();
[M, K] = ab.matrix([]);
save(ab);


% Make CBICE object and save
thick = 1.0;
cbi = CBICE(ab, thick, workingDir, filepathCbice);
cbi.linModel.M = M; cbi.linModel.K = K;
save(cbi);

