% CCSS(filepath, filename) 
% 
% CCSS - Characteristic Constraint Substructuring Module
% 
% Inherits from CBSS
%
% CCSS enables linear substructuring via characteristic constraint modes. The class 
% performs appropritate system partition, calls for secondary modal analysis of the 
% assembly boundaries, and applies the CC modal reduction to the assembly boundary modes.
% All nonlinear model generation is also handled through CCSS, with both FI/CC and QR
% vector methods available, each of which may be used via boundary stiffness reduction
% or the "in situ" NLROM generation process.
%
% Dependencies: 
%
%        ABINT.m         Abaqus interface class
%        CBSS.m          Craig-Bampton Substructuring parent class
%        odb2mat_v2.py   Used to read full-order model odb for MAC check
%
% Use "doc CCSS" for detailed description of constructor, properties, and
% methods.
 
% Joe Schoneman
% UW - Madison, Department of Engineering Physics
% 25 August, 2016

classdef CCSS < CBSS

    properties(GetAccess = 'public', SetAccess = 'public')
        
        Mcc;        % CC mass matrix
        Kcc;        % CC stiffness matrix
        phiCB;      % Original Craig-Bampton modal matrix
        psiCC;      % Matrix of characteristic constraint modes
        fnCC;       % "Frequencies" of the CC modes
        phiCC;      % Resulting modal matrix using characteristic constraint modes
                    % Note, the corresponding frequencies are "fn"
        Tcc;        % CC transformation matrix
        modalCB;    % Modal DOF indices for CB matrix
        boundaryCB; % Boundary DOF indices for CB matrix
        nCharDOF;   % Number of characteristic constraint modes retained
        ccBasis;    % Don't believe this is used but I don't want to remove it just in case
        
        % Logical values
        cbModel = false; % Last SS was a CB model
        ccModel = false; % Last SS was a CC model
        
       
    end
    
    
    methods
        % Constructor
        function obj = CCSS(filepath, filename)
            %    obj = CCSS(filepath, filename)
            %
            % CCSS constructor; identical to CBSS constructor.
            %
            % INPUT
            %
            % filepath: Path used to save CBSS object
            %
            % filename: Name used to save CBSS object
            
            % Call CBSS constructor
            obj@CBSS(filepath, filename);
            
        end
        
        function obj = initNLSystem(obj)
            %    obj = initNLSystem(obj)
            %
            % Initialize an NL system by setting all part nonlinearities to
            % "0."
            
            for i = 1:obj.npart
                obj.parts(i).part = obj.parts(i).part.zeroNLROM(obj.nCharDOF);
            end
        end
        
        function obj = copyNLPart(obj, basePart, copyPart)
            %    obj = copyNLPart(obj, basePart, copyPart)
            %
            % Copy the NL model of "basePart" to that of "copyPart."
            %
            % INPUT
            %
            % basePart:  Nonlinear model to be copied.
            %
            % copyPart:  Part to which the nonlinear model is copied.
            
            obj.parts(copyPart).part.NLROM = obj.parts(basePart).part.NLROM;
        end
        
        function [obj, fitData] = addNLPartIS(obj, ab, nodeset, bcset, refNode, basePart, fiMind, ccMind, thick, quads, triples, flist, fitData)
            %    [obj, fitData] = addNLPartIS(obj, ab, nodeset, bcset, refNode, basePart, fiMind, ccMind, thick, quads, triples, flist, fitData)
            %
            % Use the FI/CC modeset to obtain a
            % nonlinear model of the structure's restoring force. In the
            % descriptions below, mFI is the number of FI modes in the
            % structure, mCC is the number of CC modes in the component,
            % and m = mFI + mCC is the total number of modes in the
            % component model. nFI and nCC are the number of modes selected
            % to retain in the NLROM; must have nFI < mFI and nCC < nCC. 
            %
            % This version uses the finite element "in situ" process in
            % order to perform the NLROM fit.
            %
            % INPUT
            %
            %
            % ab:      An ABINT object corresponding to the full assembly
            %          which is used for "in situ" simulation.
            %
            % nodeset: [nNodes x 1] The nodeset within "ab" which
            %          corresponds to the component of interest. 
            %
            % bcset:   [nBC x 7] Boundary condition set for the full
            %          assembly. First column is BC node (actual), columns 
            %          2-6 are the associated DOF. "1" is constrained; "0" 
            %          unconstrained.
            %
            % refNode: Reference node on the Abaqus assembly which
            %          corresponds to the component reference node.
            %
            % basePart: Index of part for which the NLROM is being
            %           constructed. 
            %
            % fiMind: [nFI x 1] Indices of the FI modes to retain.
            %
            % ccMind: [nCC x 1] Indices of the CC modes to retain.
            %
            % thick:    [1 x 1] Deflection level to push each QR mode to. 
            %
            % quads:    [boolean] Include quadratic fit terms?
            %
            % triples:  [boolean] Include cubic fit terms?
            %
            % flist:    [3 x nFit] Vector of polynomial terms to create fit
            %           coefficients for.
            %
            % fitData:  Structure of the same form as output "data" which
            %           contains FEA load case results that can be used to
            %           perform a fit using different parameters without
            %           running another set of FEA load cases.
            %
            % OUTPUT
            %
            % fitDat:   Structure containing load case force and response
            %           data that can be saved for postprocessing or reuse.
            
            % Input handling
            if (nargin < 10); quads = obj.parts(basePart).part.options.quads; end
            if (nargin < 11); triples = obj.parts(basePart).part.options.triple; end
            if (nargin < 12); flist = []; end
            if (nargin < 13); fitData = []; end
                
            % Get the "part-ordered" set of CC modes
            [~, cc2cbConvert] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
            nC = obj.nCharDOF;
            if (length(ccMind) > nC); error('More CC modes requested than available'); end
            TccOrdered = obj.L*obj.Tcc(cc2cbConvert, (end - nC + 1):end);
            
            % Get global DOF of the part in order to obtain the correct CC
            % mode partition
            currDOF = 1;
            for i = 1:(basePart - 1)
                endDOF = currDOF + obj.parts(i).nModalDOF + obj.parts(i).nBoundaryDOF;
                currDOF = endDOF;
            end
            
            i = basePart;
            nM = obj.parts(i).nModalDOF; nB = obj.parts(i).nBoundaryDOF;
            if (length(fiMind) > nM); error('More FI modes requested than available'); end
            endDOF = currDOF + nM + nB;
            springs.dof = obj.parts(i).part.linModel.connDofAbs;
            [~, springs.k] = obj.interfaceReduction(i);
            
            % Create a basis in terms of the part's FI and CC modes
            psiChar = TccOrdered(((currDOF + nM):endDOF - 1), :);
%             fiBasis = [eye(nM); zeros(nB, nM)];
            ccBasis = [zeros(nM, nC); psiChar];
%             basis = [fiBasis(:, fiMind), ccBasis(:, fiMind)];
            rPart = obj.parts(i).part.abint.getAbsNodeNumber(obj.parts(i).refNode);
            rAb = ab.getAbsNodeNumber(refNode);
            refLoc = [rPart, rAb];
            % "refLoc" is [component reference node, ABINT reference node]
            [obj.parts(i).part, fitData] = obj.parts(i).part.ficcFitIS(refLoc, ...
                ab, nodeset, bcset, fiMind, ccMind, ccBasis, springs, thick, quads, triples, flist, fitData);            
                    
        end
        
         function [obj, fitData] = addNLPartQRIS(obj, ab, nodeset, bcset, refNode, basePart, qrInds, thick, quads, triples, flist, fitData)
            %     [obj, fitData] = addNLPartQRIS(obj, ab, nodeset, bcset, refNode, basePart, qrInds, thick, quads, triples, flist, fitData)
            % 
            % Use a QR decomposition of the FI/CC modeset to obtain a
            % nonlinear model of the structure's restoring force. In the
            % descriptions below, mFI is the number of FI modes in the
            % structure, mCC is the number of CC modes in the component,
            % and m = mFI + mCC is the total number of modes in the
            % component model. mQR is the number of modes in the requested
            % QR basis; must have mQR < m.
            %
            % This version uses the finite element "in situ" process in
            % order to perform the NLROM fit.
            %
            % INPUT
            %
            % ab:      An ABINT object corresponding to the full assembly
            %          which is used for "in situ" simulation.
            %
            % nodeset: [nNodes x 1] The nodeset within "ab" which
            %          corresponds to the component of interest. 
            %
            % bcset:   [nBC x 7] Boundary condition set for the full
            %          assembly. First column is BC node (actual), columns 
            %          2-6 are the associated DOF. "1" is constrained; "0" 
            %          unconstrained.
            %
            % refNode: Reference node on the Abaqus assembly which
            %          corresponds to the component reference node.
            %
            % qrInds: [nQR x 1] Absolute indices of QR vetors to retain.
            %
            % thick:    [1 x 1] Deflection level to push each QR mode to. 
            %
            % quads:    [boolean] Include quadratic fit terms?
            %
            % triples:  [boolean] Include cubic fit terms?
            %
            % flist:    [3 x nFit] Vector of polynomial terms to create fit
            %           coefficients for.
            %
            % fitData:  Structure of the same form as output "data" which
            %           contains FEA load case results that can be used to
            %           perform a fit using different parameters without
            %           running another set of FEA load cases.
            %
            % OUTPUT
            %
            % fitData:  Structure containing load case force and response
            %           data that can be saved for postprocessing or reuse.
            
            % Input handling
            if (nargin < 9); quads = obj.parts(basePart).part.options.quads; end
            if (nargin < 10); triples = obj.parts(basePart).part.options.triple; end
            if (nargin < 11); flist = []; end
            if (nargin < 12); fitData = []; end
                
            % Get the "part-ordered" set of CC modes
            [~, cc2cbConvert] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
            nC = obj.nCharDOF;
            TccOrdered = obj.L*obj.Tcc(cc2cbConvert, (end - nC + 1):end);
            
            % Get global DOF of the part in order to obtain the correct CC
            % mode partition
            currDOF = 1;
            for i = 1:(basePart - 1)
                endDOF = currDOF + obj.parts(i).nModalDOF + obj.parts(i).nBoundaryDOF;
                currDOF = endDOF;
            end
            
            i = basePart;
            nM = obj.parts(i).nModalDOF; nB = obj.parts(i).nBoundaryDOF;
            
            endDOF = currDOF + nM + nB;
            
            % Get the linearized base stiffness of the system
            springs.dof = obj.parts(i).part.linModel.connDofAbs;
            [~, springs.k] = obj.interfaceReduction(i);
            
            % Create a basis in terms of the part's FI and CC modes
            psiChar = TccOrdered(((currDOF + nM):endDOF - 1), :);
%             fiBasis = [eye(nM); zeros(nB, nM)];
            ccBasis = [zeros(nM, nC); psiChar];
%             basis = [fiBasis(:, fiMind), ccBasis(:, fiMind)];
            rPart = obj.parts(i).part.abint.getAbsNodeNumber(obj.parts(i).refNode);
            rAb = ab.getAbsNodeNumber(refNode);
            
            refLoc = [rPart, rAb];
            % "refLoc" is [component reference node, ABINT reference node]
            [obj.parts(i).part, fitData] = obj.parts(i).part.qrFitIS(refLoc, ...
                ab, nodeset, bcset, qrInds, ccBasis, springs, thick, quads, triples, flist, fitData);            
                    
        end
        
        function [obj, fitData] = addNLPart(obj, basePart, fiMind, ccMind, spring, thick, quads, triples, flist, fitData)
            %    [obj, fitData] = addNLPart(obj, basePart, fiMind, ccMind, spring, thick, quads, triples, flist, fitData)
            %
            % Use the FI/CC modeset to obtain a
            % nonlinear model of the structure's restoring force. In the
            % descriptions below, mFI is the number of FI modes in the
            % structure, mCC is the number of CC modes in the component,
            % and m = mFI + mCC is the total number of modes in the
            % component model. nFI and nCC are the number of modes selected
            % to retain in the NLROM; must have nFI < mFI and nCC < nCC. 
            %
            % This version uses the finite element "in situ" process in
            % order to perform the NLROM fit.
            %
            % INPUT
            %
            % basePart: Index of part for which the NLROM is being
            %           constructed. 
            %
            % fiMind: [nFI x 1] Indices of the FI modes to retain.
            %
            % ccMind: [nCC x 1] Indices of the CC modes to retain.
            %
            % spring:   Boolean flag for whether or not to include the
            %           reduced boundary stiffness for the component fit.
            %
            % thick:    [1 x 1] Deflection level to push each QR mode to. 
            %
            % quads:    [boolean] Include quadratic fit terms?
            %
            % triples:  [boolean] Include cubic fit terms?
            %
            % flist:    [3 x nFit] Vector of polynomial terms to create fit
            %           coefficients for.
            %
            % fitData:  Structure of the same form as output "data" which
            %           contains FEA load case results that can be used to
            %           perform a fit using different parameters without
            %           running another set of FEA load cases.
            %
            % OUTPUT
            %
            % fitData:  Structure containing load case force and response
            %           data that can be saved for postprocessing or reuse.
            
            % Input handling
            if (nargin < 7); quads = obj.parts(basePart).part.options.quads; end
            if (nargin < 8); triples = obj.parts(basePart).part.options.triple; end
            if (nargin < 9); flist = []; end
            if (nargin < 10); fitData = []; end
                % Note: FImind only applies to basePart
                
            % Get the "part-ordered" set of CC modes
            [~, cc2cbConvert] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
            nC = obj.nCharDOF;
            if (length(ccMind) > nC); error('More CC modes requested than available'); end
            TccOrdered = obj.L*obj.Tcc(cc2cbConvert, (end - nC + 1):end);
            
            % Get global DOF of the part in order to obtain the correct CC
            % mode partition
            currDOF = 1;
            for i = 1:(basePart - 1)
                endDOF = currDOF + obj.parts(i).nModalDOF + obj.parts(i).nBoundaryDOF;
                currDOF = endDOF;
            end
                % MSA: It seems that the base part must be the last, and
                % then this counts up how many DOF are before this one.
                % This could be improved - we should store indices that
                % tell us all of this when creating the models.
            
            i = basePart;
            nM = obj.parts(i).nModalDOF; nB = obj.parts(i).nBoundaryDOF;
            if (length(fiMind) > nM); error('More FI modes requested than available'); end
            endDOF = currDOF + nM + nB;
            
            springs.dof = obj.parts(i).part.linModel.connDofAbs;
            [~, springs.k] = obj.interfaceReduction(i);
            % You can use the line below to essentially "fix" the boundary
            % by including extremely stiff springs.
%             if ~spring; springs.k = 1E6*springs.k; warning('Springs multiplied by 1 million'); end
            if ~spring; springs.k = 0; end
            
            % Create a basis in terms of the part's FI and CC modes
            psiChar = TccOrdered(((currDOF + nM):endDOF - 1), :);
%             fiBasis = [eye(nM); zeros(nB, nM)];
            ccBasis = [zeros(nM, nC); psiChar];
%             basis = [fiBasis(:, fiMind), ccBasis(:, fiMind)];
            [obj.parts(i).part, fitData] = obj.parts(i).part.ficcFit(fiMind, ccMind, ccBasis, springs, thick, quads, triples, flist, fitData);            
            
            
        end
        
          function [obj, fitData] = addNLPartSVD(obj, basePart, nSvd, springs, thick, quads, triples, flist, fitData)
            %      [obj, fitData] = addNLPartSVD(obj, basePart, nSvd, springs, thick, quads, triples, flist, fitData)
            %
            % Build nonlinear model of a part using the SVD reduction.
            % Depcrecated, remove when possible.
            
            % Input handling
            if (nargin < 6); quads = []; end
            if (nargin < 7); triples = []; end
            if (nargin < 8); flist = []; end
            if (nargin < 9); fitData = []; end
                
            % Get the "part-ordered" set of CC modes
            [~, cc2cbConvert] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
            nC = obj.nCharDOF;
            
            TccOrdered = obj.L*obj.Tcc(cc2cbConvert, (end - nC + 1):end);
            
            % Get global DOF of the part in order to obtain the correct CC
            % mode partition
            currDOF = 1;
            for i = 1:(basePart - 1)
                endDOF = currDOF + obj.parts(i).nModalDOF + obj.parts(i).nBoundaryDOF;
                currDOF = endDOF;
            end
            
            i = basePart;
            nM = obj.parts(i).nModalDOF; nB = obj.parts(i).nBoundaryDOF;

            endDOF = currDOF + nM + nB;
            
            
            
            % Get the static equivalent stiffness of the part if springs
            % are not supplied
            if (nargin < 3) % None of this works right now
                xCBDOF = (currDOF + nM):(endDOF - 1);
                xADOF = [];
                for j = xCBDOF
                    xADOF = [xADOF; find(obj.L(j, :))];
                end
                xEDOF = (1:(size(obj.L, 2)))';
                for j = xADOF
                    xEDOF(j) = 0;
                end
                xEDOF(xEDOF == 0) = [];

                % Partition matrices
                KA = obj.L'*obj.Ksys*obj.L;
                cDof = obj.parts(i).part.linModel.connDofAbs;
                Kaa = KA(xADOF, xADOF) - obj.parts(i).part.linModel.K(cDof, cDof); % Kae = KA(xADOF, xEDOF);
    %             Kea = Kae'; Kee = KA(xEDOF, xEDOF);
    %             Te = -Kee\Kea;
    %             Ka = [Kaa, Kae; Kea, Kee]*[eye(length(xADOF), size(Te, 2)); Te];
    %             
                springs.dof = obj.parts(i).part.linModel.connDofAbs;
                nNodes = length(springs.dof)/6;
                springs.k = full(diag(Kaa));
            end
            
            % Create a basis in terms of the part's FI and CC modes
            psiChar = TccOrdered(((currDOF + nM):endDOF - 1), :);
%             fiBasis = [eye(nM); zeros(nB, nM)];
            ccBasis = [zeros(nM, nC); psiChar];
%             basis = [fiBasis(:, fiMind), ccBasis(:, fiMind)];
            [obj.parts(i).part, fitData] = obj.parts(i).part.svdFit(nSvd, ccBasis, springs, thick, quads, triples, flist, fitData);            
            
            
          end
        
          
          function [obj, fitData] = addNLPartQR(obj, basePart, qrInds, spring, thick, quads, triples, flist, fitData)
            %      [obj, fitData] = addNLPartQR(obj, basePart, qrInds, spring, thick, quads, triples, flist, fitData)
            % 
            % Use a QR decomposition of the FI/CC modeset to obtain a
            % nonlinear model of the structure's restoring force. In the
            % descriptions below, mFI is the number of FI modes in the
            % structure, mCC is the number of CC modes in the component,
            % and m = mFI + mCC is the total number of modes in the
            % component model. mQR is the number of modes in the requested
            % QR basis; must have mQR < m.
            %
            % This version uses the finite element "in situ" process in
            % order to perform the NLROM fit.
            %
            % INPUT
            %
            % basePart: The part which is treated as nonlinear. 
            % qrInds: [nQR x 1] Absolute indices of QR vetors to retain.
            %
            % spring:   Boolean flag for whether or not to include the
            %           reduced boundary stiffness for the component fit.
            %
            % thick:    [1 x 1] Deflection level to push each QR mode to. 
            %
            % quads:    [boolean] Include quadratic fit terms?
            %
            % triples:  [boolean] Include cubic fit terms?
            %
            % flist:    [3 x nFit] Vector of polynomial terms to create fit
            %           coefficients for.
            %
            % fitData:  Structure of the same form as output "data" which
            %           contains FEA load case results that can be used to
            %           perform a fit using different parameters without
            %           running another set of FEA load cases.
            %
            % OUTPUT
            %
            % fitData:  Structure containing load case force and response
            %           data that can be saved for postprocessing or reuse.
            
            % Input handling
            if (nargin < 6); quads = []; end
            if (nargin < 7); triples = []; end
            if (nargin < 8); flist = []; end
            if (nargin < 9); fitData = []; end
                
            % Get the "part-ordered" set of CC modes
            [~, cc2cbConvert] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
            nC = obj.nCharDOF;
            
            TccOrdered = obj.L*obj.Tcc(cc2cbConvert, (end - nC + 1):end);
            
            % Get global DOF of the part in order to obtain the correct CC
            % mode partition
            currDOF = 1;
            for i = 1:(basePart - 1)
                endDOF = currDOF + obj.parts(i).nModalDOF + obj.parts(i).nBoundaryDOF;
                currDOF = endDOF;
            end
            
            i = basePart;
            nM = obj.parts(i).nModalDOF; nB = obj.parts(i).nBoundaryDOF;

            endDOF = currDOF + nM + nB;            
            
            springs.dof = obj.parts(i).part.linModel.connDofAbs;
            [~, springs.k] = obj.interfaceReduction(i);
            if ~spring; springs.k = 0*springs.k; end
            % Create a basis in terms of the part's FI and CC modes
            psiChar = TccOrdered(((currDOF + nM):endDOF - 1), :);
%             fiBasis = [eye(nM); zeros(nB, nM)];
            ccBasis = [zeros(nM, nC); psiChar];
%             basis = [fiBasis(:, fiMind), ccBasis(:, fiMind)];
            [obj.parts(i).part, fitData] = obj.parts(i).part.qrFit(qrInds, ccBasis, springs, thick, quads, triples, flist, fitData);            
            
            
        end
        
        function obj = buildCCSystem(obj, nModes, nCModes)
            %    obj = buildCCSystem(nModes, nCModes)
            %
            % Build a system using characteristic constraint modes.
            %
            % INPUT
            %
            % nModes:  Number of modes to compute
            %
            % nCModes: Number of CC modes to retain
            
            % Build B, L, matrix etc.
            obj = obj.buildBL();
            
            % Partition mass and stiffness matrices to the boundary
            % set.
            obj.nCharDOF = nCModes;
            dofCount = sum(obj.L, 1);
            obj.modalCB = find(dofCount == 1);   % Modal DOF in the model
            obj.boundaryCB = find(dofCount > 1); % Connection DOF in the model
            [~, cc2cbConvert] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
            
            % Get assembled mass and stiffness matrices
            Ma = obj.L'*obj.Msys*obj.L;
            Ka = obj.L'*obj.Ksys*obj.L;
            
            % Partition them to boundary DOF
            Mii = Ma(obj.modalCB, obj.modalCB);
            Kii = Ka(obj.modalCB, obj.modalCB);
            Mib = Ma(obj.modalCB, obj.boundaryCB);
            Kib = Ka(obj.modalCB, obj.boundaryCB);
            Mbb = Ma(obj.boundaryCB, obj.boundaryCB);
            Kbb = Ka(obj.boundaryCB, obj.boundaryCB);
            
            % Perform eigenvalue analysis
            [obj.psiCC, obj.fnCC] = eigs(Kbb, Mbb, nCModes, 'SM');
            obj.fnCC = sqrt(diag(obj.fnCC))/2/pi;
            
            % Construct CC transformation matrix
            I = eye(obj.nModalDOF); 
            Zi = zeros(obj.nModalDOF, nCModes);
            Zb = zeros(length(obj.boundaryCB), obj.nModalDOF);
            obj.Tcc = [I, Zi; Zb, obj.psiCC];
            
            % Further transform CB matrix; perform modal
            obj.Mcc = obj.Tcc'*[Mii, Mib; Mib', Mbb]*obj.Tcc;
            obj.Kcc = obj.Tcc'*[Kii, Kib; Kib', Kbb]*obj.Tcc;
            
            [obj.phiCC, fn] = eigs(obj.Kcc, obj.Mcc, nModes, 'SM');
            obj.fn = sqrt(diag(fn))/2/pi;
            
            % Sort modes to ascending order
            [obj.fn, modeOrder] = sort(obj.fn);
            obj.phiCC = obj.phiCC(:, modeOrder);
            
            
            % Convert the CC modal matrix back to Craig-Bampton "phi" for
            % plotting and MAC comparison
            obj.phi = obj.Tcc*obj.phiCC; % Transform
            obj.phi = obj.L*obj.phi(cc2cbConvert, :); % Re-order
            
            % Assign modal amplitudes to each part
            obj = obj.assignAmplitudes();
            
            obj.cbModel = false;
            obj.ccModel = true;
        end

        % Build L-CC model, Local CC modes, by MSA 2016-08-05
        function obj = buildLCCSystem(obj, nModes, nCModes)
            %    obj = buildCCSystem(nModes, nCModes)
            %
            % Build a system using characteristic constraint modes. Inputs
            % are the number of modes to compute and the number of CC modes
            % to retain.
            %
            % Added by MSA on 2016-08-04
            
            % Build B, L, matrix etc.
            obj = obj.buildBL();
                % This will be the B matrix in physical coordinates
                % Bred=B*blkdiag(I,Im,I,pinv(psi_CC^A)*psi_CC^B,...)
                
            % Perform eigenvalue analysis on each interface to find L-CC
            % modes.
            TB=[];
            for i=1:obj.npart
                % MSA: This shouldn't really need eigs - should switch to eig
                [PhiLCC,lam]=eigs(obj.parts(i).part.linModel.KbbHat,...
                    obj.parts(i).part.linModel.MbbHat, nCModes, 'SM');
                
                    obj.parts(i).part.linModel.fnLCC=sqrt(diag(lam))/2/pi;
                    obj.parts(i).part.linModel.PhiLCC=PhiLCC;
                    
                    TB=blkdiag(TB,eye(length(obj.parts(i).part.linModel.omegaN)),PhiLCC);
                    obj.parts(i).part.linModel.TLCC=blkdiag(eye(length(obj.parts(i).part.linModel.omegaN)),PhiLCC);
                    
            end
            obj.Bm=obj.B*TB;
            obj.Lm=null(Bm); % ? Need to Implement MSA's alternatives like SVD?
            obj.Mlcc = obj.Lm'*obj.Msys*obj.Lm;
            obj.Klcc = obj.Lm'*obj.Ksys*obj.Lm;
            obj.Tcc=TB; % Transformation from CC coord to original
            
            % Modes of L-CC System
            [obj.phiCC, fn] = eig(obj.Klcc, obj.Mlcc);
            obj.fn = sqrt(diag(fn))/2/pi;
                % Sort modes to ascending order
                [obj.fn, modeOrder] = sort(obj.fn);
                obj.phiCC = obj.phiCC(:, modeOrder);

                % Convert the L-CC modal matrix back to Craig-Bampton "phi" for
                % plotting and MAC comparison
                obj.phi = obj.Tcc*obj.phiCC; % Transform
                    % From [q_FI^A; q_CC^A; q_FI^B; ...] to  [q_FI^A; x_b^A; q_FI^B; x_b^B;...]  
                    
                    %%% LEFT OFF HERE - What format should that be in??
                    
                obj.phi = obj.L*obj.phi(cc2cbConvert, :); % Re-order
            
            % Assign modal amplitudes to each part
            obj = obj.assignAmplitudes();
            
            obj.cbModel = false;
            obj.ccModel = true;
%             obj.lccModel = true; % MSA: Do this instead?

            return
            
            % %%%%%%%%%%%%%%%%%%%%%%%%%% LEFT OFF HERE  %%%%%%%%%%%%%%%
            

            
%             % Partition mass and stiffness matrices to the boundary
%             % set.
%             obj.nCharDOF = nCModes;
%             dofCount = sum(obj.L, 1);
%             obj.modalCB = find(dofCount == 1);   % Modal DOF in the model
%             obj.boundaryCB = find(dofCount > 1); % Connection DOF in the model
%             [~, cc2cbConvert] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
%             
%             % Get assembled mass and stiffness matrices
%             Ma = obj.L'*obj.Msys*obj.L;
%             Ka = obj.L'*obj.Ksys*obj.L;
%             
%             % Partition them to boundary DOF
%             Mii = Ma(obj.modalCB, obj.modalCB);
%             Kii = Ka(obj.modalCB, obj.modalCB);
%             Mib = Ma(obj.modalCB, obj.boundaryCB);
%             Kib = Ka(obj.modalCB, obj.boundaryCB);
%             Mbb = Ma(obj.boundaryCB, obj.boundaryCB);
%             Kbb = Ka(obj.boundaryCB, obj.boundaryCB);
%             
%             % Perform eigenvalue analysis
%             [obj.psiCC, obj.fnCC] = eigs(Kbb, Mbb, nCModes, 'SM');
%             obj.fnCC = sqrt(diag(obj.fnCC))/2/pi;
            
%             % Construct CC transformation matrix
%             I = eye(obj.nModalDOF); 
%             Zi = zeros(obj.nModalDOF, nCModes);
%             Zb = zeros(length(obj.boundaryCB), obj.nModalDOF);
%             obj.Tcc = [I, Zi; Zb, obj.psiCC];
            
%             % Further transform CB matrix; perform modal
%             obj.Mcc = obj.Tcc'*[Mii, Mib; Mib', Mbb]*obj.Tcc;
%             obj.Kcc = obj.Tcc'*[Kii, Kib; Kib', Kbb]*obj.Tcc;
%             [obj.phiCC, fn] = eigs(obj.Kcc, obj.Mcc, nModes, 'SM');
%             obj.fn = sqrt(diag(fn))/2/pi;
%             
%             % Sort modes to ascending order
%             [obj.fn, modeOrder] = sort(obj.fn);
%             obj.phiCC = obj.phiCC(:, modeOrder);
            
            
            % Convert the CC modal matrix back to Craig-Bampton "phi" for
            % plotting and MAC comparison
            obj.phi = obj.Tcc*obj.phiCC; % Transform
            obj.phi = obj.L*obj.phi(cc2cbConvert, :); % Re-order
            
            % Assign modal amplitudes to each part
            obj = obj.assignAmplitudes();
            
            obj.cbModel = false;
            obj.ccModel = true;
        end

        
        function fig = comparePlot(obj, maxErr, nModes, cmap)
            %    fig = comparePlot(obj, maxErr, nModes, cmap)
            %
            % Slightly modified from CBSS 'comparePlot' to change plot
            % title. 
            %
            % Compare full order FEA frequencies and mode shapes to those
            % from a CB or CC model. Also displays a comparison summary 
            % image demonstrating both the MAC and the frequency error with
            % mode comparisons based on the MAC.
            %
            % INPUT
            %
            % maxErr:   Maximum error percentage to display on the
            %           frequency error bar chart. Default 2.5%.
            %
            % nModes:   Number of modes to use for the MAC/frequency error
            %           plot.
            %
            % cmap:     Colormap (i.e. 'parula') to use for the
            %           MAC/frequency error plot. Default 'jet'.
            if nargin < 2; maxErr = 2.5; end
            if nargin < 3; nModes = size(obj.phiFull, 2); end
            if nargin < 4; cmap = colormap('jet'); end
            
            fig = comparePlot@CBSS(obj, maxErr, nModes, cmap);
            
            % Modify title and X label
            if (obj.ccModel)
                children = get(fig, 'children');
                ax = children(3);
                titlestr = get(ax, 'title'); 
                titlestr = get(titlestr, 'string');
                titlestr = sprintf('%s Reduced to %i CC Modes', titlestr, ...
                    obj.nCharDOF);
                title(ax, titlestr);
                ax = children(1);
                xlabel(ax, '\bfSubstructure Mode Number');
            end
        end
        
        function obj = buildCBSystem(obj, nModes)

            obj.cbModel = true;
            obj.ccModel = false;
            obj = buildCBSystem@CBSS(obj, nModes);
        end
        
        function plotConstraintmode(obj, mode, factor)
            %    plotConstraintmode(obj, mode, factor)
            %
            % Plot the deformation of a given Characteristic Constraint 
            % mode for all parts. Plot may be scaled as desired.
            %
            % INPUT
            %
            % mode:      Mode number to plot
            %
            % factor:    Scale factor for plotting; default 1.
            
            if (nargin < 3); factor = 1; end
            
            cMode = obj.Tcc(:, obj.nModalDOF + mode);
            [~, cc2cb] = sort([obj.modalCB(:); obj.boundaryCB(:)]);
            cMode = cMode(cc2cb);
            cMode = obj.L*cMode;
            currDOF = 1;
            for i = 1:obj.npart
                % Call the CBICE "deform" function with the appropriate set
                % of modal amplitudes
                endDOF = currDOF + obj.parts(i).nBoundaryDOF + obj.parts(i).nModalDOF;
                amps = cMode(currDOF:endDOF - 1);
                obj.parts(i).part.plot(amps*factor);
                currDOF = endDOF;
            end
            title(sprintf('\\bfConstraint Mode %i', mode));
        end
        
        
        function save(obj, filename)
            %    save(obj, filename)
            %
            % Save the object using the filepath property and desired
            % filename. Object is saved with the variable name 'cbss'
            %
            % INPUT
            %
            % filename: Name used to save file.
            
            obj.filepath = 'CCSSs';
            if (nargin == 2)
                obj.filename = filename;
            end
            ccss = obj; %#ok
            save(fullfile(obj.filepath, obj.filename), 'ccss');
        end
        
        function disp(obj)
            %    disp(obj)
            %
            % Display the object in a readable format
            
            fprintf('\nCCSS Object\n');
            if obj.npart == 0;
                fprintf('No included parts\n');
            else
                fprintf('\t%s\n', obj.filename);
                fprintf('\t%i parts\n', obj.npart);
                fprintf('\t%i Modal DOF; %i Boundary DOF\n', ...
                    obj.nModalDOF, obj.nBoundaryDOF);
                fprintf('\t%i Characteristic Constraint Modes\n', ...
                    obj.nCharDOF);
            end
            
            byteSize = whos('obj'); byteSize = byteSize.bytes;
            fprintf('\tSize of %.3f mb\n\n', byteSize/1024^2);
        end
            
    end
end