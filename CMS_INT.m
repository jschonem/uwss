% CMS_INT(ccss)
%
% CMS_INT - CMS Numerical Integration Module
% 
% CMS_INT allows the numerical integration and NNM computation of a CCSS
% object with a nonlinear fit by computing and assembling the nonlinear
% restoring force of each component.
% 
% The force, force Jacobian, and potential energy of the structure are all
% calculated using the same N1 and N2 force Jacobian matrices;
% constructing these takes the bulk of the computation time. As such, new
% N1 and N2 matrices are computed only if the system state is updated (an
% integration routine may need multiple calls to CMS_INT for a single
% state, which would be wasteful if the Jacobian matrices were re-computed 
% every time).
%
% Dependencies: CCSS.m    Characteristic Constraint substructuring module
%
% Use "doc CMS_INT" for detailed description of constructor, properties, and
% methods.

% Joe Schoneman
% UW - Madison, Department of Engineering Physics
% 25 August, 2016

classdef CMS_INT
    
    properties(GetAccess = 'public', SetAccess = 'protected')
        
        m; % Number of parts
        nCC; % Number of CC DOF
        ccBasis; % Reduced basis for the global CC modes
        fiDofs;  % Vector of FI DOF counts for each part
        nFIDof;  % Count of total FI DOF
        NLROMs; % Nonlinear models of each part (structure array)
        Mhat;   % Linear model mass matrix
        Khat;   % Linear model stiffness matrix
        L;      % Substructure assembly matrix
        psiCC;
        modalCB;  % Indices of FI modal DOF
        boundaryCB; % Indices of constraint DOF
        q0;
        N1hat;
        N2hat;
    end
    
    methods
        
        function obj = CMS_INT(ccss)
            %    obj = CMS_INT(ccss)
            %
            % CMS_INT constructor. Accepts a populated CCSS objects and
            % stores the NLROM models for later use. 
            
            obj.m = length(ccss.parts);
            obj.Mhat = ccss.Mcc;
            obj.Khat = ccss.Kcc;
            
            obj.nCC = ccss.nCharDOF;
            obj.nFIDof = size(obj.Mhat, 1) - obj.nCC;
            
            obj.psiCC = ccss.psiCC;
            obj.modalCB = ccss.modalCB;
            obj.boundaryCB = ccss.boundaryCB;
            obj.ccBasis = eye(obj.nCC);
            
            obj.q0 = zeros(obj.nCC + obj.nFIDof, 1);
            
            obj.fiDofs = zeros(obj.m, 1);
            obj.NLROMs = struct('N1', [], 'N2', [], 'Nlist', [], 'theta', [], ...
                                'thetaInv', []);
            
            % Build up the NLROMs and L matrix
            obj.L = zeros(obj.nFIDof + obj.nCC*obj.m, ...
                          obj.nFIDof + obj.nCC);
            nEq = 0; nDof = 0;
            for i = 1:obj.m
                obj.fiDofs(i) = ccss.parts(i).nModalDOF;    
                for j = 1:obj.fiDofs(i)
                    obj.L(nEq + j, nDof + j) = 1;
                end
                nEq = nEq + j; nDof = nDof + j;
                for j = 1:obj.nCC
                    obj.L(nEq + j, obj.nFIDof + j) = 1;
                end
                nEq = nEq + j;
                    
                hold.N1 = ccss.parts(i).part.NLROM.N1;
                hold.N2 = ccss.parts(i).part.NLROM.N2;
                hold.Nlist = ccss.parts(i).part.NLROM.Nlist;
                
                if ~isfield(ccss.parts(i).part.NLROM, 'theta');
                    hold.theta = eye(size(hold.N1(:, :, 1)));
                else
                    hold.theta = ccss.parts(i).part.NLROM.theta;
%                     [U, S, V] = svd(ccss.parts(i).part.NLROM.basis, 'econ');
%                     hold.theta = V';
                end
                hold.thetaInv = inv(hold.theta);
                
                
                obj.NLROMs(i) = hold;
                
            end
            
            [obj.N1hat, obj.N2hat] = update(obj, [obj.q0; obj.q0]);
        end
            
            
        function fnl = fint_nl(obj, z)
            %    fnl = fint_nl(obj, z)
            %
            % Computes the nonlinear restoring force for a modal input.
            % 
            % INPUT
            %
            % z: State space form of the modal amplitudes; z = [q; qd]
            %    where qd are the modal velocities. 
            %
            % OUTPUT
            %
            % fnl: Nonlinear restoring force of the state-space model.
            
            N = size(z, 1)/2;
            if (checkState(obj, z))
                [obj.N1hat, obj.N2hat] = update(obj, z);
                obj.q0 = z(1:N);
            end
            
            fnl = [1/2*obj.N1hat + 1/3*obj.N2hat]*z(1:N);
            
        end
        
        function J = dfint_nl(obj, z)
            %    J = dfint_nl(obj, z)
            %
            % Computes the nonlinear restoring force Jacobian for a modal 
            % input.
            % 
            % INPUT
            %
            % z: State space form of the modal amplitudes; z = [q; qd]
            %    where qd are the modal velocities. 
            %
            % OUTPUT
            %
            % J: Nonlinear restoring force Jacobian of the state-space 
            %    model.
            
            N = size(z, 1)/2;
            if (checkState(obj, z))
                [obj.N1hat, obj.N2hat] = update(obj, z);
                obj.q0 = z(1:N);
            end
            
            J = [obj.N1hat + obj.N2hat];
            J = [J, zeros(size(J))];
            
        end
        
        function E = Energy_nl(obj, z)
            %    E = Energy_nl(obj, z)
            %
            % Computes the nonlinear restoring force energy for a modal 
            % input.
            % 
            % INPUT
            %
            % z: State space form of the modal amplitudes; z = [q; qd]
            %    where qd are the modal velocities. 
            %
            % OUTPUT
            %
            % E: Nonlinear restoring force energy of the state-space 
            %    model.
            
            N = size(z, 1)/2;
            if (checkState(obj, z))
                [obj.N1hat, obj.N2hat] = update(obj, z);
                obj.q0 = z(1:N);
            end
            
            E = z(1:N)'*(1/6*obj.N1hat + 1/12*obj.N2hat)*z(1:N);
        end
        
        function [N1, N2] = update(obj, z)
            %    [N1, N2] = update(obj, z)
            %
            % Update the system by computing the quadratic and cubic
            % restoring force Jacobians based on system state. 
            %
            % INPUT
            %
            % z: State space form of the modal amplitudes; z = [q; qd]
            %    where qd are the modal velocities. 
            %
            % OUTPUT
            %
            % N1: Quadratic restoring force Jacobian
            %
            % N2: Cubic restoring force Jacobian
            
            N = size(z, 1)/2;
            q = z(1:N);
            
            N1 = zeros(obj.nFIDof + obj.nCC);
            N2 = N1;
            indCC = (obj.nFIDof + 1):(obj.nFIDof + obj.nCC);
            % Build up a vector of forces in the fixed interface modes
            indFI = 0;
            
            for j = 1:obj.m
                
                indFI = (indFI(end) + 1):(indFI(end) + obj.fiDofs(j));
                
                indj = [indFI, indCC];
                nDof = size(obj.NLROMs(j).theta, 1);
                N1j = zeros(nDof);
                N2j = zeros(nDof);
                
                Nlistj = obj.NLROMs(j).Nlist;
                
                qj = q(indj); % [FI; CC] coordinates
                
                % Convert to the local basis
                eta = obj.NLROMs(j).theta*qj;
                eta2 = eta(Nlistj(:,1)).*eta(Nlistj(:,2));
                
                % Quadratic terms (if exist)
                if sum(obj.NLROMs(j).N1(:)) 
                    for k = 1:nDof
                        N1j(:, k)= obj.NLROMs(j).N1(:, :, k)*eta;
                    end
                    N1j = obj.NLROMs(j).theta'*N1j*obj.NLROMs(j).theta;
                    N1(indj, indj) = N1j;
                end
                
                % Cubic terms (if exist)
                if sum(obj.NLROMs(j).N2(:))
                    for k = 1:nDof;
                        N2j(:, k)= obj.NLROMs(j).N2(:, :, k)*eta2;
                    end
                    N2j = obj.NLROMs(j).theta'*N2j*obj.NLROMs(j).theta;
                    N2(indj, indj) = N2j;
                end
            end
        end
        
        function new = checkState(obj, z)
            %    new = checkState(obj, z)
            %
            % Determines whether the state of the system is new or not.
            %
            % INPUT
            % 
            % z: State space form of the modal amplitudes; z = [q; qd]
            %    where qd are the modal velocities. 
            %
            % OUTPUT
            %
            % new: Boolean variable describing whether or not the system
            % state is new. 
            
            N = size(z, 1)/2;
            qCheck = z(1:N);
            if (norm(qCheck - obj.q0) > eps)
                new = true;
            else
                new = false;
            end
            
        end
        
    end
end