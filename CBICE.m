% CBICE(abint, thick, workingDir, filepath, options, NNMoptions)
%
% CBICE - Craig Bampton Implicit Condensation/Expansion Module
%
% CBICE serves as the link between ABINT and the full assembly. During the 
% linear substructuring procedure, CBICE calculates interface and 
% constraint modes of its associated component and assembles them into a 
% Craig-Bampton representation for use by the assembly. CBICE also performs 
% the NLROM construction procedure for each component, based on a set of 
% deformations which are supplied in terms of the component's free 
% interface and constraint modes. Basis vector transformation, force scale
% determination, and nonlinear coefficient estimation all occur within this 
% class.
% 
% Based on implicitcondensation_v5 by Rob Kuether
%
% Dependencies: npermutek.m               Generates permutations
%               icloading_v2.m            Generate load cases
%               expansion.m               Expand mebrane deformations
%               noregress_constrained.m   Fit nonlinear coefficients (constrained)
%               noregress.m               Fit nonlinear coefficients (unconstrained)
%               ABINT.m                   Abaqus interface class
%
% Use "doc CBICE" for detailed description of constructor, properties, and
% methods.

% Joe Schoneman
% UW - Madison, Department of Engineering Physics
% 24 August, 2016

classdef CBICE
    properties(GetAccess = 'public', SetAccess = 'public')
        
        abint; % ABINT interface for this CBROM
        NLid; % Unique ID for the nonlinear ROM
        mind; % Modeshape indices for the ROM
        thick; % Geometric thickness of the model
        options; % Option settings for the fit routine
        matDir; % Base directory for file storage
        part; % Part designation (depends on makeup of the FE model)
        linDispLC; 
        workingDir; % FEA working directory
        filepath;  % Filepath used to save the CBICE object
        linModel; % The linear model [structure]
        phiReduced; % Reduced modal basis of the NLROM
        freqsReduced; % The frequencies of the reduced modal basis
        util; % Things we need/want to know but don't want to save. [structure]
        NLROM; % Structure of NLROM properties for later use
        phi_m; % membrane basis vectors (NDof x Nmembrane modes)
        nums_m; % permutation matrix describing q_membrane terms
        NNMoptions; % Structure for the NNM continuation options
        normFactor; % Normalization factor for each flexible mode
        
    end
    
    methods
        function obj = CBICE(abint, thick, workingDir, filepath, options, NNMoptions)
        %        obj = CBICE(abint, thick, workingDir, filepath, options, NNMoptions)
        %
        % Constructor for CBICE. 
        %
        % INPUT
        %
        % abint:  An existing ABINT object.
        %
        % thick:  The aproximate thickness of the component in its bending
        %         direction. Used for nonlinear fit purposes. 
        %
        % workingDir: Working directory. Not actually used for anything
        %             right now, but maintained for backwards compatibility. 
        %
        % filepath: Filepath used to save the CBICE object
        %
        % options:  [optional] NLROM fit options. 
        %
        % NNMoptions: [optional] NNM computation options.
        
            if nargin < 7
                options = struct;
            end
            if nargin < 8
                NNMoptions = struct;
            end
            obj.NNMoptions = CBICE.parseNNMOptions(NNMoptions);
            obj.abint = abint;
            obj.filepath = filepath;
            obj.thick = thick;
            obj.options = CBICE.parseOptions(options);
            obj.workingDir = workingDir;
            
        end
        
        
        function [obj, omegaN, MkbHat, MbbHat, KbbHat] = makeCB(obj, nModes, bcs, boundaries, method)
            %    [obj, omegaN, MkbHat, MbbHat, KbbHat] = makeCB(obj, nModes, bcs, boundaries, method)
            %
            % Make a Craig-Bampton substructure of a model. 
            %
            % INPUT
            % 
            % nModes: Number of fixed interface (FI) modes to retain in the component model.
            % 
            % bcs:    [nBC x 7] Natural boundary conditions of the
            %         component, given in the format 
            %         [node, U1, U2, U3, UR1, UR2, UR3; ...] where "0" is not
            %         constrained and "1" is constrained.
            %
            % boundaries: [nConn x 7] Boundary connections of the object,
            %             given in the same format as "bcs".
            %
            % method: [string] Options are "abaqus" [default] or "matlab". 
            %         "matlab" uses MATLAB's built-in routines to perform 
            %         modal analyses of the parts; "abaqus" calls out to 
            %         Abaqus FEA software.
            
            if ~(strcmpi(method, 'matlab') || strcmpi(method, 'abaqus'))
                error('Invalid method selected for CBICE.makeCB');
            end
            % If matrices don't exist yet, get 'em
            if isempty(obj.linModel)
                [obj.linModel.M, obj.linModel.K] = obj.abint.matrix(bcs); 
            end
            
            % Sort out which DOF are which (store in ABSOLUTE DOF
            % numbering)
            % Base boundary conditions
            bcDof = [];
            for i = 1:size(bcs, 1)
                node = bcs(i, 1);
                constraints = find(bcs(i, 2:7));
                bcDof = [bcDof; node + constraints(:)/10];
            end
            % Relate to absolute DOF numbers
            bcDofAbs = obj.abint.getAbsDofNumber(bcDof);
            
            % Connector boundary DOF
            connDof = [];
            for i = 1:size(boundaries, 1)
                node = boundaries(i, 1);
                constraints = find(boundaries(i, 2:7));
                connDof = [connDof; node + constraints(:)/10];
            end
            % Relate to absolute DOF numbers
            connDofAbs = obj.abint.getAbsDofNumber(connDof);
            
            % Get internal DOF numbering
                % MSA: These are all of the other DOF in the model.
            internalDofAbs = (1:length(obj.abint.dof))';
            remove = [bcDofAbs; connDofAbs];
            for i = 1:length(remove)
                internalDofAbs(internalDofAbs == remove(i)) = [];
            end
            
            % Store these in "linModel"
            obj.linModel.bcDofAbs = bcDofAbs;
            obj.linModel.connDofAbs = connDofAbs;
            obj.linModel.internalDofAbs = internalDofAbs;
            dofOrder = [obj.linModel.internalDofAbs; obj.linModel.connDofAbs; obj.linModel.bcDofAbs];
                % MSA: This appears to be the order in which we want the
                % DOF in the CB Model.  The BC DOF are at the end, so zeros
                % will simply be appended for their displacements.
                obj.linModel.cbDofOrder=dofOrder; % MSA added
            [~, obj.linModel.dofOrder] = sort(dofOrder);
                % MSA: This appears to create an index that can un-sort the
                % DOF.  Apparently we'll put them in the order above in the CB Model. 
                        
            % Get fixed or free-interface modes
            fixedSet = [bcs; boundaries]; 
            
            if strcmpi(method, 'matlab') % If MATLAB is selected
                for i = 1
                    % Partition system matrices to internal nodes
                    M = obj.linModel.M(internalDofAbs, internalDofAbs);
                    K = obj.linModel.K(internalDofAbs, internalDofAbs);
                    if (length(nModes) == 2) % Frequency range option
                        if ~exist('obj.linModel.omegaN')
                            warning('CBICE.makeCB does not support "MATLAB" option with frequency inputs for first call.');
                            warning('Calling Abaqus. "MATLAB" may be used for subsequent calls to CBICE.makeCB');
                            method = 'abaqus';
                            break;
                        else
                            nModes = length(obj.linModel.omegaN);
                        end
                    end
                    fprintf('Finding %i modes for %s\n', nModes, obj.abint.modelname);
                    [phiFI, fnFI] = eigs(K, M, nModes, 'SM');
                    fnFI = sqrt(diag(fnFI))/2/pi;
                    [fnFI, sortInd] = sort(fnFI);
                    phiFI = phiFI(:, sortInd);
                end
            end
       
            
            if strcmpi(method, 'abaqus') % If Abaqus is selected
                [phiFI, fnFI] = obj.abint.modal(fixedSet, nModes);
                if isempty(fnFI)
                    warning('No modes detected in range [%.0f, %.0f]; re-running with 5 FI modes', ...
                        nModes(1), nModes(2));
                    [phiFI, fnFI] = obj.abint.modal(fixedSet, 5);
                end
                phiFI = phiFI(internalDofAbs, :);
            end
            
            omegaN = fnFI*2*pi;
            
            % Get characteristic constraint modes [Use stiffness matrices]
            Kii = obj.linModel.K(internalDofAbs, internalDofAbs);
            Kib = obj.linModel.K(internalDofAbs, connDofAbs);
            psiIB = -Kii\Kib; % Very badly scaled!
            
            % Save modal matrices to model
            obj.normFactor = max(abs(phiFI), [], 1); % Normalization factor for each mode
            obj.linModel.phiFI = phiFI;
            obj.linModel.omegaN = omegaN;
            obj.linModel.psiIB = psiIB;
            
            % Build mass "hat" matrices
            Mii = obj.linModel.M(internalDofAbs, internalDofAbs);
            Mib = obj.linModel.M(internalDofAbs, connDofAbs);
            MkbHat = phiFI.'*(Mii*psiIB + Mib);
            
            Mbb = obj.linModel.M(connDofAbs, connDofAbs);
            MbbHat = psiIB.'*Mii*psiIB + Mib.'*psiIB + psiIB.'*Mib + Mbb;
            
            % Build stiffness "hat" matrix
            Kbb = obj.linModel.K(connDofAbs, connDofAbs);
            KbbHat = psiIB.'*Kii*psiIB + Kib.'*psiIB + psiIB.'*Kib + Kbb;

            % Save mass and stiffness matrices
            obj.linModel.MkbHat = MkbHat;
            obj.linModel.MbbHat = MbbHat;
            obj.linModel.KbbHat = KbbHat;
            obj.linModel.isFI = [true(length(omegaN),1); false(size(psiIB,1),1)];
            
        end
        
        
        function [obj, data] = qrFit(obj, qrInds, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            %    [obj, data] = qrFit(obj, qrInds, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            % 
            % Use a QR decomposition of the FI/CC modeset to obtain a
            % nonlinear model of the structure's restoring force. In the
            % descriptions below, mFI is the number of FI modes in the
            % structure, mCC is the number of CC modes in the component,
            % and m = mFI + mCC is the total number of modes in the
            % component model. mQR is the number of modes in the requested
            % QR basis; must have mQR < m.
            %
            % INPUT
            %
            % qrInds: [nQR x 1] Absolute indices of QR vetors to retain.
            %
            % ccBasisFull: [mCC x nB] Localized CC basis in the constraint
            %              modes of the component. 
            %
            % springs:  Set of grounding springs used for the component. 
            %           Structure with fields of size [nS x 1] where nS is
            %           the number of springs. Fieldnames are "springs.k"
            %           which contains the stiffness of each spring and
            %           "springs.dof" which contains the ABSOLUTE DOF
            %           numbers of each spring to be attached. These are
            %           grounding springs only; they do not attach between
            %           nodes.
            %
            % thick:    [1 x 1] Deflection level to push each QR mode to. 
            %
            % quads:    [boolean] Include quadratic fit terms?
            %
            % triples:  [boolean] Include cubic fit terms?
            %
            % flist:    [3 x nFit] Vector of polynomial terms to create fit
            %           coefficients for.
            %
            % fitData:  Structure of the same form as output "data" which
            %           contains FEA load case results that can be used to
            %           perform a fit using different parameters without
            %           running another set of FEA load cases.
            %
            % OUTPUT
            %
            % data:   Structure containing load case force and response
            %         data that can be saved for postprocessing or reuse.
            
            % (1) Form variables of convenience
            nFI = length(obj.linModel.omegaN); nCC = size(ccBasisFull, 2);
            nB = length(obj.linModel.connDofAbs);
            
            
            % (2) Obtain physical basis for each set of modes
            % First obtain the FI/Constraint basis
            fiBasis = [obj.linModel.phiFI; zeros(nB, nFI)];
            ccBasis = [obj.linModel.psiIB; 
                          eye(nB, nB)]*ccBasisFull((nFI + 1):end, :);
            basis = [fiBasis, ccBasis];
            
            % Get loaded DOF partition and associated system matrices
            loadDof = [obj.linModel.internalDofAbs; obj.linModel.connDofAbs];
            Kl = obj.linModel.K(loadDof, loadDof);
            Ks = obj.linModel.K;
            for i = 1:length(springs.dof)
                dof = springs.dof(i); k = springs.k(i);
                Ks(dof, dof) = Ks(dof, dof) + k;
            end
            Ks = Ks(loadDof, loadDof);
            

            % (2) Perform a QR decomposition of the FI/CC basis to get a reduced set of
            % NL modes
            [Q, R] = qr(basis, 0);
            loadBasis = Q(:, qrInds);
            theta = R;
            
            tic;
            if isempty(fitData)
            
            % (4) Use icloading to generate loadings for the basis vectors
%             [P, F] = icloading_v2(loadBasis, thick*obj.thick, eye(length(loadBasis)), Kl, 1, quads, triples, 1);
            [P, F] = icloading_v2(loadBasis, thick*obj.thick, Ks, Ks, 1, quads, triples, 1);
            % Zero out forces on boundary nodes
            boundaryNodes = (length(obj.linModel.internalDofAbs) + ...
                 1):(length(obj.linModel.internalDofAbs) + length(obj.linModel.connDofAbs));
%             F(boundaryNodes, :) = 0; warning('Boundary forces zeroed in SVD fit');
            
            % (5) Send to Abaqus
            [displacement, cf] = obj.abint.static(obj.linModel.bcDofAbs, F, loadDof, 'nonlinear', springs);
            
            else
                F = fitData.F;
                P = fitData.P;
                displacement = fitData.displacement;
            end
            
            
            maxDisp = max(abs(displacement));
            if (length(thick) == 1)
                thick = thick*ones(length(qrInds), 1);
            end
            for i = 1:length(qrInds)
                fprintf('Displacement ratio QR %i = %.3f%%\n', i, maxDisp(i)/(thick(i)*obj.thick)*100);
            end
            
                
            % (6) Perform fit
            [Knl, tlist, dispErr, forceErr] = noregress(loadBasis'*Ks*loadBasis, loadBasis, F, ... % Can we fit with a reduced set of U vectors?
                    displacement, quads, triples, flist);
            fitTime = toc;
%             keyboard

           % (6) Add all non-included modes
            t = tlist(:); 
            discardModes = 1:(nFI + nCC);
            for i = 1:length(qrInds)
                ti = (t == i);
                t(ti) = qrInds(i);
                discardModes(i) = 0;
            end
            tlistActual = reshape(t, size(tlist));
            discardModes(discardModes == 0) = [];
            tlistEmpty = repmat(discardModes(:), 1, 3);

            
            KnlActual = zeros(nFI + nCC, size(Knl, 2));
            KnlActual(qrInds, :) = Knl;
            
            nEmpty = size(tlistEmpty, 1);
            obj.NLROM.tlist = [tlistActual; tlistEmpty];
            
            obj.NLROM.Knl = [KnlActual, zeros(nFI + nCC, nEmpty)];
            [obj.NLROM.N1, obj.NLROM.N2, obj.NLROM.Nlist] = Knl2Nash(obj.NLROM.Knl, obj.NLROM.tlist);
            obj.NLROM.dispErr = dispErr; obj.NLROM.forceErr = forceErr;
            obj.NLROM.fitTime = fitTime;
            obj.NLROM.theta = theta;
            obj.NLROM.basis = basis;
            
            % Optional outputs
            data.basis = basis; data.F = F; data.P = P; data.displacement = displacement;
            data.fitTime = fitTime; data.Kl = Kl;
            data.dispErr = dispErr; data.forceErr = forceErr;
            
        end
        
        function [obj, data] = qrFitIS(obj, refLoc, ab, nodeset, bcset, qrInds, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            %    [obj, data] = qrFitIS(obj, refLoc, ab, nodeset, bcset, qrInds, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            % 
            % Use a QR decomposition of the FI/CC modeset to obtain a
            % nonlinear model of the structure's restoring force. In the
            % descriptions below, mFI is the number of FI modes in the
            % structure, mCC is the number of CC modes in the component,
            % and m = mFI + mCC is the total number of modes in the
            % component model. mQR is the number of modes in the requested
            % QR basis; must have mQR < m.
            %
            % This version uses the finite element "in situ" process in
            % order to perform the NLROM fit.
            %
            % INPUT
            %
            % refLoc:  [1 x 4] A reference node (first entry) and location
            %          vector (final three entries) used to determine the
            %          placement of the component within the FEA assembly 
            %          in use. The ref node is given as an ABSOLUTE node
            %          number. 
            %
            % ab:      An ABINT object corresponding to the full assembly
            %          which is used for "in situ" simulation.
            %
            % nodeset: [nNodes x 1] The nodeset within "ab" which
            %          corresponds to the component of interest. 
            %
            % bcset:   [nBC x 7] Boundary condition set for the full
            %          assembly. First column is BC node (actual), columns 
            %          2-6 are the associated DOF. "1" is constrained; "0" 
            %          unconstrained.
            %
            % qrInds: [nQR x 1] Absolute indices of QR vetors to retain.
            %
            % ccBasisFull: [mCC x nB] Localized CC basis in the constraint
            %              modes of the component. 
            %
            % thick:    [1 x 1] Deflection level to push each QR mode to. 
            %
            % quads:    [boolean] Include quadratic fit terms?
            %
            % triples:  [boolean] Include cubic fit terms?
            %
            % flist:    [3 x nFit] Vector of polynomial terms to create fit
            %           coefficients for.
            %
            % fitData:  Structure of the same form as output "data" which
            %           contains FEA load case results that can be used to
            %           perform a fit using different parameters without
            %           running another set of FEA load cases.
            %
            % OUTPUT
            %
            % data:   Structure containing load case force and response
            %         data that can be saved for postprocessing or reuse.
            
            % (1) Form variables of convenience
            nFI = length(obj.linModel.omegaN); nCC = size(ccBasisFull, 2);
            nB = length(obj.linModel.connDofAbs);
            
            
            % (2a) Obtain physical basis for each set of modes
            % First obtain the FI/Constraint basis
            fiBasis = [obj.linModel.phiFI; zeros(nB, nFI)];
            ccBasis = [obj.linModel.psiIB; 
                          eye(nB, nB)]*ccBasisFull((nFI + 1):end, :);
            basis = [fiBasis, ccBasis];
            
            % Get loaded DOF partition and associated system matrices
            loadDof = [obj.linModel.internalDofAbs; obj.linModel.connDofAbs];
            
            Kl = obj.linModel.K(loadDof, loadDof);
            
            % Alternate stiffness matrix with springs included
            Ks = obj.linModel.K;
            for i = 1:length(springs.dof)
                dof = springs.dof(i); k = springs.k(i);
                Ks(dof, dof) = Ks(dof, dof) + k;
            end
            Ks = Ks(loadDof, loadDof);
            Ksprings = blkdiag(zeros(length(obj.linModel.internalDofAbs)), ...
                               diag(springs.k));
            
            % (2b) Perform a QR decomposition of the FI/CC basis to get a reduced set of
            % NL modes
            [Q, R] = qr(basis, 0);
            loadBasis = Q(:, qrInds);
            theta = R;

            tic
            if isempty(fitData) % No fitData present
                % (3) Use icloading to generate loadings for the basis vectors
                [P, F] = icloading_v2(loadBasis, thick*obj.thick, eye(length(loadBasis)), Kl + Ksprings, 1, quads, triples, 1);
%                 [P, F] = icloading_v2(loadBasis, thick*obj.thick, eye(length(loadBasis)), Ks, 1, quads, triples, 1);
%                 [P, F] = icloading_v2(loadBasis, thick*obj.thick, Ks, Ks, 1, quads, triples, 1);
                % Zero out forces on boundary nodes
                boundaryNodes = (length(obj.linModel.internalDofAbs) + ...
                     1):(length(obj.linModel.internalDofAbs) + length(obj.linModel.connDofAbs));
                F(boundaryNodes, :) = 0;

                % (4a) Line up the nodeset and ABINT object supplied with
                % the desired load DOF vector using node locations.
                % - Get the node locations of the component load DOF
                compDof = obj.abint.getDofNumber(loadDof);
                compNodes = unique(floor(compDof), 'stable');
                compNodesAbs = obj.abint.getAbsNodeNumber(compNodes);
                compXYZ = obj.abint.nodes(compNodesAbs, 2:4);
                
                % - Get the node locations of the ABINT nodeset DOF
                abNodesAbs = ab.getAbsNodeNumber(nodeset);
                abXYZ = ab.nodes(abNodesAbs, 2:4);
                
                % Get the offset between coordinate systems
                offset = obj.abint.nodes(refLoc(1), 2:4) - ...
                         ab.nodes(refLoc(2), 2:4);
                abXYZOffset = bsxfun(@plus, abXYZ, offset(:)');
                
                % Now run through and match the DOF at each node
                loadDofAb = zeros(size(loadDof));
                k = 1;
                for i = 1:length(compNodes)
                    compNode = compNodes(i);
                    compLoc = compXYZ(i, :);
                    d = bsxfun(@minus, abXYZOffset, compLoc);
                    dnorm = dot(d, d, 2);
                    [distance, abNodeAbs] = min(dnorm);
                    
                    if (distance > 1E-6)
                        error('Comp Node %i out of tolerance; |d| = %.3E\n', ...
                            compNode, distance);
                    end
                    abNode = nodeset(abNodeAbs);
                    
                    for j = 1:6 % This is written only for beams and shells
                        dofNum = (compNodesAbs(i) - 1)*6 + j;
                        if sum(obj.linModel.bcDofAbs == dofNum); continue; end
                        loadDofAb(k) = abNode + j/10;
                        k = k + 1;
                    end
                end
%                 loadDofAb(loadDofAb == 0) = [];
                loadDofAbs = ab.getAbsDofNumber(loadDofAb); 
                
                % Figure out the BC set using 7-column convention
                
                bcDOF = [];
                for j = 1:size(bcset, 1)
                    newDOF = bcset(j, 1) + find(bcset(j, 2:7))/10;
                    bcDOF = [bcDOF; newDOF(:)];
                end

                bcDOFAbs = ab.getAbsDofNumber(bcDOF);
                
                % (4b) Run Abaqus job on the assembly nodeset of interest
                [displacement, cf] = ab.static(bcDOFAbs, F, loadDofAbs, 'nonlinear');
%                 [displacement, cf] = obj.abint.static(obj.linModel.connDofAbs, F, loadDof, 'nonlinear', springs);
                
            else
                F = fitData.F;
                P = fitData.P;
                displacement = fitData.displacement;
            end
            
            
            maxDisp = max(abs(displacement));
            if (length(thick) == 1)
                thick = thick*ones(length(qrInds), 1);
            end
            for i = 1:length(qrInds)
                fprintf('Displacement ratio QR %i = %.3f%%\n', i, maxDisp(i)/(thick(i)*obj.thick)*100);
            end
            
            % (5) Perform fit
%             lambda = loadBasis'*Kl*loadBasis;
            lambda = loadBasis'*Ks*loadBasis;
%             [Knl, tlist, dispErr, forceErr] = noregress(lambda, basis, F, ...
%                     displacement, quads, triples, flist);
                
            if isequal(size(lambda), [1, 1])
                lambda = sqrt(lambda)/2/pi;
            end
            [Knl, tlist, dispErr, forceErr] = noregress(lambda, loadBasis, F, ...
                    displacement, quads, triples, flist);
            fitTime = toc;
            
            % (6) Add all non-included modes
            t = tlist(:); 
            discardModes = 1:(nFI + nCC);
            for i = 1:length(qrInds)
                ti = (t == i);
                t(ti) = qrInds(i);
                discardModes(i) = 0;
            end
            tlistActual = reshape(t, size(tlist));
            discardModes(discardModes == 0) = [];
            tlistEmpty = repmat(discardModes(:), 1, 3);

            
            KnlActual = zeros(nFI + nCC, size(Knl, 2));
            KnlActual(qrInds, :) = Knl;
            
            nEmpty = size(tlistEmpty, 1);
            obj.NLROM.tlist = [tlistActual; tlistEmpty];
            
            obj.NLROM.Knl = [KnlActual, zeros(nFI + nCC, nEmpty)];
            [obj.NLROM.N1, obj.NLROM.N2, obj.NLROM.Nlist] = Knl2Nash(obj.NLROM.Knl, obj.NLROM.tlist);
            obj.NLROM.dispErr = dispErr; obj.NLROM.forceErr = forceErr;
            obj.NLROM.fitTime = fitTime;
            obj.NLROM.theta = theta;
            obj.NLROM.basis = basis;
            
            % Optional outputs
            data.basis = basis; data.F = F; data.P = P; data.displacement = displacement;
            data.fitTime = fitTime; data.Kl = Kl;
            data.dispErr = dispErr; data.forceErr = forceErr;
            
         end
        
        function [obj, data] = svdFit(obj, svdInd, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            %    [obj, data] = svdFit(obj, svdInd, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            % 
            % Old, not supported any longer. Remove when possible. Uses SVD
            % instead of QR, which we were not as happy with. 
            
            % (1) Form variables of convenience
            nFI = length(obj.linModel.omegaN); nCC = size(ccBasisFull, 2);
            nB = length(obj.linModel.connDofAbs);
            nSvd = nFI + nCC;
            
            
            % (2) Obtain physical basis for each set of modes
            % First obtain the FI/Constraint basis
            fiBasis = [obj.linModel.phiFI; zeros(nB, nFI)];
            ccBasis = [obj.linModel.psiIB; 
                          eye(nB, nB)]*ccBasisFull((nFI + 1):end, :);
            basis = [fiBasis, ccBasis];
            
            % Get loaded DOF partition and associated system matrices
            loadDof = [obj.linModel.internalDofAbs; obj.linModel.connDofAbs];
            Kl = obj.linModel.K;
            for i = 1:length(springs.dof)
                dof = springs.dof(i); k = springs.k(i);
                Kl(dof, dof) = Kl(dof, dof) + k;
            end
            Kl = Kl(loadDof, loadDof);
            

            % (2) Perform an SVD of the FI/CC basis to get a reduced set of
            % NL modes
            [U, S, V] = svd(basis, 'econ');
            if isempty(svdInd) % This is the input option to examine the SVD results
                plot(diag(S));
                keyboard
            end
            loadBasis = U(:, svdInd);
            theta = S*V';
            
            
            tic
            if isempty(fitData) % No fitData present
                % (3) Use icloading to generate loadings for the basis vectors
                [P, F] = icloading_v2(loadBasis, thick*obj.thick, eye(length(loadBasis)), Kl, 1, quads, triples, 1);
                % Zero out forces on boundary nodes
%                 boundaryNodes = (length(obj.linModel.internalDofAbs) + ...
%                      1):(length(obj.linModel.internalDofAbs) + length(obj.linModel.connDofAbs));
    %             F(boundaryNodes, :) = 0;

                % (4) Send to Abaqus
                [displacement, cf] = obj.abint.static(obj.linModel.bcDofAbs, F, loadDof, 'nonlinear', springs);
                
            else
                F = fitData.F;
                P = fitData.P;
                displacement = fitData.displacement;
            end
            
            maxDisp = max(abs(displacement));
            for i = 1:length(svdInd)
                if length(thick) > 1; t = thick(i); 
                else t = thick;
                end
                fprintf('Displacement ratio SVD %i = %.3f%%\n', svdInd(i), maxDisp(i)/(t*obj.thick)*100);
            end
            
            % (6) Perform fit
            keyboard
            [Knl, tlist, dispErr, forceErr] = noregress(loadBasis'*Kl*loadBasis, loadBasis, F, ... % Can we fit with a reduced set of U vectors?
                    displacement, quads, triples, flist);
            fitTime = toc;
%             keyboard
            % (7) Zero out all other NL terms
            
            emptyModes = 1:nSvd;
            for i = emptyModes
                if sum(svdInd == i)
                    emptyModes(i) = nan;
                end
            end
            emptyModes(isnan(emptyModes)) = [];
            
            tlistEmpty = repmat(emptyModes', 1, 3);
            nEmpty = size(tlistEmpty, 1);
            obj.NLROM.tlist = [tlist; tlistEmpty];
            obj.NLROM.Knl = blkdiag(Knl, zeros(nEmpty));
            [obj.NLROM.N1, obj.NLROM.N2, obj.NLROM.Nlist] = Knl2Nash(obj.NLROM.Knl, obj.NLROM.tlist);
            obj.NLROM.theta = theta;
            obj.NLROM.basis = basis;
            
            % Optional outputs
            data.basis = basis; data.F = F; data.P = P; data.displacement = displacement;
            data.loadBasis = loadBasis; data.theta = theta; data.fitTime = fitTime;
            data.dispErr = dispErr; data.forceErr = forceErr; data.Kl = Kl;
            
        end
        
        function [obj, data] = ficcFitIS(obj, refLoc, ab, nodeset, bcset, fiMind, ccMind, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            %    [obj, data] = ficcFitIS(obj, refLoc, ab, nodeset, bcset, fiMind, ccMind, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            %
            % Use the FI/CC modeset to obtain a
            % nonlinear model of the structure's restoring force. In the
            % descriptions below, mFI is the number of FI modes in the
            % structure, mCC is the number of CC modes in the component,
            % and m = mFI + mCC is the total number of modes in the
            % component model. nFI and nCC are the number of modes selected
            % to retain in the NLROM; must have nFI < mFI and nCC < nCC. 
            %
            % This version uses the finite element "in situ" process in
            % order to perform the NLROM fit.
            %
            % INPUT
            %
            % refLoc:  [1 x 4] A reference node (first entry) and location
            %          vector (final three entries) used to determine the
            %          placement of the component within the FEA assembly 
            %          in use. The ref node is given as an ABSOLUTE node
            %          number. 
            %
            % ab:      An ABINT object corresponding to the full assembly
            %          which is used for "in situ" simulation.
            %
            % nodeset: [nNodes x 1] The nodeset within "ab" which
            %          corresponds to the component of interest. 
            %
            % bcset:   [nBC x 7] Boundary condition set for the full
            %          assembly. First column is BC node (actual), columns 
            %          2-6 are the associated DOF. "1" is constrained; "0" 
            %          unconstrained.
            %
            % fiMind: [nFI x 1] Indices of the FI modes to retain.
            %
            % ccMind: [nCC x 1] Indices of the CC modes to retain.
            %
            % ccBasisFull: [mCC x nB] Localized CC basis in the constraint
            %              modes of the component. 
            %
            %
            % thick:    [1 x 1] Deflection level to push each QR mode to. 
            %
            % quads:    [boolean] Include quadratic fit terms?
            %
            % triples:  [boolean] Include cubic fit terms?
            %
            % flist:    [3 x nFit] Vector of polynomial terms to create fit
            %           coefficients for.
            %
            % fitData:  Structure of the same form as output "data" which
            %           contains FEA load case results that can be used to
            %           perform a fit using different parameters without
            %           running another set of FEA load cases.
            %
            % OUTPUT
            %
            % data:   Structure containing load case force and response
            %         data that can be saved for postprocessing or reuse.
            
            % (1) Form variables of convenience
            nFI = length(obj.linModel.omegaN); nCC = size(ccBasisFull, 2);
            nM = length(fiMind); nB = length(obj.linModel.connDofAbs);
            
            
            % (2) Obtain physical basis for each set of modes
            % First obtain the FI/Constraint basis
            fiBasis = [obj.linModel.phiFI(:, fiMind); zeros(nB, nM)];
            ccBasis = [obj.linModel.psiIB; 
                          eye(nB, nB)]*ccBasisFull((nFI + 1):end, ccMind);
            basis = [fiBasis, ccBasis];
            
            % Get loaded DOF partition and associated system matrices
            loadDof = [obj.linModel.internalDofAbs; obj.linModel.connDofAbs];
            
            Kl = obj.linModel.K(loadDof, loadDof);
            Ksprings = blkdiag(zeros(length(obj.linModel.internalDofAbs)), ...
                               diag(springs.k));
%             keyboard
            tic
            if isempty(fitData) % No fitData present
                % (3) Use icloading to generate loadings for the basis vectors
                [P, F] = icloading_v2(basis, thick*obj.thick, Kl, Kl + Ksprings, 1, quads, triples, 1);
                % Zero out forces on boundary nodes
                if (isempty(obj.linModel.bcDofAbs))
                    boundaryNodes = (length(obj.linModel.internalDofAbs) + ...
                         1):(length(obj.linModel.internalDofAbs) + length(obj.linModel.connDofAbs));
%                     F(boundaryNodes, :) = 0;
%                     warning('Natural BCs not present; boundary nodes zeroed');
                else
                    warning('Spring stiffness inactive, boundary Nodes NOT zeroed');
                end

                % (4a) Line up the nodeset and ABINT object supplied with
                % the desired load DOF vector using node locations.
                % - Get the node locations of the component load DOF
                compDof = obj.abint.getDofNumber(loadDof);
                compNodes = unique(floor(compDof), 'stable');
                compNodesAbs = obj.abint.getAbsNodeNumber(compNodes);
                compXYZ = obj.abint.nodes(compNodesAbs, 2:4);
                
                % - Get the node locations of the ABINT nodeset DOF
                abNodesAbs = ab.getAbsNodeNumber(nodeset);
                abXYZ = ab.nodes(abNodesAbs, 2:4);
                
                % Get the offset between coordinate systems
                offset = obj.abint.nodes(refLoc(1), 2:4) - ...
                         ab.nodes(refLoc(2), 2:4);
                abXYZOffset = bsxfun(@plus, abXYZ, offset(:)');
                
                % Now run through and match the DOF at each node
                loadDofAb = zeros(size(loadDof));
                for i = 1:length(compNodes)
                    compNode = compNodes(i);
                    compLoc = compXYZ(i, :);
                    d = bsxfun(@minus, abXYZOffset, compLoc);
                    dnorm = dot(d, d, 2);
                    [distance, abNodeAbs] = min(dnorm);
                    if (distance > 1E-6)
                        error('Comp Node %i out of tolerance; |d| = %.3E\n', ...
                            compNode, distance);
                    end
                    abNode = nodeset(abNodeAbs);
                    for j = 1:6 % This is written only for beams and shells
                        dofNum = (i - 1)*6 + j;
                        loadDofAb(dofNum) = abNode + j/10;
                    end
                end
                loadDofAbs = ab.getAbsDofNumber(loadDofAb); 
                
                % Figure out the BC set - not written generally at all
                % right now, just assumes each node is fully fixed.
                bcDOF = [];
                for j = 1:size(bcset, 1)
                    newDOF = bcset(j, 1) + find(bcset(j, 2:7))/10;
                    bcDOF = [bcDOF; newDOF(:)];
                end

                bcDOFAbs = ab.getAbsDofNumber(bcDOF);
                
                % (4b) Run Abaqus job on the assembly nodeset of interest
                [displacement, cf] = ab.static(bcDOFAbs, F, loadDofAbs, 'nonlinear');
%                 [displacement, cf] = obj.abint.static(obj.linModel.connDofAbs, F, loadDof, 'nonlinear', springs);
                
            else
                F = fitData.F;
                P = fitData.P;
                displacement = fitData.displacement;
            end
            
            
            % (5) Perform fit
            lambda = basis'*Kl*basis;
%             [Knl, tlist, dispErr, forceErr] = noregress(lambda, basis, F, ...
%                     displacement, quads, triples, flist);
                
            phiInv = lambda\basis'*Kl;
            if isequal(size(lambda), [1, 1])
                lambda = sqrt(lambda)/2/pi;
            end
            [Knl, tlist, dispErr, forceErr] = noregress(lambda, basis, F, ...
                    displacement, quads, triples, flist, phiInv);
            fitTime = toc;
            
            % (6) Add all non-included modes
            t = tlist(:); 
            for i = ccMind
                tCC = (t == (nM + i));
                t(tCC) = nFI + i;
            end
            tlistActual = reshape(t, size(tlist));
            allFIModes = 1:nFI;
            allCCModes = (nFI + 1):(nFI + nCC);
            allFIModes(fiMind) = []; allCCModes(ccMind) = [];
            
            KnlActual = zeros(nFI + nCC, size(Knl, 2));
            KnlActual([fiMind(:); nFI + ccMind(:)], :) = Knl;

            tlistEmpty = repmat([allFIModes, allCCModes]', 1, 3);
            nEmpty = size(tlistEmpty, 1);
            obj.NLROM.tlist = [tlistActual; tlistEmpty];
            
            obj.NLROM.Knl = [KnlActual, zeros(nFI + nCC, nEmpty)];
            [obj.NLROM.N1, obj.NLROM.N2, obj.NLROM.Nlist] = Knl2Nash(obj.NLROM.Knl, obj.NLROM.tlist);
            obj.NLROM.dispErr = dispErr; obj.NLROM.forceErr = forceErr;
            obj.NLROM.fitTime = fitTime;
            
            % Optional outputs
            data.basis = basis; data.F = F; data.P = P; data.displacement = displacement;
            data.fitTime = fitTime; data.Kl = Kl;
            data.dispErr = dispErr; data.forceErr = forceErr;
            
         end
        
        function [obj, data] = ficcFit(obj, fiMind, ccMind, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            %    [obj, data] = ficcFit(obj, fiMind, ccMind, ccBasisFull, springs, thick, quads, triples, flist, fitData)
            %
            % Use the FI/CC modeset to obtain a
            % nonlinear model of the structure's restoring force. In the
            % descriptions below, mFI is the number of FI modes in the
            % structure, mCC is the number of CC modes in the component,
            % and m = mFI + mCC is the total number of modes in the
            % component model. nFI and nCC are the number of modes selected
            % to retain in the NLROM; must have nFI < mFI and nCC < nCC. 
            %
            % INPUT
            %
            % fiMind: [nFI x 1] Indices of the FI modes to retain.
            %
            % ccMind: [nCC x 1] Indices of the CC modes to retain.
            %
            % ccBasisFull: [mCC x nB] Localized CC basis in the constraint
            %              modes of the component. 
            %
            % springs:  Set of grounding springs used for the component. 
            %           Structure with fields of size [nS x 1] where nS is
            %           the number of springs. Fieldnames are "springs.k"
            %           which contains the stiffness of each spring and
            %           "springs.dof" which contains the ABSOLUTE DOF
            %           numbers of each spring to be attached. These are
            %           grounding springs only; they do not attach between
            %           nodes.
            %
            % thick:    [1 x 1] Deflection level to push each QR mode to. 
            %
            % quads:    [boolean] Include quadratic fit terms?
            %
            % triples:  [boolean] Include cubic fit terms?
            %
            % flist:    [3 x nFit] Vector of polynomial terms to create fit
            %           coefficients for.
            %
            % fitData:  Structure of the same form as output "data" which
            %           contains FEA load case results that can be used to
            %           perform a fit using different parameters without
            %           running another set of FEA load cases.
            %
            % OUTPUT
            %
            % data:   Structure containing load case force and response
            %         data that can be saved for postprocessing or reuse.
            
            % (1) Form variables of convenience
            nFI = length(obj.linModel.omegaN); nCC = size(ccBasisFull, 2);
            nM = length(fiMind); nB = length(obj.linModel.connDofAbs);
            
            
            % (2) Obtain physical basis for each set of modes
            % First obtain the FI/Constraint basis
%             fiBasisFull = [obj.linModel.phiFI; zeros(nB, nFI)];
%             ccBasisFull = [obj.linModel.psiIB; 
%                           eye(nB, nB)]*ccBasisFull((nFI + 1):end, :);
            fiBasis = [obj.linModel.phiFI(:, fiMind); zeros(nB, nM)];
            ccBasis = [obj.linModel.psiIB; 
                          eye(nB, nB)]*ccBasisFull((nFI + 1):end, ccMind);
            basis = [fiBasis, ccBasis];
%             basisFull = [fiBasisFull, ccBasisFull];
            % Get loaded DOF partition and associated system matrices
            loadDof = [obj.linModel.internalDofAbs; obj.linModel.connDofAbs];
            
            Ksprings = blkdiag(zeros(length(obj.linModel.internalDofAbs)), ...
                               diag(springs.k));
            Kl = obj.linModel.K(loadDof, loadDof);
            
%             keyboard
            tic
            if isempty(fitData) % No fitData present
                % (3) Use icloading to generate loadings for the basis vectors
                [P, F] = icloading_v2(basis, thick*obj.thick, Kl, Kl + Ksprings, 1, quads, triples, 1);
                % Zero out forces on boundary nodes
                if (isempty(obj.linModel.bcDofAbs))
                    boundaryNodes = (length(obj.linModel.internalDofAbs) + ...
                         1):(length(obj.linModel.internalDofAbs) + length(obj.linModel.connDofAbs));
%                     F(boundaryNodes, :) = 0;
% %                     keyboard
%                     warning('Natural BCs not present; boundary nodes zeroed');
                else
                    warning('Spring stiffness inactive, boundary Nodes NOT zeroed');
                end

                % (4) Send to Abaqus
                [displacement, cf] = obj.abint.static(obj.linModel.bcDofAbs, F, loadDof, 'nonlinear', springs);
%                 [displacement, cf] = obj.abint.static(obj.linModel.connDofAbs, F, loadDof, 'nonlinear', springs);
                
            else
                F = fitData.F;
                P = fitData.P;
                displacement = fitData.displacement;
            end
            
            
            % (5) Perform fit
            lambda = basis'*Kl*basis;
            
%             [Knl, tlist, dispErr, forceErr] = noregress(lambda, basis, F, ...
%                     displacement, quads, triples, flist);
                
            if isequal(size(lambda), [1, 1])
                lambda = sqrt(lambda)/2/pi;
            end
            [Knl, tlist, dispErr, forceErr] = noregress(lambda, basis, F, ...
                    displacement, quads, triples, flist);
            fitTime = toc;
            
            % (6) Add all non-included modes
            t = tlist(:); 
            for i = ccMind
                tCC = (t == (nM + i));
                t(tCC) = nFI + i;
            end
            tlistActual = reshape(t, size(tlist));
            allFIModes = 1:nFI;
            allCCModes = (nFI + 1):(nFI + nCC);
            allFIModes(fiMind) = []; allCCModes(ccMind) = [];
            
            KnlActual = zeros(nFI + nCC, size(Knl, 2));
            KnlActual([fiMind(:); nFI + ccMind(:)], :) = Knl;

            tlistEmpty = repmat([allFIModes, allCCModes]', 1, 3);
            nEmpty = size(tlistEmpty, 1);
            obj.NLROM.tlist = [tlistActual; tlistEmpty];
            
            obj.NLROM.Knl = [KnlActual, zeros(nFI + nCC, nEmpty)];
            [obj.NLROM.N1, obj.NLROM.N2, obj.NLROM.Nlist] = Knl2Nash(obj.NLROM.Knl, obj.NLROM.tlist);
            obj.NLROM.dispErr = dispErr; obj.NLROM.forceErr = forceErr;
            obj.NLROM.fitTime = fitTime;
            
            % Optional outputs
            data.basis = basis; data.F = F; data.P = P; data.displacement = displacement;
            data.fitTime = fitTime; data.Kl = Kl;
            data.dispErr = dispErr; data.forceErr = forceErr;
            
            % Display which mode to add to reduce displacement residual the
            % most
            
        end
        
        function obj = zeroNLROM(obj, ncc)
            %    obj = zeroNLROM(obj, ncc)
            %
            % Construct a zero-valued NLROM which will allow simulation of
            % the component with no associated nonlinearity. 
            %
            % INPUT
            %
            % ncc: Number of CC modes in the parent assembly.
            
            N = length(obj.linModel.omegaN) + ncc;
            Knl = zeros(N);
            tlist = repmat((1:N)', 1, 3);
            [NLROM.N1, NLROM.N2, NLROM.Nlist]= Knl2Nash(Knl,tlist);
            NLROM.Knl = Knl;    
            NLROM.tlist = tlist;
            obj.NLROM = NLROM;
        end
            
        
        function diagnostics(obj, modeRange)
            %    diagnostics(obj, modeRange)
            %
            % Perform basic diagonostics on the NLROM fit. Not currently
            % implemented. 
            
            error('Not Implemented')
            if nargin < 2
                modeRange = [1, length(obj.linModel.fn)];
            end
            % Diagnostics
            % First load cases are positive and negative single mode forces:
            Lambda = diag((2*pi*obj.linModel.fn).^2);
            singleModes = 1:2*length(obj.mind);
            Q = Lambda\obj.linModel.phi'*obj.util.F(:, singleModes);
            
            Qnl = obj.linModel.phi'*obj.linModel.M*obj.util.disp(:, singleModes);
            
            ratio = Q./Qnl;
            [~, indices] = max(abs(ratio), [], 1);
            
            % Summary of single-mode displacement ratios
            fprintf('Summary of single-mode displacement ratios:\n')
            fprintf('\tMode\tLoad\tRatio [%%]\n');
            
            for i = 1:2*length(obj.mind)
                fprintf('\t%i\t\t%i\t\t%.2f\n', obj.mind(indices(i)), ...
                    obj.util.P(i, 1), 1/ratio(indices(i), i)*100);
            end
                    
            % Plot modes excited by each modal force.  If modes not in the ROM
            % are excited then the model may fail.
            mind_maxdisp = sign(obj.util.P(1:2*length(obj.mind),1)).*vec(obj.mind(abs(obj.util.P(1:2*length(obj.mind),1))));
            q = obj.linModel.phi.'*obj.linModel.M*obj.util.disp(:, 1:2*length(obj.mind));
            figure;
            plot(1:length(q), q(:, 1:length(obj.mind)), '.-', 'linewidth', 2); grid on;
            hold on; 
            plot(1:length(q), q(:, length(obj.mind) + 1:end), '+--', 'linewidth', 2);  
            hold off;
            legend(num2str(mind_maxdisp)); xlim(modeRange);
            xlabel('\bfMode Number'); ylabel('\bfModal Displacement');
            title('\bfModes Excited by Each Modal Force');
            
%             figure;
%             plot(1:size(obj.util.disp,2), obj.linModel.phiTM*obj.util.disp,'.');
%             legend(num2str(obj.linModel.fn.'));
%             xlabel('Load Case'); ylabel('Modal Displacement');
%             title('\bfModes Excited by Each Load Case');
        end
         
        function [fnl, N1_u, N2_u] = nlForce(obj, u)
            %    [fnl, N1_u, N2_u] = nlForce(obj, u)
            %
            % Compute the nonlinear force resulting from a displacement u
            % according to the ROM's Nash-form coefficients.
            %
            % The Nash-form coefficients are given by N1, N2, and Nlist.
            % From Knl2Nash.m:
            %   In theory:
            %    Knl(u)= [1/2 * N1(u)+ 1/3 * N2(u,u)] * u
            %     and
            %    dKnl(u) = [N1(u) + N2(u,u)]*u;
            %      where N1(u) is N1 evaluated at u 
            %      where N2(u,u) is N2 evaluated at the Nlist quad combos of u
            %
            %    EXAMPLE evaluation of N1(u) and N2(u,u):
            %     u_nl = u(Nlist(:,1)).*u(Nlist(:,2));
            %     for jj=1:Neqn;
            %       N1_u(:,jj)= N1(:,:,jj) * u;
            %       N2_u(:,jj)= N2(:,:,jj) * u_nl;
            %     end;
            %
            % INPUT
            %
            % u: Modal displacement of the component
            %
            % OUTPUT
            %
            % fnl: Nonlinear modal restoring force generated by the
            % component.
            %
            % N1_u: Quadratic force Jacobian matrix
            %
            % N2_u: Cubic force Jacboian matrix
            
            N1 = obj.NLROM.N1; N2 = obj.NLROM.N2; Nlist = obj.NLROM.Nlist;
            u_nl = u(Nlist(:, 1)).*u(Nlist(:, 2));
            n = length(u); N1_u = zeros(n); N2_u = zeros(n);
            for j = 1:n
                N1_u(:, j) = N1(:, :, j)*u;
                N2_u(:, j) = N2(:, :, j)*u_nl;
            end
            fnl = (1/2*N1_u + 1/3*N2_u)*u;
%            keyboard
        end
        
        
        function sys = defsys(obj, mode)
            %    sys = defsys(obj, mode)
            %
            % Create the "sys" object expected by NNMcont using the
            % included "NNMoptions" structure.
            %
            % INPUT
            %
            % mode: NNM to compute
            %
            % OUTPUT
            %
            % sys: "sys" structure expected by NNMcont. Must be global;
            %      call "NNMcont('initialize') to proceed without wiping 
            %      out all workspace variables. 
            
            % Linear System:
            sys.Klin = []; % obj.NLROM.Khat;
            sys.Mlin = []; %obj.NLROM.Mhat;
            
            % Nonlinearities:
            sys.nl = NL_ROM(obj.NLROM.N1, obj.NLROM.N2, obj.NLROM.Nlist);
            
            % Options
            opt = obj.NNMoptions;
            sys.norm = opt.norm;
            sys.NumMeth = opt.NumMeth;
            sys.TFS = opt.TFS;
            sys.PrecS = opt.PrecS;
            sys.itoptS = opt.itoptS;
            sys.itmaxS = opt.itoptS;
            sys.RelTolode = opt.RelTolode;
            sys.AbsTolode = opt.AbsTolode;
            sys.NRprec = opt.NRprec;
            sys.alpha_m = opt.alpha_m;
            sys.h = opt.h;
            sys.hMax = opt.hMax;
            sys.hMin = opt.hMin;
            sys.betamax = opt.betamax;
            sys.betamin = opt.betamin;
            sys.wstop = opt.wstop;
            sys.shootinPeriod = opt.shootingPeriod;
            
            % Mode and filename
            mindstr = strrep(num2str(obj.mind), '  ', '-');
            sys.filename = sprintf('CMS_%s_NNM-%i', obj.abint.modelname, mode);
            sys.mode = mode;
            
        
        end
        
        

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%% "SET" METHODS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function obj = setLinModel(obj, linModel)
            %    obj = setLinModel(obj, linModel)
            %
            % Set the CBICE "linModel" property.
            
            obj.linModel = linModel;
        end
        
        function obj = setNLROM(obj, NLROM)
            %    obj = setNLROM(obj, NLROM)
            %
            % Set the CBICE "NLROM" property.
            
            obj.NLROM = NLROM;
        end
        
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% UTILITY METHODS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        function save(obj)
            %    \save(obj)
            %
            % Save the CBICE object using the "filepath" and "filename"
            % properties. 
            
            cbice = obj;
            filename = [obj.abint.modelname];
            save(fullfile(obj.filepath, filename), 'cbice');
        end
        
        function plot(obj, amplitudes)
            %    plot(obj, amplitudes)
            %
            % Plot the part using the given modal amplitudes. 
            %
            % INPUT:
            %
            % amplitudes: [nFI + nB x 1] vector of the form [qi; xb] where 
            %              qi are the fixed interface modes and xb are the
            %              boundary DOF. 
            
            % Check lengths on the input coordinates
            if ((size(amplitudes, 1) ~= (length(obj.linModel.connDofAbs) ...
                    + length(obj.linModel.omegaN))) || (size(amplitudes, 2) ~= 1))
                error('Wrong size on "amplitudes" in CBICE plot');
            end
            
            % Build X vector of displacements
            qi = amplitudes(1:length(obj.linModel.omegaN));
            xb = amplitudes((length(obj.linModel.omegaN) + 1):end);
            
            X = [obj.linModel.phiFI*qi + obj.linModel.psiIB*xb;
                 xb; zeros(size(obj.linModel.bcDofAbs))];
            
            obj.abint.deform(X(obj.linModel.dofOrder), 'norm');
            
        end
            
        function plotMode(obj, mode, scale)
            %    plotMode(obj, mode, scale)
            %
            % Plot a fixed interface or constraint (not characteristic
            % constraint) mode of the assembly. 
            %
            % INPUT
            %
            % mode: Mode to plot. Values 1:nFI will plot FI modes; values
            %       (nFI + 1):(nFI + nB) will plot constraint modes.
            
            nM = length(obj.linModel.omegaN);
            nB = length(obj.linModel.connDofAbs);
            if (mode <= nM)
                amps = zeros(nM + nB, 1); 
                amps(mode) = scale;
            else
                Z = zeros(nB, 1); Z(mode - nM) = 1;
                amps = [zeros(nM, 1);
                        Z(:)]; 
            end
            obj.plot(amps);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%% OPERATOR OVERLOADS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function disp(obj)
            %    disp(obj)
            %
            % Display the object in a readable format
            
            fprintf('\nCBICE Object\n');
            fprintf('\t%s\n', obj.abint.modelname);
            if isempty(obj.NLROM)
                fprintf('\tNo ROM fit exists\n');
            else
                fprintf('\tROM fit exists');
            end
            byteSize = whos('obj'); byteSize = byteSize.bytes;
            fprintf('\tSize of %.3f mb\n\n', byteSize/1024^2);
            
        end
        
    end
    
    methods(Static)
       
        function opt = parseOptions(options)
            %    opt = parseOptions(options)
            %
            % Parse the supplied NLROM options structure and assign
            % defaults if necessary.
            
            % Options
            % Scaling Method for Applied Loads? 1- Constant force, [0-
            % Constant displacement]
            try opt.cf = options.cf; catch err; opt.cf = 0; end
            % Reduce scale factors of 2 and 3 combos? [1- Yes], 0- No
            try opt.rf = options.rf; catch err; opt.rf = 1; end
            % do constrained fit in no_regress? [1- Yes], 0- No
            try opt.constr = options.constr; catch err; opt.constr = 1; end
            % include quadratics in no_regress? [1- Yes], 0- No
            try opt.quads = options.quads; catch err; opt.quads = 1; end
            % include triple cubics in no_regress? [1- Yes], 0- No
            try opt.triple = options.triple; catch err; opt.triple = 1; end
            % include membrane expansion? [1- Yes], 0- No
            try opt.memb = options.memb; catch err; opt.memb = 1; end
            % Overwrite linear model? [1 - Yes], 0 - No
            try opt.ovrwrt = options.overwrite; catch err; opt.ovrwrt = 1; end
        end
        
        function opt = parseNNMOptions(opt)
            %    opt = parseNNMOptions(opt)
            %
            % Parse the supplied NNM options structure and assign
            % defaults if necessary.
            
            % Options for the NNMcont continuation code
            if ~isfield(opt, 'norm')
                opt.norm = 5e-8; % Make this smaller if having convergence issues
            end
            if ~isfield(opt,'NumMeth')
                opt.NumMeth='NEWMARK'; % Numerical method
            end
            if ~isfield(opt,'TFS')
                opt.TFS=100; % Time steps per period integration
            end
            if ~isfield(opt,'PrecS'),
                opt.PrecS = 1E-6; % 
            end
            if ~isfield(opt,'itoptS'),
                opt.itoptS = 3; % No idea what this does
            end
            if ~isfield(opt,'itmaxS'),
                opt.itmaxS = 10; % "h_max???"
            end
            if ~isfield(opt, 'RelTolode'),
                opt.RelTolode = 1.0e-7; % ODE relative tolerance
            end
            if ~isfield(opt,'AbsTolode')
                opt.AbsTolode = 1.0e-7; % ODE absolute tolerance
            end
            if ~isfield(opt,'NRprec'),
                opt.NRprec = 1.0e-7; % Don't know what this does 
            end
            if ~isfield(opt,'alpha_m'),
                opt.alpha_m = 0; % Don't know what this does
            end
            if ~isfield(opt,'alpha_f'),
                opt.alpha_f = 0.01; % Don't know what this does
            end
            
            if ~isfield(opt,'h'); opt.h = 1E-5; end
            if ~isfield(opt, 'hMax'); opt.hMax = 1E-2; end
            if ~isfield(opt, 'hMin'); opt.hMin = 1E-9; end
            if ~isfield(opt, 'betamax'); opt.betamax = 90; end
            if ~isfield(opt, 'betamin'); opt.betamin = 0; end
            if ~isfield(opt, 'wstop'); opt.wstop = []; end
            if ~isfield(opt, 'shootingPeriod'); opt.shootingPeriod = 1; end
            if strcmpi(opt.shootingPeriod, 'HALF'); opt.shootingPeriod = 1; end
            if strcmpi(opt.shootingPeriod, 'FULL'); opt.shootingPeriod = 2; end
            if (opt.shootingPeriod ~= 1) && (opt.shootingPeriod ~= 2)
                error('Incorrect specification for shootingPeriod')
            end
        end
                
    end
end




