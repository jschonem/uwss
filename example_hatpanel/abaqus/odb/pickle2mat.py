import pickle
import scipy.io
import sys
filename = sys.argv[1]
with open(filename, "rb") as fid:
	data = pickle.load(fid, encoding="latin1")
matname = filename.split(".")[0]
scipy.io.savemat("%s.mat" % matname, data)
