# -*- coding: mbcs -*-
#
# Abaqus/CAE Release 6.12-2 replay file
# Internal Version: 2012_06_28-23.52.08 119883
# Run by Joe on Tue Aug 23 16:45:52 2016
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=170.660415649414, 
    height=269.522216796875)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from caeModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
openMdb('testModel.cae')
#: The model database "C:\Users\Joe\Dropbox\matlab\tools\SubStructJDS\Example_HatPanel\Abaqus\CAE\testModel.cae" has been opened.
session.viewports['Viewport: 1'].setValues(displayedObject=None)
session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    referenceRepresentation=ON)
p = mdb.models['assembly_soft'].parts['hat_stiffener']
session.viewports['Viewport: 1'].setValues(displayedObject=p)
p1 = mdb.models['assembly_stiff'].parts['hat_stiffener']
session.viewports['Viewport: 1'].setValues(displayedObject=p1)
a = mdb.models['assembly_stiff'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    optimizationTasks=OFF, geometricRestrictions=OFF, stopConditions=OFF)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(mesh=ON)
a = mdb.models['assembly_soft'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
a = mdb.models['assembly_stiff'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
a = mdb.models['stiffener_soft'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
a = mdb.models['stiffener_stiff'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
a = mdb.models['panel'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
mdb.ModelFromInputFile(name='testModel', 
    inputFileName='C:/Users/Joe/Dropbox/matlab/tools/SubStructJDS/Example_HatPanel/Abaqus/models/testModel.inp')
#: The model "testModel" has been created.
#: Warning: Undefined node and element ids have been removed from some node and element sets
#: The part "PART-1" has been imported from the input file.
#: 
#: WARNING: The following keywords/parameters are not yet supported by the input file reader:
#: ---------------------------------------------------------------------------------
#: *PREPRINT
#: The model "testModel" has been imported from an input file. 
#: Please scroll up to check for error and warning messages.
a = mdb.models['testModel'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
