%% Example script - generates substructure model for a 3 component system
% with a panel that is fixed to two hat stiffeners, creates Craig-Bampton
% models and performs interface reduction.
%
% To use this you must have the abaqus models stiffener.inp and panel.inp
% in the subfolder Abaqus\models\
% Also the Matlab classes CBSS, CCSS, etc... should be on the Matlab path.
%
% By Joseph Schoneman, August 2016
%
% This script and the Abaqus input files should be moved to a local folder
% on your computer (not in Dropbox).  It will create subdirectories ABINTs,
% CBICEs, etc... and store the results there.

clear; clc; close all
addpath('..'); % Adds the class files above to path

%% #1 Generate CBICE for Hat Stiffener
workingDir = '..\scratch'; % where to run Abaqus
filepathAbint = 'ABINTs'; % where to save these
    if ~exist(filepathAbint,'dir'); mkdir(filepathAbint); end
filepathCbice = 'CBICEs';
    if ~exist(filepathCbice,'dir'); mkdir(filepathCbice); end
pythonpath = '..\python';

% Make ABINT object and save for later
ab = ABINT(workingDir, filepathAbint, pythonpath);
% Read an input file
inputPath = 'Abaqus\models';
inputFile = 'stiffener_soft'; % don't include the *.inp extension
ab = ab.readInp(inputPath, inputFile);

ab = ab.getDof();
[M, K] = ab.matrix([]);
save(ab); % stiffener.mat ?

% Make CBICE object and save
thick = 1.25;
cbi = CBICE(ab, thick, workingDir, filepathCbice);
cbi.linModel.M = M; cbi.linModel.K = K;
save(cbi); % Creates: CBICEs/stiffener.mat

%% #2 Generate CBICE for Panel

% Make ABINT object and save for later
ab = ABINT(workingDir, filepathAbint, pythonpath);
% Read an input file
inputPath = 'Abaqus\models';
inputFile = 'panel'; % don't include the *.inp extension
ab = ab.readInp(inputPath, inputFile);
ab = ab.getDof();
[M, K] = ab.matrix([]);
save(ab); % panel.mat ??

% Make CBICE object and save
thick = 1.25;
cbi = CBICE(ab, thick, workingDir, filepathCbice);
cbi.linModel.M = M; cbi.linModel.K = K;
save(cbi); % Creates: CBICEs/panel.mat

%% #3 Generate CCSS object for the assembled structure
models = load('CBICEs/stiffener_soft');
stiffener = models.cbice;
models = load('CBICEs/panel');
panel = models.cbice;

filepath = 'CCSSs'; 
    if ~exist(filepath,'dir'); mkdir(filepath); end
filename = 'testModel';
ccs = CCSS(filepath, filename);

% Now add each part to the CCSS object "ccss"

% Reference node, base location, and offset for hat stiffeners
refNodeHat = 1;
locationHat = [0, 0, 0];

% Boundary and connector sets for hat stiffeners
boundaryNodes = stiffener.abint.nodeSets.('HAT_STIFFENER-1_BCSET');
boundaryHat = [boundaryNodes, ones(length(boundaryNodes), 6)]; % Natural boundary conditions
connNodes = stiffener.abint.nodeSets.('HAT_STIFFENER-1_RETAINEDDOFS'); 
connHat = [connNodes, ones(length(connNodes), 6)];

% Add hat stiffener
nmode = 1;
ccs = ccs.addPart(stiffener, refNodeHat, locationHat, boundaryHat, ...
    connHat, nmode);

% Add other hat stiffener, offset by 500mm
ccs = ccs.addPart(stiffener, refNodeHat, locationHat + [500, 0, 0], ...
    boundaryHat, connHat, nmode);

% Add panel
refNode = 182; % Which node has the location given below.
boundary = []; % No natural boundary conditions on the component
connNodes = panel.abint.nodeSets.('PANEL_SECTIN-1_RETAINEDDOFS');
connPanel = [connNodes, ones(length(connNodes), 6)];
location = [0, 0, 0];
nmode = 5;

ccs = ccs.addPart(panel, refNode, location, boundary, connPanel, nmode);

% Build the connection array
ccs = ccs.makeConnections();

% Build the CB model of each part
% ccs = ccs.buildCBModels('abaqus');
ccs = ccs.buildCBModels('matlab');

% Generate CC modal model
ccs = ccs.buildCCSystem(10, 5);

% Abaqus comparison.
% Load full-order modal results
% This requires that the ODBs in "Abaqus\odb" be converted to the installed
% version. They were originally generated in release 2014. If you have an
% older version of Abaqus, then re-run a modal solution on the assembly
% input file and load the resulting ODB below. 
% odbpath = 'Abaqus\odb'; %'C:\temp';
% odbfile = 'assembly_soft';
% pythonpath = '..\python';
% ccs = ccs.loadOdb(odbpath, odbfile, pythonpath);
% ccs = ccs.makeMac();
% ccs.comparePlot(1, 10);

ccs.save(); % Creates CCSSs\testModel.mat
% Also save all CBICE parts with their modes included
for i = 1:length(ccs.parts)
    ccs.parts(i).part.save();
end

% Plot the system
ccs.plot();
title('\bfHat Stiffener/Beam Assembly');

%% Plot first four assembly modes

fn = ccs.fn;
phi = ccs.phi;
figure('units', 'inches', 'position', [0.5, 0.5, 12, 6]);

for i = 1:4
    subplot(2, 2, i);
    ccs.deform(phi(:, i));
    title(sprintf('\bMode %i; Frequency = %.1f Hz', i, fn(i)));
    axis off
end







