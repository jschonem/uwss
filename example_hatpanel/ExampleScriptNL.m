%% Example script - generates nonlinear substructure model for a 3 
% component system using the characteristic constraint mode interface
% reduction method.
%
% This assumes that a CCSS object has been created and saved to
% "CCSSs/testModel.mat". Use the associated .m file "ExampleScriptLin" to
% generate the CCSS substructure object. 
%
% By Joseph Schoneman, August 2016
%

clear; clc; close all
addpath('..'); % Adds the class files above to path
addpath('../functions'); % Adds necessary functions to path

%% Perform a very simple nonlinear fit
% This is not a good NLROM, it simply demonstrates the concept. 

load('CCSSs/testModel');
ccs = ccss;
clear('ccss');

ccs = ccs.initNLSystem();
nlPart = 3; % Part to treat as nonlinear (the cross beam)
modeInds = [1, 3, 5, 6]; % Absolute indices of component QR vectors to retain.
% (mode 6 is the remnant of CC mode 1 after QR factorization)
springs = true; % Include spring stiffness in the nonlinear fit?
displacementLevel = 1; % # of thicknesses of displacement to push the modes to
quads = 0; % Do not include quadratic terms in fit
triples = 0; % Do not include "triple cubic" terms in fit. 

ccs = ccs.addNLPartQR(nlPart, modeInds, springs, displacementLevel, ...
                      quads, triples); 


%% Integration comparison                  

fnl = CMS_INT(ccs); % Integration helper object
% Nonlinear equation of motion. Note that these are integrated in terms of
% the component fixed interface and system characteristic constraint modal 
% amplitudes of the.
eomNl = @(t, z) [zeros(length(z)/2), eye(length(z)/2);
                 -ccs.Mcc\ccs.Kcc, zeros(length(z)/2)]*z - [zeros(length(z)/2, 1);
                                                            fnl.fint_nl(z)];
             
% Linear equation of motion
eomLin = @(t, z) [zeros(length(z)/2), eye(length(z)/2);
                 -ccs.Mcc\ccs.Kcc, zeros(length(z)/2)]*z;
                 
             
% Integrator settings
ndof = ccs.nModalDOF + ccs.nCharDOF;
T = 1/ccs.fn(1); t = linspace(0, T, 1000);
% Set displacement initial condition in first mode of the system.
q0 = 0.05*ccs.phiCC(:, 1); qd0 = q0;
z0 = [q0; qd0];

% Run integrations
% ODE45 NOT the preferred way to integrate these equations; works okay for
% demonstration purposes (takes longer and longer for larger initial
% conditions.)
[~, znl] = ode45(eomNl, t, z0);
qnl = ccs.phiCC\znl(:, 1:end/2)';
[~, zlin] = ode45(eomLin, t, z0);
qlin = ccs.phiCC\zlin(:, 1:end/2)';

%% Plot modes (1, 3, 5) for each case

figure('units', 'inches', 'position', [0.5, 0.5, 12, 8]);

subplot(2, 1, 1);
plot(t, qlin([1, 3, 5], :), 'linewidth', 2);
legend('location', 'southeast', 'Mode 1', 'Mode 3', 'Mode 5');
grid on
xlim([0, T]);
ylabel('\bfModal Amplitude');
title('\bfLinear Modal Response');


subplot(2, 1, 2);
plot(t, qnl([1, 3, 5], :), 'linewidth', 2);
grid on
xlim([0, T]);
xlabel('\bfTime [s]');
ylabel('\bfModal Amplitude');
title('\bfNonlinear Modal Response');

