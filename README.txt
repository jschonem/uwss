SubStructJDS MATLAB substructuring toolset. Research-focused tool for explorations in 
linear and nonlinear modal substructuring of large-scale finite element models.
Interfaces with Abaqus FEA software using a Python model data extraction routine, 
"odb2mat_v2.py." 

The function of this toolset is described in Chapter 3 and 
Appendix A of the thesis "Advances in Modal Substructuring of Geometrically 
Nonlinear Finite Element Models" by Joe Schoneman, 2016. This is available 
online at

http://sd.engr.wisc.edu/wp-uploads/2016/08/jdschoneman_thesis_vf.pdf

Additional help may
be found in the MATLAB files of each class. Either view the files directly 
or use the command "doc XXXX", where "XXXX" is the class name of interest, 
to view an HTML-based help document for the class. Note that "help XXXX" 
only displays top-level help for the class and is of little use. 

MATLAB's help for class methods (functions) is somewhat limited. "help XXXX.method" 
will work as long as "XXXX" is the actual name of the class file. For an instance 
of a class (i.e. a variable in the workspace) which is named "yyyy," the call 
"yyyy.method" will be unrecognized. Use the actual class name for method-level help. 
Alternatively, from the HTML viewer produced by "doc XXXX," click on each method 
name to obtain expanded help.

If you are using this toolbox for the first time, start by running the file 
"example_hatpanel/ExampleScript.m". This script will generate inidividual ABINT
and CBICE objects for each component, assemble them into a CCSS, and then generate
a substructured NLROM using single-mode nonlinear terms only. (The NLROM will not
be accurate, it is for demonstration only.) 

To familiarize yourself with the toolbox, start with the scripts:

example_hatPanel\ExampleScriptLin.m
example_hatPanel\ExampleScriptNL.m

which will perform linear and nonlinear substructuring of the hat-panel/stiffeners
system from the thesis above. (Note that the nonlinear model is for demosntration only
and will not be particularly accurate.)

The classes in this toolset are:

ABINT

  Abaqus interface class. Used to store Abaqus models as MATLAB data for
  rapid manipulation. Can write model input files and step 
  input files for selected analysis procedures. Reads 
  results and stores as necessary.

CBICE - Craig Bampton Implicit Condensation/Expansion Module

  CBICE serves as the link between ABINT and the full assembly. During the 
  linear substructuring procedure, CBICE calculates interface and 
  constraint modes of its associated component and assembles them into a 
  Craig-Bampton representation for use by the assembly. CBICE also performs 
  the NLROM construction procedure for each component, based on a set of 
  deformations which are supplied in terms of the component's free 
  interface and constraint modes. Basis vector transformation, force scale
  determination, and nonlinear coefficient estimation all occur within this 
  class.


CBSS - Craig Bampton Substructuring Module

  CBSS is written to handle assembly-level substructuring, along with 
  “administrative” functions, such as placement of each component, 
  determination of the appropriate degrees of freedom to be coupled, and
  verification of the computed assembly modes with those of a full-order 
  finite element model.

CCSS - Characteristic Constraint Substructuring Model

  CCSS enables linear substructuring via characteristic constraint modes. The class 
  performs appropritate system partition, calls for secondary modal analysis of the 
  assembly boundaries, and applies the CC modal reduction to the assembly boundary modes.
  All nonlinear model generation is also handled through CCSS, with both FI/CC and QR
  vector methods available, each of which may be used via boundary stiffness reduction
  or the "in situ" NLROM generation process.

CMS_INT - Component mode synthesis integration helper

  Accepts a CCSS object and implements functions to provide the nonlinear restoring force,
  nonlinear force Jacobian, and nonlinear potential energy at a given deformation. The
  for-loop based implementation is inefficient, particularly when used with an explicit
  integrator (such as ODE45) that takes small timesteps. It is suitable for use with a
  Newmark scheme, however. 