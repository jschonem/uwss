# -*- coding: utf-8 -*-
"""
Load a pickle and save a mat
"""


import pickle
import scipy.io
import sys

filename = sys.argv[1]
# print filename
with open(filename, 'rb') as fid:
    data = pickle.load(fid, encoding='latin1')
matname = filename.split('.')[0]
scipy.io.savemat('%s.mat' % matname, data)
    
