# File: odb2mat.py
# Written:  J Hollkamp
# Version: 5.5
# Date: 6/3/2014
# Purpose: convert abaqus odb-files to matlab mat-files
#
# Options:
#     what             print out what is in the file
#     model            save the model data
#     shapes           save the mode shapes
#     disp             save the displacement data
#     appliedF
#     reactions
#     history          save all the history data
#     fieldOutputs
#     sets             save the element and node sets from instances
#
# 6/2/2014: fix bug for 2D structural output
# 6/3/2014: fixed bug in fieldOutputs
# 2/3/2016: [JDS, UW-Madison] Added further capabilities to model export:
#             - Exports all part-based node sets and element sets
#             - Exports sections, materials, profiles, and assignments
#             - Tracks element types
#             - "Shapes" also exports DOF vector
#             - Also re-wrote some functionality for more robustness
#             
#------------load in the libraries ---------------------------

# First, get the utilities to access abaqus odb files
#
# if the computer is the SGI cluster
#  append the path to find scipy
#

from odbAccess import *
from abaqusConstants import *
import sys

from socket import gethostname
hostName=gethostname()
if hostName == 'UVL00000270-P000' :
     sys.path.append('/usr/lib64/python2.6/site-packages')

import numpy
import scipy
from scipy import io as sio

#-----------find out the option------------------------------

nargin=len(sys.argv)

if nargin > 1:
    jobName=sys.argv[1]
else:
    jobName='job-1'    # DEFAULT jobName

if nargin > 2:
    option=sys.argv[2]
else:
    option='what'      # DEFAULT option

odbFileName= jobName + ".odb"
matFileName = jobName + "FromAbaqus"
db = openOdb(odbFileName)

#-------------------option: what---------------------------------

if option == 'what':

    # import libraries specifically for this python script
    from textRepr import *
    import string

    maxRD=1
    jobAppend=""

    if nargin > 3:
       try:
         maxRD=string.atoi(sys.argv[3])
       except ValueError:
         jobAppend="."+sys.argv[3]

    if nargin > 4:
        maxRD=string.atoi(sys.argv[4])

    exec( "pt=db" + jobAppend)
    prettyPrint(pt, maxRecursionDepth=maxRD)

#-------------------option: model---------------------------------

if option == 'model':
    
    nSets = 1 # This is a hack for the save options at bottom
    
    rA = db.rootAssembly # Label root assembly
    partKey = rA.instances.keys()[0]
    part = rA.instances[partKey]
    print('Extracting from Part %s' % partKey)

    # Start empty dictionary
    dictionary = dict()
    
    # Extract nodes
    abNodes = part.nodes
    nnodes = len(abNodes)
    nodes = numpy.zeros((nnodes, 4), dtype = float)
    print('  #nodes: %i' % nnodes)

    for jj in range(nnodes):
        nodes[jj][0] = abNodes[jj].label
        nodes[jj][1] = abNodes[jj].coordinates [0]
        nodes[jj][2] = abNodes[jj].coordinates [1]
        nodes[jj][3] = abNodes[jj].coordinates [2]
    
    dictionary['nodes'] = nodes

    # Extract elements
    elements = part.elements
    neles = len(elements)
    eles = numpy.zeros((neles, 10), dtype = float) # Changed to float
    eleTypes = ['']*neles # This holds element types by Abaqus designation
    print('  #elements: %s' % neles)
    warning20node=0;
     
    for jj in range(neles):
        con = elements[jj].connectivity
        ncon = len(con)

        eleTypes[jj] = elements[jj].type
        eles[jj][0] = elements[jj].label        
        
        # assign element types for Gordon's viewer
        if ncon == 2:         # Bar, Beam
            eles[jj][1]=2
        if ncon == 4:         # Quad
            eles[jj][1]=9
        if ncon == 3:         # Quadratic beam (write something special to check for triangles)
            eles[jj][1]=2
        if ncon == 8:         # Hex
            eles[jj][1]=12
        if ncon == 6:         # Pent
            eles[jj][1]=13
        if ncon == 20:        # We'll trick it into thinking its a Hex
            eles[jj][1]=12
        
        ii=0

        # Matt added this to fix incompatibility with higher order Hex.
        if ncon > 8:
             ncon=8
             if warning20node == 0:
                  print ' Warning, only saving first 8 nodes of element # '+ str(jj)
                  print con
                  print ' No additional warnings will be shown.'
                  warning20node=1
                  
        for ii in range(ncon):
            eles[jj][ii+2] = con[ii]


    dictionary['elements'] = {'eles' : eles, 'eleTypes' : eleTypes}

    # Extract node sets and element sets
    abNodeSets = part.nodeSets
    nodeKeys = abNodeSets.keys()
    nodeSets = dict()

    for key in nodeKeys:
        nset = abNodeSets[key]
        labels = numpy.zeros((nset.nodes.__len__(), 1), dtype = float)
        for ii in range(nset.nodes.__len__()):
            labels[ii] = nset.nodes[ii].label
        # If the nodeset name is longer than 31 characters, truncate (MATLAB reasons)
        if (len(nset.name) > 31):
            nameString = nset.name[-31:]
        else:
            nameString = nset.name
        nodeSets[nameString] = labels
    
    if (len(nodeSets) > 0):
        dictionary['nodeSets'] = nodeSets

    abEleSets = part.elementSets
    eleKeys = abEleSets.keys()
    eleSets = dict()

    for key in eleKeys:
        elset = abEleSets[key]
        labels = numpy.zeros((elset.elements.__len__(), 1), dtype = float)
        for ii in range(elset.elements.__len__()):
            labels[ii] = elset.elements[ii].label
        # If the elset name is longer than 31 characters, truncate (MATLAB reasons)
        if (len(elset.name) > 31):
            nameString = elset.name[-31:]
        else:
            nameString = elset.name
        eleSets[nameString] = labels
    
    if (len(eleSets) > 0):
        dictionary['eleSets'] = eleSets

    # Extract material properties
    abMats = db.materials
    matKeys = abMats.keys()
    mats = dict()
    for key in matKeys:
        mat = abMats[key]
        mats[key] = {'density' : mat.density.table, 'elastic' : mat.elastic.table}

    dictionary['materials'] = mats

    # Santize part name
    partKey = partKey.replace('-', '_')

    #if nSets == 1:
    print '  MATLAB file: ' + matFileName
    sio.savemat(matFileName, {'FE' : dictionary}, oned_as='column')
    #else:
    #    print '  MATLAB file: ' + matFileName+str(kk)
    #    sio.savemat(matFileName+str(kk), {setName : dictionary}, oned_as='column')

#-------------------option: shapes---------------------------------

if option == 'shapes':

    setName='mode'
    step_names=db.steps.keys()
    print '\nsteps in Odb:'
    print  step_names
    if nargin > 3:
      step_name=sys.argv[3]
    else:
      step_name=step_names[-1]
    print '\nextracting step:'
    print  step_name

    myStep=db.steps[step_name]
    numFrames=len(myStep.frames)-1
    numValues=len(myStep.frames[0].fieldOutputs['U'].values)

 #   print numFrames
 #   print numValues

    # frames[0] is the base state, really have numFrames-1 numFrames
    freq=numpy.zeros((numFrames,1),dtype=float);
    disp=numpy.zeros((6*numValues,numFrames),dtype=float);
    dof = numpy.zeros((6*numValues, 1), dtype=float); # JDS Modification
    descript=[]

    v=0
    while v < numFrames:
      myMode=myStep.frames[v+1]
      freq[v]=myMode.frequency
      descript.append(myMode.description)
      n=0
      while n < numValues:
           
          try:
             data=myMode.fieldOutputs['U'].values[n].data
          except OdbError:
             data=myMode.fieldOutputs['U'].values[n].dataDouble
          
          # Add displacements
          disp[6*n+0][v]=data[0]
          disp[6*n+1][v]=data[1]
          disp[6*n+2][v]=data[2]
          
          # Add DOF to DOF vector using [node.1, node.2, node.3, ...] convention
          if v == 0:
              dof[6*n + 0] = myMode.fieldOutputs['U'].values[n].nodeLabel + 0.1
              dof[6*n + 1] = myMode.fieldOutputs['U'].values[n].nodeLabel + 0.2
              dof[6*n + 2] = myMode.fieldOutputs['U'].values[n].nodeLabel + 0.3

          if myMode.fieldOutputs.has_key('UR'):

             try:
                data=myMode.fieldOutputs['UR'].values[n].data
             except OdbError:
                data=myMode.fieldOutputs['UR'].values[n].dataDouble
             
             # Add displacements
             disp[6*n + 3][v] = data[0]
             disp[6*n + 4][v] = data[1]
             disp[6*n + 5][v] = data[2]

             # Add DOF
             if v == 0:
                 dof[6*n + 3] = myMode.fieldOutputs['UR'].values[n].nodeLabel + 0.4
                 dof[6*n + 4] = myMode.fieldOutputs['UR'].values[n].nodeLabel + 0.5
                 dof[6*n + 5] = myMode.fieldOutputs['UR'].values[n].nodeLabel + 0.6             
             
          elif myMode.fieldOutputs.has_key('UR3'): # ? need to add else? else: # 

             try:
                data=myMode.fieldOutputs['UR3'].values[n].data
             except OdbError:
                data=myMode.fieldOutputs['UR3'].values[n].dataDouble

             disp[6*n + 5][v]=data
             if v == 0:
                 dof[6*n + 5] = myMode.fieldOutputs['UR3'].values[n].nodeLabel + 0.6

              
          n+=1
      v+=1
      
    # Wipe out any remaining zero-values in DOF vector
    dofAct = [y for y in dof if y != 0] 
    dictionary={'freqs': freq, 'shapes': disp, 'dof' : dofAct, 'description': descript}

    print '  #modes: ',numFrames
    print '  Saved as Structure: '+ setName +'.freqs'
    print '  Saved as Structure: '+ setName +'.shapes'
    print '  MATLAB file: ' + matFileName

    sio.savemat(matFileName, {setName: dictionary}, oned_as='column')

#-------------------option: disp, appliedF, reactions------------------

if option == 'disp' or option == 'appliedF' or option == 'reactions':

    stepNames=db.steps.keys()
    print '\nsteps in Odb:'
    print  stepNames
    numSteps=len(stepNames)
    if nargin > 3:
      stepNames[0]=sys.argv[3]
      numSteps=1
    # print  stepNames
    if nargin > 4:
      frameNumber=int(sys.argv[4])
    else:
      frameNumber=-1
    # print FrameNumber

    v=0
    # print numSteps
    numValues=len(db.steps[stepNames[0]].frames[frameNumber].fieldOutputs['U'].values)
    disp=numpy.zeros((6*numValues,numSteps),dtype=float);

    while v < numSteps:
       myFrame=db.steps[stepNames[v]].frames[frameNumber]
       n=0
       while n < numValues:

          if option == 'appliedF':
             varName='CF'
          elif option == 'reactions':
             varName='RF'
          else:
             varName='U'
             
          try:
             data=myFrame.fieldOutputs[varName].values[n].data
          except OdbError:
             data=myFrame.fieldOutputs[varName].values[n].dataDouble
          

          disp[6*n+0][v]=data[0]
          disp[6*n+1][v]=data[1]
          if len(data) > 2:
             disp[6*n+2][v]=data[2]
          else:
             disp[6*n+2][v]=0.

          if option == 'appliedF':
             varName='CM'
          elif option == 'reactions':
             varName='RM'
          else:
             varName='UR'

          if myFrame.fieldOutputs.has_key(varName):
               
             try:
                data=myFrame.fieldOutputs[varName].values[n].data
             except OdbError:
                data=myFrame.fieldOutputs[varName].values[n].dataDouble
                
             disp[6*n+3][v]=data[0]
             disp[6*n+4][v]=data[1]
             disp[6*n+5][v]=data[2]
             
          else:

             if option == 'appliedF':
                varName='CM3'
             elif option == 'reactions':
                varName='RM3'
             else:
                varName='UR3'

             try:
                data=myFrame.fieldOutputs[varName].values[n].data
             except OdbError:
                data=myFrame.fieldOutputs[varName].values[n].dataDouble
                
             disp[6*n+5][v]=data
             
          n+=1
       v+=1
      
    print '  MATLAB file: ' + matFileName
    if option == 'appliedF':
      print '  applied forces for ', numSteps,' steps saved as: appliedF'
      sio.savemat(matFileName, {'appliedF': disp}, oned_as='column')
    elif option == 'reactions':
      print '  reaction forces for ', numSteps,' steps saved as: reactionF'
      sio.savemat(matFileName, {'reactionF': disp}, oned_as='column')
    else:
      print '  displacements for ', numSteps,' steps saved as: disp '
      sio.savemat(matFileName, {'disp': disp}, oned_as='column')

#-------------------option: history ---------------------------------

if option == 'history':

    stepNames=db.steps.keys()
    print '\nsteps in Odb:'
    print  stepNames
    numSteps=len(stepNames)
    if nargin > 3:
        temp=sys.argv[3]
        if temp <> 'all':
         stepNames=temp
         numSteps=1
        
    stepCount=0

    D={}
    while stepCount < numSteps:
        h=db.steps[stepNames[stepCount]].historyRegions.keys()
        hr=range(len(h));
        matStepName=re.sub("[\s\.\-]+","_",stepNames[stepCount].strip())
        print matStepName
        D[matStepName]={}
        for v in hr:  
            myH=db.steps[stepNames[stepCount]].historyRegions[h[v]]
            myK=myH.historyOutputs.keys()
            kr=range(len(myK))
            print 'history Reqion: ' + myH.name
            # print myH.name.strip()
            myRegion=re.sub("[\s\.\-]+","_",myH.name.strip())
            myRegion=re.sub("Int_Point_","IPt",myRegion)
            myRegion=re.sub("Section_Point_","SPt",myRegion)

            myRegion=re.sub("Element","El",myRegion)
            myRegion=re.sub("Point","Pt",myRegion)
            myRegion=re.sub("Section","Sec",myRegion)

            print '    renamed as: ' + myRegion
            D[matStepName][myRegion]={}
            D[matStepName][myRegion]['description']=myH.description
            for vv in kr:
                myHO=myH.historyOutputs[myK[vv]]
                print '      Output: ' + myHO.name
                myString=myHO.description+':'+myHO.name
                D[matStepName][myRegion][myK[vv]]={}
                D[matStepName][myRegion][myK[vv]]['description']=myString
                # print myString
                try:
                    data=myHO.data
                except OdbError:
                    data=myHO.datadouble
                D[matStepName][myRegion][myK[vv]]['data']=data
                # print data
                
        stepCount += 1
    print 'saved as MATLAB file: ' + matFileName
    sio.savemat(matFileName, {'history':D}, oned_as='column', format='5')
    # Note, Joe said we may have to switch it to 'S' for single precision?
     # Add?? , long_field_names=True
#-------------------option: fieldOutput ------------------------------
        
if option == 'fieldOutput':

    stepNames=db.steps.keys()
    print 'steps in Odb: ',stepNames
    numSteps=len(stepNames)

    if nargin > 3:
        temp=sys.argv[3]
        if temp <> 'all':
         stepNames[0]=temp
         numSteps=1
         
    # print  stepNames
    frameFlag='single'
    if nargin > 4:
      try: frameNumber=int(sys.argv[4])
      except:
           # print "all the Frames requested"
           frameFlag='all'
    else:
      frameNumber=-1
    
    nS=0
    # print numSteps
    D={}
    print "converting fieldOutputs to Matlab..."
    while nS < numSteps:
       print "processing STEP: "+stepNames[nS]
       # print sys.path
       stepString=re.sub("[\s\.\-]+","_",stepNames[nS])
       D[stepString]={}
       nFrames=len(db.steps[stepNames[nS]].frames)
       if frameFlag == 'all':
         frameRange=range(nFrames)
       else:
         if frameNumber > nFrames-1:
           print "!!!frameNumber exceeds nFrames, processing last frame instead!!!"
           frameRange=[-1]
         else: frameRange=[frameNumber]

       # print "nFrames: ", nFrames
       # print "frameRange: ", frameRange
       for fN in frameRange:
           print "frame: ", fN
           myFrame=db.steps[stepNames[nS]].frames[fN]
           foKeys=myFrame.fieldOutputs.keys()
           foRange=range(len(foKeys))

           if fN == -1 or fN == nFrames-1:
               frameString="frame_last"
           else:
               frameString="frame_"+str(fN)
           D[stepString][frameString]={}
           
           for jh in foRange:
               # print "   fieldOutput: "+foKeys[jh]
               if jh == foRange[-1]: print " "
               values=myFrame.fieldOutputs[foKeys[jh]].values
               nValues=len(values)
               valueRange=range(nValues)

               foString=re.sub("[\s\.\-\/]+","_",foKeys[jh])
               if len(foString) > 31:
                    foString=re.sub("__","_",foString)
               if len(foString) > 31:
                    foString=re.sub("ASSEMBLY","",foString)
               if len(foString) > 31:
                    foString=re.sub("__","_",foString)
               if len(foString) > 31:
                    foString=re.sub("PICKED","P",foString)
               if len(foString) > 31:
                    foString=re.sub("SURF","S",foString)      
                    
               D[stepString] [frameString] [foString]={}
               D[stepString][frameString][foString]['data']={}
               elementLabel=[]
               nodeLabel=[]
               instance=[]
               secPtName=[]
               secPt=[]
               mises=[]
               intPt=[]

               try:
                   nCols=len(values[-1].data)
               except TypeError:
                   nCols=1
               except:
                   try:  
                       nCols=len(values[-1].dataDouble)
                   except TypeError:
                       nCols=1
                   
               d=numpy.zeros((nValues,nCols),dtype=float);
               for v in valueRange:
                   try:
                       data=values[v].data
                   except OdbError:
                       data=values[v].dataDouble

                   d[v][0:nCols]=data

                   if values[v].elementLabel == None:
                        elementLabel.append(0)
                   else:
                        elementLabel.append(values[v].elementLabel)


                   if values[v].nodeLabel == None:
                        nodeLabel.append(0)
                   else:
                        nodeLabel.append(values[v].nodeLabel)

                   if values[v].instance== None:
                        instance.append('None')
                   else:
                        instance.append(values[v].instance.name)

                   if values[v].mises == None:
                        mises.append(0)
                   else:
                        mises.append(values[v].mises)
    
                   if values[v].integrationPoint == None:
                        intPt.append(0)
                   else:
                        intPt.append(values[v].integrationPoint)

                   if values[v].sectionPoint == None:
                        secPt.append(0)
                        secPtName.append('None')
                   else:
                        secPt.append(values[v].sectionPoint.number)
                        secPtName.append(values[v].sectionPoint.description)
                        
                        
               D[stepString][frameString][foString]['data']=d
               D[stepString][frameString]['TIME'] = myFrame.frameValue
               D[stepString][frameString][foString]['instance']=instance
               if max(elementLabel) <> 0:
                 D[stepString][frameString][foString]['elementLabel']=elementLabel
               if max(nodeLabel) <> 0:
                 D[stepString][frameString][foString]['nodeLabel']=nodeLabel
               if max(mises) <> 0:
                 D[stepString][frameString][foString]['mises']=mises
               if max(intPt) <> 0:
                 D[stepString][frameString][foString]['intPt']=intPt
               if max(secPt) <> 0:
                 D[stepString][frameString][foString]['secPt']=secPt
                 D[stepString][frameString][foString]['secPtName']=secPtName

       nS += 1
    
    print 'MATLAB FILE: ' + matFileName + '.mat'
    sio.savemat(matFileName, {'fieldOutputs': D}, oned_as='column')
    
#-------------------option: sets ------------------------------
        
if option == 'sets':

    instanceNames=db.rootAssembly.instances.keys()
    #print 'instances: ',instanceNames
    nInstances=len(instanceNames)
    nI=0
    D={}
    while nI < nInstances:
       instance=db.rootAssembly.instances[instanceNames[nI]]
       print "Sets from instance: "+instanceNames[nI]
       inString=re.sub("[\s\.\-]+","_",instanceNames[nI])
       D[inString]={}
       D[inString]['eSets']={}
       D[inString]['nSets']={}
       elementSetNames=instance.elementSets.keys()
       nElementSets=len(elementSetNames)
       nodeSetNames=instance.nodeSets.keys()
       nNodeSets=len(nodeSetNames)

       nES=0
       while nES < nElementSets:
           eSet=instance.elementSets[elementSetNames[nES]]
           eString=re.sub("[\s\.\-]+","_",elementSetNames[nES])
           D[inString]['eSets'][eString]={}
           nElements=len(eSet.elements)
           nE=0
           eLabel=[]
           while nE < nElements:
               if eSet.elements[nE].label == None:
                    #print "No label"
                    eLabel.append(0)
               else:
                    #print "Here I am"
                    eLabel.append(eSet.elements[nE].label)
               nE += 1
           D[inString]['eSets'][eString]['Labels']=eLabel
           nES += 1
       
       nNS=0
       while nNS < nNodeSets:
           nSet=instance.nodeSets[nodeSetNames[nNS]]
           nString=re.sub("[\s\.\-]+","_",nodeSetNames[nNS])
           D[inString]['nSets'][nString]={}
           nNodes=len(nSet.nodes)
           nN=0
           nLabel=[]
           while nN < nNodes:
               if nSet.nodes[nN].label == None:
                    #print "No label"
                    nLabel.append(0)
               else:
                    #print "Here I am"
                    nLabel.append(nSet.nodes[nN].label)
               nN += 1
           D[inString]['nSets'][nString]['Labels']=nLabel
           nNS += 1

       nI += 1

    print 'MATLAB FILE: ' + matFileName + '.mat'
    sio.savemat(matFileName, {'Sets': D}, oned_as='column')
    
#-----------close things and exit -----------------------------
db.close()
exit()


