% Wrapper script for substructure testing and development
% Hit "F5" to change directory
clear; clc; close all
clear classes;
addpath('classes');
return;

%% Characteristic constraint mode test
% 1 hat stiffener, 2 panels
existing = true;
models = load('CBROMs/hat_Stiffener_short_-1');
hat_stiffener = models.cbice;
models = load('CBROMs/panel_noBeams_-1');
panel = models.cbice;

% Store large files locally
filepathCbss = 'C:\Users\Joe\Documents\Projects\MBPanel Storage\CBSSs'; 
filename = 'hat_2panel_cc';

% Reference node, bas location, and offset for hat stiffeners
refNodeHat = 6050;
locationHat = [0, 0, 0];

% Boundary and connector sets for hat stiffeners
boundaryNodes = [12922, 12955, 13828, 13861, 14734, 14767, 15640, 15673]';
boundaryHat = [boundaryNodes, ones(length(boundaryNodes), 6)];
nBC = size(hat_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connHat = [hat_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)];
fRangeHat = [0, 1500];

if existing
    load(fullfile(filepathCbss, filename));
    ccs = ccss; clear('ccss');
    ccs = ccs.clearParts();
else
    ccs = CCSS(filepathCbss, filename);
end


% Add hat stiffener
ccs = ccs.addPart(hat_stiffener, refNodeHat, locationHat, boundaryHat, ...
    connHat, fRangeHat);


% Panels
refNode = 5906;
nBC = size(panel.abint.nodeSets.RETAINEDDOFS, 1);
connectors = [panel.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)]; % Fully clamped
location = [1.484299, 0, 0];
xOffset = [18.75, 0, 0]; zOffset = [0, 0, 7.5];
boundary = []; fRangePanel = [0, 500];

ccs = ccs.addPart(panel, refNode, location, boundary, connectors, fRangePanel);
ccs = ccs.addPart(panel, refNode, location + xOffset - zOffset, boundary, ...
    connectors, fRangePanel);



% Build the connection array
ccs = ccs.makeConnections();

% Build the CB model of each part
ccs = ccs.buildCBModels('matlab');
% Build the full system CB model
return
ccs = ccs.buildCBSystem(50);
ccs = ccs.makeMac();
ccs.comparePlot(2.5, 36);
quicksave('hat_2Panel_CB', 'Z:\Schoneman\Meetings\30 March 2016\images');

ccs = ccs.buildCCSystem(50, 15);
ccs = ccs.makeMac();
ccs.comparePlot(5, 36);
quicksave('hat_2Panel_CC', 'Z:\Schoneman\Meetings\30 March 2016\images');

% Now build the CC system model
plot(ccs);

if isempty(ccs.fullOrder.fn)
    ccs = ccs.loadOdb(odbpath, odbfile, '../python')
end

ccs = ccs.makeMac();
ccs.compareTable(50);
ccs.comparePlot(5, 36);


% ccs.save();


%% Substructure frame model; 9 Panels; Characteristic Constraint Modes
existing = false;

models = load('CBROMs/S_Stiffener_SS_-1');
S_stiffener = models.cbice;
models = load('CBROMs/hat_Stiffener_short_-1');
hat_stiffener = models.cbice;
models = load('CBROMs/panel_noBeams_-1');
panel = models.cbice;

filepathCbss = 'C:\Users\Joe\Documents\Projects\MBPanel Storage\CBSSs'; 
filename = 'frame_9panel_CC';
ccs = CCSS(filepathCbss, filename);

% Reference node, base location, and offset for SS stiffeners
refNodeSS = 13882;
locationSS = [0, 0, 0];
offsetSS = [18.75, 0, 0];

% Boundary and connsector sets for SS stiffeners
nBC = size(S_stiffener.abint.nodeSets.BCDOF, 1);
boundarySS = [S_stiffener.abint.nodeSets.BCDOF, ones(nBC, 6)]; % All fixed
nBC = size(S_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connSS = [S_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)]; % All fixed

% Reference node, bas location, and offset for hat stiffeners
refNodeHat = 6050;
locationHat = [-196.9E-03,750.E-03,1.280102]; 
offsetHat = [0, 0, 7.5];

% Boundary and connector sets for hat stiffeners
boundaryHat = [];
nBC = size(hat_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connHat = [hat_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)];

% Add parts
if existing
    load(fullfile(filepathCbss, filename));
    ccs = ccss; clear('ccss');
    ccs = ccs.clearParts();
else
    ccs = CCSS(filepathCbss, filename);
end

% Add S stiffeners
for i = 0:3
    ccs = ccs.addPart(S_stiffener, refNodeSS, locationSS + i*offsetSS, boundarySS, connSS, 5);
end

% Add hat stiffeners
for i = 0:3
    ccs = ccs.addPart(hat_stiffener, refNodeHat, locationHat + i*offsetHat, boundaryHat, connHat, 5);
end

% Add all 9 panels
refNode = 3929;
nBC = size(panel.abint.nodeSets.RETAINEDDOFS, 1);
connectors = [panel.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)]; % Fully clamped
location = [57.537399, 750.E-03, 23.780012];  xOffset = [18.75, 0, 0]; zOffset = [0, 0, 7.5];
boundary = [];




for i = 0:2 % x offset
    for j = 0:2 % y offset
        loc = location - i*xOffset - j*zOffset;
    ccs = ccs.addPart(panel, refNode, loc, boundary, connectors, 5);
    end
end




% Build the connection array
ccs = ccs.makeConnections();
ccs = ccs.removeConnection(9:17, 9:17);

% Build the CB model of each part
ccs = ccs.buildCBModels('matlab');

odbpath = 'C:\Temp';
odbfile = 'frame_9Panel';
ccs = ccs.loadOdb(odbpath, odbfile, '../python')


ccs.plot();

return

ccs = ccs.buildCBSystem(50);
ccs = ccs.makeMac();
ccs.comparePlot(2.5, 35);
quicksave('frame_9panel_CB', 'C:\Users\Joe\Dropbox\College\Resumes\Full time [2016]\Fall\ATA 30 August\hville_images');

ccs = ccs.buildCCSystem(50, 35);
ccs = ccs.makeMac();
ccs.comparePlot(2.5, 35);
quicksave('frame_9panel_CC', 'C:\Users\Joe\Dropbox\Thesis\chapter4\images');


% Build the full system CB model
ccs = ccs.buildCBSystem(50);
ccs = ccs.buildCCSystem(50, 50);
fprintf('CCSS Construction Time = %.0f\n', toc);
plot(ccs);


if false
    odbpath = 'C:\Temp';
    odbfile = 'frame_9Panel';
    ccs = ccs.loadOdb(odbpath, odbfile, '../python')
    ccs = ccs.makeMac();
    ccs.compareTable(50);
    ccs.comparePlot(5, 50);
%     quicksave('frame_9Panel', 'Z:\Schoneman\Meetings\15 March 2016\images');
end

% ccs.save();


% Do some snooping around with CC modes
% What do they look like?
figure('units', 'inches', 'position', [0, 0, 8, 10]);
for i = 1:4
    subplot(2, 2, i)
    ccs.plotConstraintmode(i);
end
quicksave('constraintModes', 'Z:\Schoneman\Meetings\30 March 2016\images');    

% What is their MAC?
[~, cc2cb] = sort([ccs.modalCB(:); ccs.boundaryCB(:)]);
phiCC = ccs.Tcc(:, (end - ccs.nCharDOF + 1):end);
phiCC = ccs.L*phiCC(cc2cb, :);
MAC(ccs.phi, phiCC); xlabel('\bf\Phi'); ylabel('T_{N_c}','fontweight', 'bold');
title('\bfMAC Between CC Modes and Structure Modes');
quicksave('MAC', 'C:\Users\Joe\Dropbox\College\Resumes\Full time [2016]\Fall\ATA 30 August\hville_images');    

% Examine the local CC modes of the panel
part = ccs.parts(17); nM = ccs.parts(17).nModalDOF; nB = ccs.parts(17).nBoundaryDOF;
cModes = ccs.psiCC((end - nB + 1):end, :);

lm = part.part.linModel; 
I = eye(nB, nB);
cModesPhys = [lm.psiIB; I]*cModes;

s = svd(cModesPhys); 
plot(1:50, s, 'o--', 'linewidth', 1);
xlim([1, 50]);
title('\bfSingular Values of CC Modes at Panel');
xlabel('\bfIndex'); ylabel('\bfSingular Value');
grid on
quicksave('svd', 'Z:\Schoneman\Meetings\30 March 2016\images');    
%% Full frame model; substructured
existing = false;

models = load('CBROMs/S_Stiffener_SS_-1');
S_stiffener = models.cbice;
models = load('CBROMs/hat_Stiffener_SS_-1');
models = load('CBROMs/hat_Stiffener_short_-1');
hat_stiffener = models.cbice;

filepathCbss = 'C:\Users\Joe\Documents\Projects\MBPanel Storage\CBSSs'; 
filename = 'fullFrame_SS';
cbs = CBSS(filepathCbss, filename);

% Reference node, base location, and offset for SS stiffeners
refNodeSS = 13882;
locationSS = [0, 0, 0];
offsetSS = [18.75, 0, 0];

% Boundary and connsector sets for SS stiffeners
nBC = size(S_stiffener.abint.nodeSets.BCDOF, 1);
boundarySS = [S_stiffener.abint.nodeSets.BCDOF, ones(nBC, 6)]; % All fixed
nBC = size(S_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connSS = [S_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)]; % All fixed

% Reference node, bas location, and offset for hat stiffeners
refNodeHat = 6050;
locationHat = [-196.9E-03,750.E-03,1.280102]; 
offsetHat = [0, 0, 7.5];

% Boundary and connector sets for hat stiffeners
boundaryHat = [];
nBC = size(hat_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connHat = [hat_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)];

% Add parts
if existing
    load('CBSSs\fullframe_SS');
    cbs = cbss; clear('cbss');
    cbs = cbs.clearParts();
end

% Add S stiffeners
for i = 0:3
    cbs = cbs.addPart(S_stiffener, refNodeSS, locationSS + i*offsetSS, boundarySS, connSS, [0, 500]);
end

% Add hat stiffeners
for i = 0:3
    cbs = cbs.addPart(hat_stiffener, refNodeHat, locationHat + i*offsetHat, boundaryHat, connHat, [0, 500]);
end


% Build the connection array
cbs = cbs.makeConnections();

% plot(cbs); 
% return;
% 
% Build the CB model of each part
cbs = cbs.buildCBModels();
% Build the full system CB model
cbs = cbs.buildCBSystem(50);

plot(cbs);


if true
    odbpath = 'C:\Temp';
    odbfile = 'frame_0panel';
    cbs = cbs.loadOdb(odbpath, odbfile, '../python')
    cbs = cbs.makeMac();
    cbs.compareTable(20);
    cbs.comparePlot();
%     quicksave('frameSS', 'Z:\Schoneman\Meetings\15 March 2016\images');
end
cbs.save();

%% Substructured frame model - 1 Panel; No Beams

models = load('CBROMs/S_Stiffener_SS_-1');
S_stiffener = models.cbice;
models = load('CBROMs/hat_Stiffener_short_-1');
hat_stiffener = models.cbice;
models = load('CBROMs/panel_noBeams_-1');
panel = models.cbice;

% Store large files locally
filepathCbss = 'C:\Users\Joe\Documents\Projects\MBPanel Storage\CBSSs'; 
filename = 'fullFrame_1panel_noBeams';
cbs = CBSS(filepathCbss, filename);

% Reference node, base location, and offset for SS stiffeners
refNodeSS = 13882;
locationSS = [0, 0, 0];
offsetSS = [18.75, 0, 0];

% Boundary and connsector sets for SS stiffeners
nBC = size(S_stiffener.abint.nodeSets.BCDOF, 1);
boundarySS = [S_stiffener.abint.nodeSets.BCDOF, ones(nBC, 6)]; % All fixed
nBC = size(S_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connSS = [S_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)]; % All fixed

% Reference node, bas location, and offset for hat stiffeners
refNodeHat = 6050;
locationHat = [-196.9E-03,750.E-03,1.280102]; 
offsetHat = [0, 0, 7.5];

% Boundary and connector sets for hat stiffeners
boundaryHat = [];
nBC = size(hat_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connHat = [hat_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)];



% Add S stiffeners
for i = 0:3
    cbs = cbs.addPart(S_stiffener, refNodeSS, locationSS + i*offsetSS, boundarySS, connSS);
end

% Add hat stiffeners
for i = 0:3
    cbs = cbs.addPart(hat_stiffener, refNodeHat, locationHat + i*offsetHat, boundaryHat, connHat);
end

% Add a single panel
refNode = 3929;
nBC = size(panel.abint.nodeSets.RETAINEDDOFS, 1);
% connectors = [panel.abint.nodeSets.RETAINEDDOFS, ones(nBC, 3), zeros(nBC, 3)]; % pinned only
connectors = [panel.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)]; % Fully clamped
location = [57.537399, 750.E-03, 23.780012];
boundary = [];
cbs = cbs.addPart(panel, refNode, location, boundary, connectors);



% Build the connection array
cbs = cbs.makeConnections();

% Build the CB model of each part
cbs = cbs.buildCBModels(10);
% Build the full system CB model
cbs = cbs.buildCBSystem(20);

plot(cbs);



if false
    odbpath = 'C:\Temp';
    odbfile = 'frame_1Panel';
    cbs = cbs.loadOdb(odbpath, odbfile, '../python')
    cbs = cbs.makeMac();
    cbs.compareTable(20);
    cbs.comparePlot(10);
    quicksave('frameSS_1Panel', 'Z:\Schoneman\Meetings\15 March 2016\images');
end


% cbs.save();

%% Substructure frame model; 9 Panels; 
existing = true;
models = load('CBROMs/S_Stiffener_SS_-1');
S_stiffener = models.cbice;
models = load('CBROMs/hat_Stiffener_short_-1');
hat_stiffener = models.cbice;
models = load('CBROMs/panel_noBeams_-1');
panel = models.cbice;

filepathCbss = 'C:\Users\Joe\Documents\Projects\MBPanel Storage\CBSSs'; 
filename = 'frame_9panel';
cbs = CBSS(filepathCbss, filename);

% Reference node, base location, and offset for SS stiffeners
refNodeSS = 13882;
locationSS = [0, 0, 0];
offsetSS = [18.75, 0, 0];

% Boundary and connsector sets for SS stiffeners
nBC = size(S_stiffener.abint.nodeSets.BCDOF, 1);
boundarySS = [S_stiffener.abint.nodeSets.BCDOF, ones(nBC, 6)]; % All fixed
nBC = size(S_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connSS = [S_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)]; % All fixed

% Reference node, bas location, and offset for hat stiffeners
refNodeHat = 6050;
locationHat = [-196.9E-03,750.E-03,1.280102]; 
offsetHat = [0, 0, 7.5];

% Boundary and connector sets for hat stiffeners
boundaryHat = [];
nBC = size(hat_stiffener.abint.nodeSets.RETAINEDDOFS, 1);
connHat = [hat_stiffener.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)];

% Add parts
if existing
    load('C:\Users\Joe\Documents\Projects\MBPanel Storage\CBSSs\frame_9panel');
    cbs = cbss; clear('cbss');
    cbs = cbs.clearParts();
end

% Add S stiffeners
for i = 0:3
    cbs = cbs.addPart(S_stiffener, refNodeSS, locationSS + i*offsetSS, boundarySS, connSS, [0, 1000]);
end

% Add hat stiffeners
for i = 0:3
    cbs = cbs.addPart(hat_stiffener, refNodeHat, locationHat + i*offsetHat, boundaryHat, connHat, [0, 1500]);
end

% Add all 9 panels
refNode = 3929;
nBC = size(panel.abint.nodeSets.RETAINEDDOFS, 1);
connectors4 = [3913; 3929; 5742; 5906];
connectors4 = [connectors4(:), ones(length(connectors4), 6)];
connectors16 = [3913, 3929, 4255, 4271, 4426, 4442, 4654, 4670, 5068, 5084, 5398, 5414, 5656, 5672, 5742, 5906];
connectors16 = [connectors16(:), ones(length(connectors16), 6)];
connectors = [panel.abint.nodeSets.RETAINEDDOFS, ones(nBC, 6)]; % Fully clamped
location = [57.537399, 750.E-03, 23.780012];  xOffset = [18.75, 0, 0]; zOffset = [0, 0, 7.5];
boundary = [];




for i = 0:2 % x offset
    for j = 0:2 % y offset
        loc = location - i*xOffset - j*zOffset;
    cbs = cbs.addPart(panel, refNode, loc, boundary, connectors, [0, 1000]);
    end
end




% Build the connection array
cbs = cbs.makeConnections();
cbs = cbs.removeConnection(9:17, 9:17);
tic
% Build the CB model of each part
cbs = cbs.buildCBModels();
% Build the full system CB model
cbs = cbs.buildCBSystem(50);
fprintf('CBSS Construction Time = %.0f\n', toc);
plot(cbs);


if false
    odbpath = 'C:\Temp';
    odbfile = 'frame_9Panel';
    cbs = cbs.loadOdb(odbpath, odbfile, '../python')
    cbs = cbs.makeMac();
    cbs.compareTable(20);
    cbs.comparePlot(30);
    quicksave('frame_9Panel', 'Z:\Schoneman\Meetings\15 March 2016\images');
end

% cbs.save();
