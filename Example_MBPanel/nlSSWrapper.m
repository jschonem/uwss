% Wrapper script for nonlinear substructure testing and development
% Hit "F5" to change directory
clear; clc; close all
clear classes;
addpath('classes');
return;


%% Substructure frame model; 9 Panels; Characteristic Constraint Modes

filepathCbss = 'C:\Users\Joe\Documents\Projects\MBPanel Storage\CBSSs'; 
filename = 'frame_9panel_CC';

load(fullfile(filepathCbss, filename));
ccs = ccss; clear('ccss');

% Make new CC model and evaluate if desired 
if false
    ccs = ccs.buildCCSystem(50, 35);
    ccs = ccs.makeMac();
    ccs.comparePlot(2.5, 35);
    % quicksave('frame_9panel_CC', 'C:\Users\Joe\Dropbox\College\Resumes\Full time [2016]\Fall\ATA 30 August\hville_images');
end

% Make NLROMs of the parts
basePart = 9;
copyParts = 10:17;
ccs = ccs.buildNLSystem(basePart, copyParts);
