# -*- coding: mbcs -*-
#
# Abaqus/CAE Release 6.12-2 replay file
# Internal Version: 2012_06_28-23.52.08 119883
# Run by Joe on Thu Aug 25 10:53:45 2016
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=170.660415649414, 
    height=269.522216796875)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from caeModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
openMdb('mbpanel.cae')
#: The model database "C:\Users\Joe\Dropbox\matlab\tools\SubStructJDS\Example_MBPanel\Abaqus\cae\mbpanel.cae" has been opened.
session.viewports['Viewport: 1'].setValues(displayedObject=None)
session.viewports['Viewport: 1'].partDisplay.geometryOptions.setValues(
    referenceRepresentation=ON)
p = mdb.models['S_stiffener_SS'].parts['PART-1']
session.viewports['Viewport: 1'].setValues(displayedObject=p)
p1 = mdb.models['frame_1Panel'].parts['PART-1']
session.viewports['Viewport: 1'].setValues(displayedObject=p1)
a = mdb.models['frame_1Panel'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
session.viewports['Viewport: 1'].assemblyDisplay.setValues(
    optimizationTasks=OFF, geometricRestrictions=OFF, stopConditions=OFF)
a = mdb.models['frame_0panel'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
a = mdb.models['frame_9panel'].rootAssembly
session.viewports['Viewport: 1'].setValues(displayedObject=a)
