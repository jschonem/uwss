# -*- coding: mbcs -*-
#
# Abaqus/Viewer Release 6.12-2 replay file
# Internal Version: 2012_06_28-23.52.08 119883
# Run by Joe on Thu Aug 25 10:53:11 2016
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=170.660415649414, 
    height=269.522216796875)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from viewerModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
o2 = session.openOdb(name='frame_9panel.odb')
#: Model: C:/Users/Joe/Dropbox/matlab/tools/SubStructJDS/Example_MBPanel/Abaqus/odb/frame_9panel.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       38
#: Number of Node Sets:          33
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o2)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
