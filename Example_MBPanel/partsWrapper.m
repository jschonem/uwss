% Generate ABINT/NLROM objects for later use
clear; clc; close all
clear classes

return;

%% S Stiffener
clear

workingDir = 'C:\Temp';
filepathAbint = 'ABINTs';
filepathCbice = 'CBROMs';
pythonpath = '..\python';

% Make ABINT object and save for later
ab = ABINT(workingDir, filepathAbint, pythonpath);
% Read an input file
inputPath = 'abaqus\models\S_stiffener';
inputFile = 'S_Stiffener_SS';
ab = ab.readInp(inputPath, inputFile);
ab = ab.getDof();
[M, K] = ab.matrix([]);
% save(ab);


% Make CBICE object and save
linDispLC = 1; mind = 1; thick = 1;
cbi = CBICE(ab, mind, thick, linDispLC, workingDir, filepathCbice);
cbi.linModel.M = M; cbi.linModel.K = K;
% save(cbi);


%% Hat stiffener - Short
clear

workingDir = 'C:\Temp';
filepathAbint = 'ABINTs';
filepathCbice = 'CBICEs';
pythonpath = '..\python';

if ~exist('ab')
    ab = ABINT(workingDir, filepathAbint, pythonpath);
    % Read an input file
    inputPath = 'abaqus\models\hat_stiffener_short';
    inputFile = 'hat_Stiffener_short';
    ab = ab.readInp(inputPath, inputFile);
    ab = ab.getDof();
    [M, K] = ab.matrix([]);
%     save(ab);
end

if ~exist('cbi')
    linDispLC = 1; mind = 1; thick = 1;
    cbi = CBICE(ab, mind, thick, linDispLC, workingDir, filepathCbice);
    cbi.linModel.M = M; cbi.linModel.K = K;
%     save(cbi);
end




%% Panel - No Beams
clear

workingDir = 'C:\Temp';
filepathAbint = 'ABINTs';
filepathCbice = 'CBROMs';
pythonpath = '..\python';

% Make ABINT object and save for later
ab = ABINT(workingDir, filepathAbint, pythonpath);
% Read an input file
inputPath = '..\SchonemanMBPAbaqus\Abaqus Storage\models\panel_noBeams';
inputFile = 'panel_noBeams';
ab = ab.readInp(inputPath, inputFile);
ab = ab.getDof();
[M, K] = ab.matrix([]);
save(ab);


% Make CBICE object and save
linDispLC = 1; mind = 1; thick = 1;
cbi = CBICE(ab, mind, thick, linDispLC, workingDir, filepathCbice);
cbi.linModel.M = M; cbi.linModel.K = K;
save(cbi);


